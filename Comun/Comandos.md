Soluci�n de ClientLink
===================

## Contenido

[TOC]

------------

## Enlaces

```
#!powershell
# Todas las incidencias
http://jira-led:8080/secure/RapidBoard.jspa?rapidView=80&view=planning.nodetail

# Acceder al panel de newsletter para ver mis incidencias
http://jira-led:8080/secure/RapidBoard.jspa?rapidView=80&view=planning.nodetail&quickFilter=208

``` 

## Dotnet Commands

* Obtener datos BD:

```
#!powershell
#Para esta api no generamos las entidades a partir de labase de datos por que las tablas de kojak tienen muchos campos que no vamos a usar
BBDD CrmMySql
Scaffold-DbContext "server=ES006-PPDDB005;port=3306;user=led-ext-aheras;password=iu45:MQWaxG;database=led_crm" "Pomelo.EntityFrameworkCore.MySql" -Tables product_types -OutputDir Entidades/Crm -Project Crm.Modelos -Force -Context CrmMySqlDbContext

//Si desaparece el constructor al hacer el scaffolding  en el fichero ClavesDbContext.cs a�adirlo, suele ocurrir este error cuando aparecen lineas amarillas al ejecutar el comando anterior en el administrador de paquetes
public CrmMySqlDbContext()
        {
        }

        public CrmMySqlDbContext(DbContextOptions<CrmMySqlDbContext> options)
            : base(options)
        {
        }



BBDD CrmReplicaMySql
Scaffold-DbContext "server=ES006-PPDDB005;port=3306;user=led-ext-aheras;password=iu45:MQWaxG;database=led_crm" "Pomelo.EntityFrameworkCore.MySql" -Tables product_types -OutputDir Entidades/CrmReplica -Project Crm.Modelos -Force -Context CrmReplicaMySqlDbContext

//Si desaparece el constructor al hacer el scaffolding  en el fichero ClavesDbContext.cs a�adirlo, suele ocurrir este error cuando aparecen lineas amarillas al ejecutar el comando anterior en el administrador de paquetes
public CrmReplicaMySqlDbContext()
        {
        }

        public CrmReplicaMySqlDbContext(DbContextOptions<CrmReplicaMySqlDbContext> options)
            : base(options)
        {
        }


BBDD CrmBusquedasMySql
Scaffold-DbContext "server=ES006-PPDDB005;port=3306;user=led-ext-aheras;password=iu45:MQWaxG;database=led_crm" "Pomelo.EntityFrameworkCore.MySql" -Tables clientes_relacionados -OutputDir Entidades/Busquedas -Project Crm.Modelos -Force -Context CrmBusquedasMySqlDbContext

//Si desaparece el constructor al hacer el scaffolding  en el fichero ClavesDbContext.cs a�adirlo, suele ocurrir este error cuando aparecen lineas amarillas al ejecutar el comando anterior en el administrador de paquetes
public CrmBusquedasMySqlDbContext()
        {
        }

        public CrmBusquedasMySqlDbContext(DbContextOptions<CrmBusquedasMySqlDbContext> options)
            : base(options)
        {
        }



* Cambios en ClavesDbContext.cs


```
#!csharp

    //Actualizamos Referencias
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Diagnostics;
    using Microsoft.Extensions.Logging;

```

```
#!csharp
    ///cambiamos constructor y a�adimos logging
    private readonly ILoggerFactory _loggerFactory;

    /// <summary>
    /// Permite capturar los datos de EF para el log
    /// Ver https://dennisroche.com/2017/12/04/ef-core-logging-with-serilog/
    /// Ver https://weblogs.asp.net/dixin/entity-framework-core-and-linq-to-entities-3-logging-and-tracing-queries
    /// Ver https://docs.microsoft.com/en-us/ef/core/miscellaneous/logging
    /// </summary>
    /// <param name="optionsBuilder"></param>
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder
            .UseLoggerFactory(_loggerFactory)
            .ConfigureWarnings(warnings => {
                warnings.Log(RelationalEventId.CommandExecuted);
                warnings.Log(RelationalEventId.CommandExecuting);
                warnings.Log(RelationalEventId.CommandError);
            });
    }

    /// <summary>
    /// Para que tome la conexi�n desde la configuraci�n del proyecto llamante
    /// </summary>
    /// <param name="options">par�metros</param>
    /// <param name="loggerFactory">inyectamos el objeto de log</param>
    public HubDbContext(DbContextOptions<HubDbContext> options, ILoggerFactory loggerFactory)
        : base(options)
    {
        _loggerFactory = loggerFactory;
    }


```


* Cambiar Entorno en web.config (no es necesario):

```
#!xml

 <aspNetCore stdoutLogEnabled="true">
     <environmentVariables>
        <environmentVariable name="ASPNETCORE_ENVIRONMENT" value="Staging" />
        <environmentVariable name="ASPNETCORE_DETAILEDERRORS" value="true" />
     </environmentVariables>
   </aspNetCore>

```



