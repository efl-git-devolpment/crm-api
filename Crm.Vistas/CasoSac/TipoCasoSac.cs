﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.CasoSac
{
    public class TipoCasoSac
    {
        public int Id { get; set; }
        public string NombreTipo { get; set; }
        public string DescripcionTipo { get; set; }
        public int Tipo { get; set; }
        public int Motivo { get; set; }
        public string NombreMotivo { get; set; }
        public int Submotivo { get; set; }
        public string NombreSubmotivo { get; set; }
        public int? DescripcionSubmotivo { get; set; }
        public string NombreDescripcionSubmotivo { get; set; }
        public string EstadoCaso { get; set; }
        public string DetalleGestion { get; set; }
        public string resultado { get; set; }
    }
}
