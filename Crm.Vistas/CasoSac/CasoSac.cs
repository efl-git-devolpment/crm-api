﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.CasoSac
{
    public class CasoSac
    {
        public int IdCuentaCrm { get; set; }
        public int IdContactoCrm { get; set; }
        public int IdTipoCaso { get; set; }
        public string CanalPrimerContacto { get; set; }
        public string SentidoPrimerContacto { get; set; }
        public string MotivoPendiente { get; set; }
        public string AccionRealizada { get; set; }
        public string Comentarios { get; set; }
        public List<DatosSuscripcion> SuscripcionesRelacionadas { get; set; }
        public string IdCasosac { get; set; }
        public int IdUsuarioApp { get; set; }
    }

    public class DatosSuscripcion
    {
        public int IdSuscripcionNav { get; set; }
        public string IdProductoNav { get; set; }
        public string DescProductoNav { get; set; }
    }

}
