﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.CasoSac
{
    public class ResultadoCasoSac
    {
        public int codigoResultado { get; set; }
        public string mensajeResultado { get; set; }
        public string idCasoSac { get; set; }
    }
}
