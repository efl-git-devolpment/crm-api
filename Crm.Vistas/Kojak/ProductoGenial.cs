﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Vistas.Kojak
{
    public class ProductoGenial
    {

        public string Cod_Producto { get; set; }
        public string Nombre_Responsable { get; set; }
        public string Apellido_1_Responsable { get; set; }
        public string Apellido_2_Responsable { get; set; }
        public string Email_Responsable { get; set; }

    }

    public class ProductoGenialExt
    {
        public bool encontrado { get; set; }

        public ProductoGenial productoGenial { get; set; }
    }

}

