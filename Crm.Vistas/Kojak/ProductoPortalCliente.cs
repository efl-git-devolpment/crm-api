﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Kojak
{
    public class ProductoPortalCliente
    {
        public string Cod_Producto { get; set; }

        public string Migracion { get; set; }

        public FechaContacto Fecha_Contacto { get; set; }

        public string Observaciones { get; set; }
    }

    public class ProductoPortalClienteExt
    {
        public bool encontrado { get; set; }
        public ProductoPortalCliente productoPortalCliente { get; set; }
    }

    public class FechaContacto
    {
        public DateTime? date { get; set; }
        public int? timezone_type { get; set; }
        public string timezone { get; set; }
    }
}
