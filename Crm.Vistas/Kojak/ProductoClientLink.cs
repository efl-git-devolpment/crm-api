﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Kojak
{
    public class ProductoClientLink
    {
        public string Cod_Producto { get; set; }
        public string Dominio_Web { get; set; }
        public FechaClientLink Fecha_Inicio_Onboarding { get; set; }
        public string  Observaciones { get; set; }
        public string Nombre_Responsable { get; set; }
        public string Apellido_1_Responsable { get; set; }
        public string Apellido_2_Responsable { get; set; }
        public string Email_Responsable { get; set; }
        public string Ampliacion_Materias { get; set; }
        public string Webs_Demo_Referencia { get; set; }
    }

    public class ProductoClientLinkExt
    {
        public bool encontrado { get; set; }

        public ProductoClientLink productoClientLink { get; set; }
    }

    public class FechaClientLink
    {
        public DateTime? date { get; set; }
        public int? timezone_type { get; set; }
        public string timezone { get; set; }
    }
}
