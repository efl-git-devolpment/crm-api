﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Kojak
{
    public class SuperAdministrador
    {
        public int? Num_Kojak { get; set; }
        public string Nombre { get; set; }
        public string Apellido_1 { get; set; }
        public string Apellido_2 { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        
    }

    public class SuperAdministradorCentinela
    {
        public bool Encontrado { get; set; }
        public SuperAdministrador SuperAdministrador { get; set; }
    }

}
