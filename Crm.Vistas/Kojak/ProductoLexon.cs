﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Kojak
{
    public class ProductoLexon
    {
        public string Cod_Producto { get; set; }
        public int? Migracion { get; set; }
        public string Origen_Migracion { get; set; }
        public string Responsable_Migracion { get; set; }
        public string Telefono_Responsable_Migracion { get; set; }
        public string Email_Responsable_Migracion { get; set; }
        public FechaLexon Fecha_Contacto { get; set; }
        public FechaLexon Fecha_Formacion { get; set; }
        public string Observaciones { get; set; }
        public List<Usuario> Usuarios { get; set; }
        public string Rango_Horas { get; set; }
    }


    public class ProductoLexonExt
    {
        public bool encontrado { get; set; }

        public ProductoLexon productoLexon { get; set; }
    }

    public class FechaLexon
    {
        public DateTime? date { get; set; }
        public int? timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class Usuario
    {
        public int num_kojak_usuario { get; set; }
        public string nombre_usurio { get; set; }
        public int contacto_espejo { get; set; }
        public object bloqueo { get; set; }
        public string email { get; set; }
    }
}
