﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Asistentes
{
    public class PermitirAsistente
    {
        public bool codigoResultado { get; set; }
        public ControlErrorAsistente mensajeResultado { get; set; }
    }
    public class ControlErrorAsistente
    {
        public string controlExistenciaCuentaFacturacion { get; set; }
        public string controlExistenciaCuentaEnvio { get; set; }
        public string controlExistenciaAsistente { get; set; }
        public string controlExistenciaEmailEnAsistente { get; set; }
        public string controlEmailPrincipal { get; set; }
        public string controlAsistentePerteneceCuentasFacturacionEnvio { get; set; }
        public string controlNIFAsistente { get; set; }
        public string controlBloqueoAsistente { get; set; }
    }
}
