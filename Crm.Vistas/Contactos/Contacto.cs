﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Contactos
{
    public class Contacto
    {
        public int IdClienteCrm { get; set; }
        public int IdContactoCrm { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Tlf { get; set; }
        public string Email { get; set; }
        public string CifNif { get; set; }
        public short indEspejo { get; set; }
    }

    public class ContactoListadoExt
    {
        public int contador { get; set; }

        public IEnumerable<Contacto> Listado { get; set; }

    }
}
