﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Contactos
{
    public class InsertarContactoPeticion
    {

        /// <summary>
        /// Id Cliente Crm
        /// </summary>
        public int IdClienteCrm { get; set; }

        /// <summary>
        /// Módulo Origen ( 1 - account , 2 - lead
        /// </summary>
        public int intModuloOrigen { get; set; }
        /// <summary>
        /// Nombre del contacto
        /// </summary>

        public string strNombre { get; set; }

        /// <summary>
        /// Primer apellido del contacto
        /// </summary>
        public string strApellido1 { get; set; }

        /// <summary>
        /// Segundo apellido del contacto
        /// </summary>
        public string strApellido2 { get; set; }

        /// <summary>
        /// Teléfono del contacto
        /// </summary>
        public string strTelefono { get; set; }

        /// <summary>
        /// Emails del contacto
        /// </summary>
        public string strEmail { get; set; }

        /// <summary>
        /// CifNif del contacto
        /// </summary>
        public string strCifNif { get; set; }

        /// <summary>
        /// Indicativo contacto tercero
        /// </summary>
        public bool bIndTerceros { get; set; }

        /// <summary>
        /// Indicativo revisar duplicados
        /// </summary>
        public bool bIndCheckDuplicados { get; set; }



    }

    public class ModificarContactoPeticion
    {
        /// <summary>
        /// Id Contacto Crm
        /// </summary>
        public int IdContactoCrm { get; set; }
        /// <summary>
        /// Id Cliente Crm
        /// </summary>
        public int IdClienteCrm { get; set; }

        /// <summary>
        /// Módulo Origen ( 1 - account , 2 - lead
        /// </summary>
        public int intModuloOrigen { get; set; }
        /// <summary>
        /// Nombre del contacto
        /// </summary>

        public string strNombre { get; set; }

        /// <summary>
        /// Primer apellido del contacto
        /// </summary>
        public string strApellido1 { get; set; }

        /// <summary>
        /// Segundo apellido del contacto
        /// </summary>
        public string strApellido2 { get; set; }

        /// <summary>
        /// Teléfono del contacto
        /// </summary>
        public string strTelefono { get; set; }

        /// <summary>
        /// Email del contacto
        /// </summary>
        public string strEmail { get; set; }

        /// <summary>
        /// Email anterior del contacto
        /// </summary>
        public string strEmailAnt { get; set; }

        /// <summary>
        /// CifNif del contacto
        /// </summary>
        public string strCifNif { get; set; }

        /// <summary>
        /// Indicativo revisar duplicados
        /// </summary>
        public bool bIndCheckDuplicados { get; set; }
        /// <summary>
        /// Tipo de modificacion de Email (0 - No se tiene encuenta, 1 - Entidad, 2 - Todas
        /// </summary>
        public int intTipoEmail { get; set; } = -1;


    }

    public class ModificarEmailContactoPeticion
    {
        /// <summary>
        /// Id Contacto Crm
        /// </summary>
        public int IdContactoCrm { get; set; }

        /// <summary>
        /// Módulo Origen ( 1 - account , 2 - lead, 3 - Contact
        /// </summary>
        public int intModuloOrigen { get; set; }

        /// <summary>
        /// Email nuevo del contacto
        /// </summary>
        public string strEmailNuevo { get; set; }

        /// <summary>
        /// Email anterior del contacto
        /// </summary>
        public string strEmailAnterior { get; set; }

        /// <summary>
        /// Id Contacto Crm - formato cadena 36char
        /// </summary>
        public string sIdContactoCrm { get; set; }

    }
}
