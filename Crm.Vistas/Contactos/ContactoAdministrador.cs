﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Contactos
{
    public class ContactoAdministrador
    {
        public int IdClienteNav { get; set; }
        public int IdContactoCrm { get; set; }

        public string IdUsuarioPro { get; set; }

        public string strEmail { get; set; } 
    }
}
