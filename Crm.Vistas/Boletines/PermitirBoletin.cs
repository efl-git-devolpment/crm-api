﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Boletines
{
    public class PermitirBoletin
    {
        public bool codigoResultado { get; set; }
        public ControlError mensajeResultado { get; set; }
    }
    public class ControlError
    {
        public string controlExistenciaCuenta { get; set; }
        public string controlExistenciaUsuario { get; set; }
        public string controlBloqueoCuenta { get; set; }
        public string controlAccionComercialTMKPL { get; set; }
        public string controlAccionComercialTMKBD { get; set; }
        public string controlEstadoExpediente { get; set; }
        public string controlActividadCuenta { get; set; }
        public string controlOportunidadesDemoConcertadaCaducar { get; set; }
        public string controlOportunidadesDemoRealizadaCaducar { get; set; }
        public string controlOportunidadesDiasCierre { get; set; }
        public string controlCuentaUsuarioAsignado { get; set; }
        public string controlCuentaUsuarioAsignadoEquipo { get; set; }
        public controlOportunidadesAbiertasLead controlOportunidadesAbiertasLead { get; set; }
        public string controlOportunidadesAbiertas { get; set; }
    }

    public class controlOportunidadesAbiertasLead
    {
        public string msj { get; set; }
        public data data { get; set; }
    }
    public class data
    {
        public int Id_Lead_Kojak { get; set; }
        public int Id_Lead_Web { get; set; }
        public string Id_Cuenta { get; set; }
        public string Id_Oportunidad { get; set; }
        public string Etapa_De_Ventas { get; set; }
    }

}
