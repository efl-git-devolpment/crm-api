﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Externos
{
    public class PermisoUsuarios
    {
        public string idEntrada { get; set; }
        public int idCliente { get; set; }
        public string nombre { get; set; }

        public string tipoEntrada { get; set; }

        public string estado { get; set; }

        public string email { get; set; }

        public string nnavision { get; set; }

    }
}
