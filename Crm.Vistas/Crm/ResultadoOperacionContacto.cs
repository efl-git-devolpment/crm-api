﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{

    public class ResultadoOperacionContacto
    {
        [JsonProperty("codigoResultado")]
        public int codigoResultado { get; set; }
        [JsonProperty("mensajeResultado")]
        public string mensajeResultado { get; set; }
        [JsonProperty("idContacto")]
        public string idContacto { get; set; }
        [JsonProperty("num_crm")]
        public string num_crm { get; set; }
        [JsonProperty("info_carga")]
        public string info_carga { get; set; }

    }

    public class ResultadoModificarEmailContacto
    {
        [JsonProperty("codigoResultado")]
        public string codigoResultado { get; set; }
        [JsonProperty("mensajeResultado")]
        public string mensajeResultado { get; set; }

    }
}
