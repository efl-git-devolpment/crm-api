﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
   
    public class UsuariosBoletin
    {
        [JsonProperty("NUM_KOJAK_USUARIO")]
        public int num_kojak_usuario { get; set; }

        [JsonProperty("NOMBRE_USUARIO")]
        public string nombreUsuario { get; set; }

        [JsonProperty("APELLIDO_1_USUARIO")]
        public string apellido1Usuario { get; set; }

        [JsonProperty("APELLIDO_2_USUARIO")]
        public string apellido2Usuario { get; set; }

        [JsonProperty("IP_UP")]
        public string ipUp { get; set; }

        [JsonProperty("EMAIL")]
        public string email { get; set; }

        [JsonProperty("TELEFONO_ASISTENTE")]
        public string telefonoAsistente { get; set; }
       
    }
    public class BoletinGenial
    {
        [JsonProperty("COD_PRODUCTO")]
        public string codigoProducto { get; set; }

        [JsonProperty("NOMBRE_RESPONSABLE")]
        public string nombreResponsable { get; set; }

        [JsonProperty("APELLIDO_1_RESPONSABLE")]
        public string apellido1Responsable { get; set; }

        [JsonProperty("APELLIDO_2_RESPONSABLE")]
        public string apellido2Responsable { get; set; }

        [JsonProperty("EMAIL_RESPONSABLE")]
        public string emailResponsable { get; set; }

        [JsonProperty("TELEFONO_RESPONSABLE")]
        public string telefonoResponsable { get; set; }

        [JsonProperty("USUARIOS")] 
        public List<UsuariosBoletin> lstUsuarios { get; set; }

    }

    public class ResultadoBoletinGenial
    {
        [JsonProperty("encontrado")]
        public bool encontrado { get; set; }
        [JsonProperty("productoGenial")]
        public BoletinGenial productoGenial { get; set; }
    }

}
