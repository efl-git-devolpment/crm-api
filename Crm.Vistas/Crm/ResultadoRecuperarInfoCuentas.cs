﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using DllApiBase.Vistas.Cliente;

namespace Crm.Vistas.Crm
{


    public class ResultadoRecuperarInfoCuentas
    {
        [JsonProperty("codigo")]
        public int codigoResultado { get; set; }
        [JsonProperty("descripcion")]
        public string mensajeResultado { get; set; }
        [JsonProperty("numero_total_clientes")]
        public int? numero_total_clientes { get; set; }
        [JsonProperty("clientes")]
        public List<DllApiBase.Vistas.Cliente.ClientesCrm> lstCuentas { get; set; }

    }


}
