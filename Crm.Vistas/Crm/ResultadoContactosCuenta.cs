﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{

    public class EmailContacto
    {
        public string email { get; set; }
        public int principal { get; set; }
    }
    public class ContactoCrm
    {
        public string id { get; set; }
        public int num_crm { get; set; }
        public string nombre { get; set; }
        public string apellido_1 { get; set; }
        public string apellido_2 { get; set; }
        public List<EmailContacto> emails { get; set; }
        public string telefono { get; set; }
        public string nif { get; set; }
        public int espejo { get; set; }
        public string bloqueo { get; set; }
        public string cancelacion { get; set; }
    }

    public class ResultadoContactosCuenta
    {
        [JsonProperty("codigo")]
        public int codigoResultado { get; set; }

        [JsonProperty("descripcion")]
        public string mensajeResultado { get; set; }

        [JsonProperty("numero_total_contactos")]
        public string numero_total_contactos { get; set; }

        [JsonProperty("contactos")]
        public List<ContactoCrm> lstContactos { get; set; }

    }
}
