﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
    public class InfoContacto
    {
        public string id_contacto { get; set; }
        public int num_kojak { get; set; }
        public string cod_indicator { get; set; }
        public string statut { get; set; }
        public string contacto_espejo { get; set; }
        public string tratamiento { get; set; }
        public string nombre { get; set; }
        public string apellido_1 { get; set; }
        public string apellido_2 { get; set; }
        public string origen_web { get; set; }
        public string descripcion { get; set; }
        public string fecha_creacion { get; set; }
        public string fecha_modificacion { get; set; }
        public string telefono_ppal { get; set; }
        public string telefono_2 { get; set; }
        public string telefono_3 { get; set; }
        public string telefono_movil { get; set; }
        public string fax { get; set; }
        public List<string> emails { get; set; }
        public string web { get; set; }
        public string twitter { get; set; }
        public string documento { get; set; }
        public string departamento { get; set; }
        public string departamento_mkt { get; set; }
        public string via { get; set; }
        public string poblacion { get; set; }
        public string provincia { get; set; }
        public string cp { get; set; }
        public string pais { get; set; }
        public string tipo_via { get; set; }
        public string complemento { get; set; }
        public string resto { get; set; }
        public string numero { get; set; }
        public string latitud { get; set; }
        public string longitud { get; set; }
        public string direccion_google { get; set; }
        public string fecha_colegiacion { get; set; }
        public string ejerciente { get; set; }
        public string colectivo { get; set; }
        public string cargo { get; set; }
        public string n0_colegiado { get; set; }

    }

    public class ResultadoInfoContacto
    {
        [JsonProperty("codigoResultado")]
        public int codigoResultado { get; set; }
        [JsonProperty("mensajeResultado")]
        public string mensajeResultado { get; set; }
        [JsonProperty("contacto")]
        public InfoContacto contacto { get; set; }
    }


    public class InfoValidoContacto
    {
        public int bloqueado { get; set; }
        public string duplicado_con_modulo { get; set; }
        public string duplicado_con_id { get; set; }
        public int cancelado { get; set; }
    }

    public class ResultadoInfoValidoContacto : InfoValidoContacto
    {
        public int codigo { get; set; }
        public string descripcion { get; set; }

    }

}
