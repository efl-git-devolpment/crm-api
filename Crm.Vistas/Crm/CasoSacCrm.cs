﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
    public class CasoSacCrm
    {
        /// <summary>
        /// Hay que pasar siempre web
        /// </summary>
        public string origen { get; set; }
        /// <summary>
        /// Es el usuario ldap logado en la aplicacion
        /// </summary>
        public string createdByName { get; set; }
        public string id_potencial { get; set; }
        /// <summary>
        /// Es el id cliente crm
        /// </summary>
        public string id_cuenta { get; set; }
        /// <summary>
        /// Es el id contacto crm
        /// </summary>
        public string id_contacto { get; set; }
        /// <summary>
        /// Es el id que se obtiene del metodo de crm devuelveTipologiaCasoSac
        /// </summary>
        public int tipo_caso { get; set; }
        public string canal_primer_contacto { get; set; }
        public string sentido_primer_contacto { get; set; }
        public string motivo_pendiente { get; set; }
        public string accion_realizada { get; set; }
        public string comentarios { get; set; }
        /// <summary>
        /// Es el id usuario crm logado id_usuario_sugar en claves
        /// </summary>
        public string agente { get; set; }
        /// <summary>
        /// Es el idSuscripcionNav separado por comas
        /// </summary>
        public string productos_relacionados { get; set; }
        public string id_casosac { get; set; }
        /// <summary>
        /// Es el id usuario crm logado id_usuario_sugar en claves
        /// </summary>
        public string agente_gestor_usuarios { get; set; }
    }
}
