﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
    public class ResultadoCuenta
    {
        public int codigoResultado { get; set; }
        public string mensajeResultado { get; set; }
        public string idCuenta { get; set; }
        public string numeroCuenta { get; set; }
        public string idContacto { get; set; }
        public string numeroContacto { get; set; }
        public List<listadoCoincidentes> listadoCoincidentes { get; set; }
        //public Boolean existenMasCoincidentes { get; set; }


    }

    public class listadoCoincidentes 
    { 
        public string modulo { get; set; }
        public string id { get; set; }
        public string nombre { get; set; }
        public int? numero { get; set; }
    }



}
