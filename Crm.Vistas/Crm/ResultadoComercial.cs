﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
    public class ResultadoDameInfoComercial
    {
        [JsonProperty("codigoResultado")]
        public int codigoResultado { get; set; }

        [JsonProperty("mensajeResultado")]
        public object mensajeResultado { get; set; }

        [JsonProperty("listadoUsuarios")]
        public List<UsuarioVisual> listadoUsuarios { get; set; }
    }


    public class UsuarioVisual
    {
        [JsonProperty("nombreUsuario")]
        public string nombreUsuario { get; set; }

        [JsonProperty("emailUsuario")]
        public string emailUsuario { get; set; }

        [JsonProperty("nombreResponsable")]
        public string nombreResponsable { get; set; }

        [JsonProperty("emailResponsable")]
        public string emailResponsable { get; set; }
    }

   
}
