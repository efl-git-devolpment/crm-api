﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
   

    public class Emails
    {
        public string email { get; set; }
    }

    public class InfoCuenta
    {
        public string id_cuenta { get; set; }
        public int num_kojak { get; set; }
        public string cod_navision { get; set; }
        public int cod_evolution { get; set; }
        public string cod_indicator { get; set; }
        public string gran_cuenta { get; set; }
        public string tipo_cuenta { get; set; }
        public string tipo_cliente { get; set; }
        public string statut { get; set; }
        public string razon_social { get; set; }
        public string tratamiento { get; set; }
        public string nombre { get; set; }
        public string apellido_1 { get; set; }
        public string apellido_2 { get; set; }
        public string origen_web { get; set; }
        public string descripcion { get; set; }
        public string unidad_negocio { get; set; }
        public string venta_internacional { get; set; }
        public string fecha_creacion { get; set; }
        public string fecha_modificacion { get; set; }
        public string telefono_ppal { get; set; }
        public string telefono_2 { get; set; }
        public string telefono_3 { get; set; }
        public string telefono_movil { get; set; }
        public string fax { get; set; }
        public List<string> emails { get; set; }
        public string web { get; set; }
        public string twitter { get; set; }
        public string tipo_documento { get; set; }
        public string documento { get; set; }
        public string departamento { get; set; }
        public string departamento_mkt { get; set; }
        public string via { get; set; }
        public string poblacion { get; set; }
        public string provincia { get; set; }
        public string cp { get; set; }
        public string pais { get; set; }
        public string tipo_via { get; set; }
        public string complemento { get; set; }
        public string resto { get; set; }
        public string numero { get; set; }
        public string latitud { get; set; }
        public string longitud { get; set; }
        public string direccion_google { get; set; }
        public int num_empleados { get; set; }
        public string fecha_colegiacion { get; set; }
        public string ejerciente { get; set; }
        public string actividad { get; set; }
        public string sector_especialidad { get; set; }
        public string colectivo { get; set; }
        public string origen { get; set; }
        public string cargo { get; set; }
        public string n0_colegiado { get; set; }
        public string forma_juridica { get; set; }

        public List<string> acciones_comerciales { get; set; }
        public bool tiene_expediente_baja_en_gestion { get; set; }
        public string expediente_baja_en_gestion { get; set; }
        public string agente_expediente_baja_en_gestion { get; set; }

    }

    public class ResultadoObtenerInfoCuenta
    {
        [JsonProperty("codigoResultado")]
        public int codigoResultado { get; set; }
        [JsonProperty("mensajeResultado")]
        public string mensajeResultado { get; set; }
        [JsonProperty("cuenta")]
        public InfoCuenta cuenta { get; set; }

    }

}
