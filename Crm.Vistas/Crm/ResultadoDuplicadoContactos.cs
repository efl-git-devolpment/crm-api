﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
    public class ContactoDuplicado
    {
        public string ID_CONTACTO { get; set; }
        public int NUM_CRM { get; set; }
        public string NOMBRE { get; set; }
        public string APELLIDO_1 { get; set; }
        public string APELLIDO_2 { get; set; }
        public string EMAIL { get; set; }
        public bool BLOQUEO { get; set; }
        public int ESPEJO { get; set; }
    }

    public class ResultadoDuplicadoContactos
    {
        [JsonProperty("codigoResultado")]
        public int codigoResultado { get; set; }
        [JsonProperty("mensajeResultado")]
        public string mensajeResultado { get; set; }

        [JsonProperty("contactos")]
        public List<ContactoDuplicado> lstContactos { get; set; }

    }
}
