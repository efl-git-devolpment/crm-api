﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
    public class TipoCasoSacCrm
    {
        public string id { get; set; }
        public string nombre_tipo { get; set; }
        public string descripcion_tipo { get; set; }
        public string tipo { get; set; }
        public string motivo { get; set; }
        public string nombre_motivo { get; set; }
        public string submotivo { get; set; }
        public string nombre_submotivo { get; set; }
        public string descripcion_submotivo { get; set; }
        public string nombre_descripcion_submotivo { get; set; }
        public string estado_caso { get; set; }
        public string detalle_gestion { get; set; }
        public string resultado { get; set; }
    }
}
