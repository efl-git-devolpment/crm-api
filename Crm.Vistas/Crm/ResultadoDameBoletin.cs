﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
    public class LineaBoletin
    {
        public string cod_producto { get; set; }
        public string nombre_producto { get; set; }
        public string variante { get; set; }
        public string tipo_producto { get; set; }
        public int renovable { get; set; }
        public int unidades { get; set; }
        public double precio { get; set; }
        public double iva { get; set; }
        public double importe_iva { get; set; }
        public double descuento { get; set; }
        public double importe_descuento { get; set; }
        public double base_imponible { get; set; }
    }


    public class ContactoAdministradorCrm
    {
        public int id_contacto { get; set; }
        public string nombre_completo { get; set; }
        public string email_contacto { get; set; }
    }
    public class CabeceraBoletin
    {
        public int num_boletin { get; set; }
        public string num_boletin_nav { get; set; }
        public string nombre_boletin { get; set; }
        public ContactoAdministradorCrm contacto_administrador { get; set; }
        public string estado { get; set; }
        public string forma_pago { get; set; }
        public string medio_pago { get; set; }
        public double subtotal { get; set; }
        public double importe_iva { get; set; }
        public double total { get; set; }
    }
    public class BoletinInfo
    {
        [JsonProperty("cabecera")]
        public CabeceraBoletin cabecera { get; set; }

        [JsonProperty("lineas")]
        public List<LineaBoletin> lstLineas { get; set; }

    }

    public class ResultadoDameBoletinInfo
    {
        [JsonProperty("codigoResultado")]
        public string codigoResultado { get; set; }
        [JsonProperty("mensajeResultado")]
        public BoletinInfo mensajeResultado { get; set; }

    }

}
