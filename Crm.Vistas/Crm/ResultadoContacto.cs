﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
    public class ResultadoContacto
    {
        [JsonProperty("codigoRespuesta")]
        public string codigoRespuesta { get; set; }

        [JsonProperty("mensajeRespuesta")]
        public object mensajeRespuesta { get; set; }

        [JsonProperty("num_sugar")]
        public string num_sugar { get; set; }
    }

    public class ResultadoContacto_MensajeRespuestaKO
    {
        public string ID_EXTERNO { get; set; }
        public string ERROR { get; set; }
    }

    public class ResultadoContactoKO
    {
        [JsonProperty("codigoRespuesta")]
        public string codigoRespuesta { get; set; }

        [JsonProperty("mensajeRespuesta")]
        public List<ResultadoContacto_MensajeRespuestaKO> mensajeRespuesta { get; set; }

        [JsonProperty("num_sugar")]
        public string num_sugar { get; set; }
    }

   
}
