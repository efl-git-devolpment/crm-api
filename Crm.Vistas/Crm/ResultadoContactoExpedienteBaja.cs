﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Crm
{
    public class ResultadoContactoExpedienteBaja
    {
        public int codigoResultado { get; set; }
        public string num_expediente_baja { get; set; }
        public string apellido_1 { get; set; }
        public string apellido_2 { get; set; }
        public Dictionary<string,string> emails { get; set; }
    }
}


