﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Pedidos
{
    public class TiposPedidos
    {
        public bool codigoResultado { get; set; }
        public List<mensajeResultadoTipoPedido> mensajeResultado { get; set; }
    }
    public class mensajeResultadoTipoPedido
    {
        public int clave { get; set; }
        public string valor { get; set; }
    }
}
