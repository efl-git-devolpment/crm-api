﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.LibreriaVirtual
{
    public class UsuariosIndicator
    {
        public int IdUsuarioIndicator { get; set; }
        public string Email { get; set; }
        public string CifNif { get; set; }
        public string Nombre { get; set; }
        public string NombreEmpresa { get; set; }
        public string Apellidos { get; set; }
        public string Tlf { get; set; }
        public string DirNombre { get; set; }
        public string DirCpostal { get; set; }
        public string DirLocalidad { get; set; }
        public string IdProvincia { get; set; }
        public string DirIdPais { get; set; }
        public int? IdClienteCrm { get; set; }
        public int? IdContactoCrm { get; set; }
    }
}
