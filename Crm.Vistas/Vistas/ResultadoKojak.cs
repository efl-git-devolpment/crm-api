﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Vistas
{
    public class ResultadoKojak
    {
        public int codigoResultado { get; set; }
        public string mensajeResultado { get; set; }
        public string tipoClienteLead { get; set; }
        public string idClienteLead { get; set; }
        public int? numKojakClienteLead { get; set; }
    }
}
