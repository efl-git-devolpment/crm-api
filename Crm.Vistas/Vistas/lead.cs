﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Vistas.Vistas
{
    public class lead
    {
        public string origen_solicitud { get; set; }
        public string CIF_NIF { get; set; }
        public string LEAD_POSTALCODE { get; set; }
        public string LEAD_PHONE_HOME { get; set; }
        public string LEAD_MAIL { get; set; }
        public string LEAD_COMPANY_NAME { get; set; }
        public string LEAD_FIRST_NAME { get; set; }
        public string LEAD_LAST_NAME_1 { get; set; }
        public string LEAD_LAST_NAME_2 { get; set; }
        public string LEAD_ADDRESS { get; set; }
        public string ADDRESS_STATE_ID { get; set; }
        public string LEAD_PHONE_MOBILE { get; set; }
        public int? EXTERNAL_CODE { get; set; }
        public string ACTIVITY_ID { get; set; }
        public string AC_CODE { get; set; }
        public string LEAD_PRODUCT_ID { get; set; }
        public string LEAD_PRODUCT_NAME { get; set; }
        public string LEAD_PRODUCT_ID_DEMO { get; set; }
        public string LEAD_PRODUCT_NAME_DEMO { get; set; }
        public string COLECTIVO { get; set; }
        public string PROFESSION { get; set; }
        public string DEPARTMENT { get; set; }
        public string INTEREST_AREAS { get; set; }
        public int? EMPLOYEES_NUMBER { get; set; }
        public string REQUEST { get; set; }
        public string OBSERVATIONS { get; set; }
        public bool IND_PUBLICIDAD { get; set; }
        public bool? IND_CRM { get; set; }
        public string ID_ANALYTICS { get; set; }
        public string BUSINESS_UNIT_CODE { get; set; }
        public string STATUT_CODE { get; set; }
        public string REQUEST_DATE { get; set; }
        public string ORIGEN_WEB { get; set; }
        public int? NUM_CUENTA_CRM { get; set; }
        public string COMENTARIOS_CLIENTE { get; set; }
        public string COMPETIDOR { get; set; }
        public string FAMILIA { get; set; }
        public string GASTO { get; set; }
        public string GRATUITO { get; set; }
        public string FECHA_FIN { get; set; }
        public string ACCION_COMERCIAL_ORIGEN { get; set; }
        public string MEDIO { get; set; }
        public string FUENTE { get; set; }
    }
}
