﻿using DllApiBase.Comun.Configuracion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Configuration
{
    public class ConfigApi : ConfigApiBase
    {
        /// <summary>
        /// Contiene la parte relacionada con lo tokens JWT
        /// </summary>
        public OptJwt Jwt { get; set; }
        public OptServices Services { get; set; }
        /// <summary>
        /// Contiene las URLS con las que se comunica la aplicación
        /// </summary>
        public OptUrls Urls { get; set; }

        public string strPermisoGestor { get; set; }
    }
}
