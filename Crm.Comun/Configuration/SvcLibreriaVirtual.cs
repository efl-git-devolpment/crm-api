﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Configuration
{
    public class SvcLibreriaVirtual
    {
        /// <summary>
        /// Dominio o ruta del servicio
        /// </summary>
        public string Domain { get; set; }
    }
}
