﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Configuration
{
    public class OptClaims
    {
        /// <summary>
        ///   idClienteNavision, se utiliza para mandar el id de la empresa
        /// </summary>
        public string idClienteNavision { get; set; }
        /// <summary>
        ///   correo electrónico, se utiliza para mandar entradas navision
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// entrada de usuario de navision en el formato EXXXXXXXX
        /// </summary>
        public string id { get; set; }

        /// <summary>
        ///   tiempo de espiración
        /// </summary>
        public string exp { get; set; }

        /// <summary>
        ///   filtros para incluir en las llamadas
        /// </summary>
        public string filters { get; set; }
    }
}
