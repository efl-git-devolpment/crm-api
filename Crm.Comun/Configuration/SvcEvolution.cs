﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Configuration
{
    /// <summary>
    /// Clase para comunicarse con el sistema de Evolution
    /// </summary>
    public class SvcEvolution
    {
        /// <summary>
        /// Dominio o ruta del servicio
        /// </summary>
        public string Domain { get; set; }

        public string CargaLeads { get; set; }
        public string ResultadoLeadsSinCuenta { get; set; }
        public string CargaContactoEnCrm { get; set; }
    }
}
