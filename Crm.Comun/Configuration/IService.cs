﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Configuration
{
    /// <summary>
    /// Interfaz común a cualquier servicio
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// Dominio o ruta del servicio
        /// </summary>
        string Domain { get; set; }

        /// <summary>
        /// Método que permite la autenticación en el servicio
        /// </summary>
        string Login { get; set; }

    }
}
