﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Configuration
{
    public class OptJwt
    {
        /// <summary>
        ///   clave principal
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        ///   poseedor por defecto de la clave
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        ///   Dominio donde es válida la clave
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        ///   usuario
        /// </summary>
        public string User { get; set; }

        /// <summary>
        ///   usuario de entrada en la aplicación
        /// </summary>
        public string Login { get; set; }


        /// <summary>
        ///   tiempo que se suma en segundos para generar el token
        /// </summary>
        public long Caducidad { get; set; }

        /// <summary>
        ///   conjunto de claims
        /// </summary>
        public OptClaims Claims { get; set; }
    }
}
