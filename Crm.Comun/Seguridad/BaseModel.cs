﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Seguridad
{
    /// <summary>
    ///   Modelo de usuario para trabajar con los Tokens
    /// </summary>
    public class BaseModel
    {

        /// <summary>
        ///   Fecha de espiración del token en UNIX TimeStamp. Normalmente ahora + 60 segundos. Cada token será valido durante 60 segundos desde su generación.
        /// </summary>
        public long exp { get; set; }


    }
}
