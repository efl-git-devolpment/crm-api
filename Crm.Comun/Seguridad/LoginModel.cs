﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Seguridad
{
    /// <summary>
    ///   entidad para trabajar con contraseña y usuario
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        ///   nombre de usuario, entrada navision, o correo electrónico
        /// </summary>
        public string EntradaUsuario { get; set; }

        ///// <summary>
        /////   contraseña de usuario
        ///// </summary>
        //public string Password { get; set; }
    }
}
