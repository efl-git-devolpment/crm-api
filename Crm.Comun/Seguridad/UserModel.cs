﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Seguridad
{
    /// <summary>
    ///   Modelo de usuario para trabajar con los Tokens
    /// </summary>
    public class UserModel : BaseModel
    {

        /// <summary>
        /// entrada de usuario de navision en el formato EXXXXXXXX
        /// </summary>
        public string idUsuarioProducto { get; set; }

    }

    /// <summary>
    /// Modelo para creación de clientes en LeXon
    /// </summary>
    public class ClienteModel : BaseModel
    {
        /// <summary>
        ///  Identificador de navision del cliente que se ha creado
        /// </summary>
        public string idClienteNavision { get; set; }

    }
}
