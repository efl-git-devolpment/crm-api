﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Exceptions
{
    /// <summary>
    /// Excepción para controlar el error genérico en el BACKEND -> -2
    /// </summary>
    public class DatabaseException : Exception
    {
        /// <summary>
        /// Constructor básico
        /// </summary>
        public DatabaseException()
        { }

        /// <summary>
        /// Constructor con mensaje de error
        /// </summary>
        /// <param name="messsage"></param>
        public DatabaseException(string messsage) : base(messsage)
        { }

        /// <summary>
        /// Constructor con mensaje de error y manteniendo datos de la excepción original
        /// </summary>
        /// <param name="messsage"></param>
        /// <param name="inner"></param>
        public DatabaseException(string messsage, Exception inner) : base(messsage, inner)
        { }
    }
}
