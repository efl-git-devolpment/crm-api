﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Exceptions
{
    /// <summary>
    /// Excepción para controlar el elemento repetido en el BACKEND -> -3
    /// </summary>
    public class RepeatItemException : Exception
    {
        /// <summary>
        /// Constructor básico
        /// </summary>
        public RepeatItemException()
        { }

        /// <summary>
        /// Constructor con mensaje de error
        /// </summary>
        /// <param name="messsage"></param>
        public RepeatItemException(string messsage) : base(messsage)
        { }

        /// <summary>
        /// Constructor con mensaje de error y manteniendo datos de la excepción original
        /// </summary>
        /// <param name="messsage"></param>
        /// <param name="inner"></param>
        public RepeatItemException(string messsage, Exception inner) : base(messsage, inner)
        { }
    }
}
