﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Exceptions
{
    /// <summary>
    /// Excepción para controlar el error de los parámetros de entrada en el BACKEND -> -3
    /// </summary>
    public class InputParametersException : Exception
    {
        /// <summary>
        /// Constructor básico
        /// </summary>
        public InputParametersException()
        { }

        /// <summary>
        /// Constructor con mensaje de error
        /// </summary>
        /// <param name="messsage"></param>
        public InputParametersException(string messsage) : base(messsage)
        { }

        /// <summary>
        /// Constructor con mensaje de error y manteniendo datos de la excepción original
        /// </summary>
        /// <param name="messsage"></param>
        /// <param name="inner"></param>
        public InputParametersException(string messsage, Exception inner) : base(messsage, inner)
        { }
    }
}
