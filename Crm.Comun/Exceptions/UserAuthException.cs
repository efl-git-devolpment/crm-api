﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Comun.Exceptions
{
    public class UserAuthException : Exception
    {
        /// <summary>
        /// Constructor básico
        /// </summary>
        public UserAuthException()
        { }

        /// <summary>
        /// Constructor con mensaje de error
        /// </summary>
        /// <param name="messsage"></param>
        public UserAuthException(string messsage) : base(messsage)
        { }

        /// <summary>
        /// Constructor con mensaje de error y manteniendo datos de la excepción original
        /// </summary>
        /// <param name="messsage"></param>
        /// <param name="inner"></param>
        public UserAuthException(string messsage, Exception inner) : base(messsage, inner)
        { }
    }
}
