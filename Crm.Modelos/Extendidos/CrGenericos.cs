﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Modelos.Extendidos
{
    public class CrListaGenerica<C>
    {

        public int iContador { get; set; }
        public List<C> lstListado { get; set; }
    }
}
