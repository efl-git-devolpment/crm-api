﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Modelos.Extendidos
{
    public class CrContactoModificacion
    {
        public int IdContactoCrm { get; set; }
        public int NumTdcSugar { get; set; }
        public int OrigenModule { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string CifNif { get; set; }
        public string Pais { get; set; }
        public string IdProvincia { get; set; }
        public string Provincia { get; set; }
        public string Poblacion { get; set; }
        public string CP { get; set; }
        public string Via { get; set; }
        public string Numero { get; set; }
        public string Resto { get; set; }
        public int IndLv { get; set; }
    }
}
