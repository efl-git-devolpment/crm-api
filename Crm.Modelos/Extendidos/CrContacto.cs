﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Modelos.Extendidos
{
    public class CrContacto
    {
        public int IdClienteCrm { get; set; }
        public int IdContactoCrm { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Tlf { get; set; }
        public string Email { get; set; }
        public string CifNif { get; set; }
        public short indEspejo { get; set; }
    }

    public class CrContactoListadoExt
    {
        public int contador { get; set; }

        public IEnumerable<CrContacto> Listado { get; set; }

    }

    public class CrContactoListado : CrListaGenerica<CrContacto>
    {

    }
}
