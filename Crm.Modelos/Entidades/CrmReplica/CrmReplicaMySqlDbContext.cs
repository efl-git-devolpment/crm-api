﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Crm.Modelos.Entidades.CrmReplica
{
    public partial class CrmReplicaMySqlDbContext : DbContext
    {
        public CrmReplicaMySqlDbContext()
        {
        }

        public CrmReplicaMySqlDbContext(DbContextOptions<CrmReplicaMySqlDbContext> options)
            : base(options)
        {
        }

        //public virtual DbSet<ProductTypes> ProductTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ProductTypes>(entity =>
            //{
            //    entity.ToTable("product_types");

            //    entity.HasIndex(e => new { e.Name, e.Deleted })
            //        .HasName("idx_producttypes");

            //    entity.Property(e => e.Id)
            //        .HasColumnName("id")
            //        .HasColumnType("char(36)");

            //    entity.Property(e => e.CreatedBy)
            //        .HasColumnName("created_by")
            //        .HasColumnType("char(36)");

            //    entity.Property(e => e.DateEntered)
            //        .HasColumnName("date_entered")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.DateModified)
            //        .HasColumnName("date_modified")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Deleted)
            //        .HasColumnName("deleted")
            //        .HasColumnType("tinyint(1)")
            //        .HasDefaultValueSql("'0'");

            //    entity.Property(e => e.Description)
            //        .HasColumnName("description")
            //        .HasColumnType("text");

            //    entity.Property(e => e.ListOrder).HasColumnName("list_order");

            //    entity.Property(e => e.ModifiedUserId)
            //        .HasColumnName("modified_user_id")
            //        .HasColumnType("char(36)");

            //    entity.Property(e => e.Name)
            //        .HasColumnName("name")
            //        .HasColumnType("varchar(50)");
            //});
        }
    }
}
