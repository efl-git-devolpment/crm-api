﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Crm.Modelos.Entidades.Busquedas
{
    public partial class CrmBusquedasMySqlDbContext : DbContext
    {
        public CrmBusquedasMySqlDbContext()
        {
        }

        public CrmBusquedasMySqlDbContext(DbContextOptions<CrmBusquedasMySqlDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {}
    }
}
