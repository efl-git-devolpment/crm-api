﻿using Crm.Modelos.Entidades.Crm;
using Crm.Modelos.Extendidos;
using Crm.Modelos.Repositorios.Interfaces;
using DllApiBase.Modelos;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Crm.Modelos.Repositorios
{
    public class ContactosRepository : ModeloApiBase<ContactosRepository>, IContactosRepository<MySqlConnection, CrContactoListadoExt, CrContactoCreacion, CrContactoModificacion>
    {
        private readonly CrmMySqlDbContext _CrmMySqlcontext;
        private readonly ILogger<ContactosRepository> _logger;

        public ContactosRepository(
            CrmMySqlDbContext CrmMySqlcontext,
            ILogger<ContactosRepository> logger
            )
        {
            _logger = logger;
            _CrmMySqlcontext = CrmMySqlcontext;
        }

        //12/09/24 --> No se han encontrado llamadas en el log
        /// <summary>
        ///   Obtiene el numero de bases de datos disponibles
        /// </summary>
        /// <returns></returns>
        public async Task<CrContactoListadoExt> RecuperarCuentaContactosByNavisionAsync(
            int IdClienteNav,
            string Search,
            int PageSize,
            int PageNum,
            short indEspejo,
            MySqlConnection conn)
        {
            _logger.LogInformation("START -->{0}", nameof(RecuperarCuentaContactosByNavisionAsync));

            //MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
            //conn.ConnectionString = _CrmMySqlcontext.Database.GetDbConnection().ConnectionString;

            var listaItems = new List<CrContacto>();
            var totalResult = 0;
            using (var command = conn.CreateCommand())
            {
                var outParam = new MySqlParameter() { ParameterName = "P_ERROR", DbType = DbType.String, Direction = ParameterDirection.Output };
                var TotalRegParam = new MySqlParameter() { ParameterName = "P_TOTAL_REG", DbType = DbType.Int16, Direction = ParameterDirection.Output };

                command.CommandText = "PROC_RECUPERAR_CUENTA_CONTACTOS_BY_NAVISION";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new MySqlParameter("P_COD_CUENTA_NAVISION_C", IdClienteNav));
                command.Parameters.Add(new MySqlParameter("P_SEARCH", Search));
                command.Parameters.Add(new MySqlParameter("P_PAGE_SIZE", PageSize));
                command.Parameters.Add(new MySqlParameter("P_PAGE_NUMBER", PageNum));
                command.Parameters.Add(outParam);
                command.Parameters.Add(TotalRegParam);
                TraceCommandStoreProcedure(_logger, command.CommandText, command.Parameters);
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            var row = new CrContacto
                            {
                                IdClienteCrm = (int)ObtenerCampoInteger(reader, "ID_CLIENTE_CRM"),
                                IdContactoCrm = (int)ObtenerCampoInteger(reader, "ID_CONTACTO_CRM"),
                                Nombre = ObtenerCampoString(reader, "NOMBRE"),
                                Apellido1 = ObtenerCampoString(reader, "APELLIDO1"),
                                Apellido2 = ObtenerCampoString(reader, "APELLIDO2"),
                                Tlf = ObtenerCampoString(reader, "TLF"),
                                Email = ObtenerCampoString(reader, "EMAIL"),
                                //CifNif = ObtenerCampoString(reader, "CIF_NIF"),
                                indEspejo = indEspejo
                            };

                            listaItems.Add(row);
                        }
                    }
                }
                totalResult = (short)command.Parameters["P_TOTAL_REG"].Value;

            }
            _logger.LogInformation("END -->{0} ", nameof(RecuperarCuentaContactosByNavisionAsync));
            return new CrContactoListadoExt
            {
                contador = totalResult,
                Listado = listaItems
            };
        }



        /// <summary>
        ///   Obtiene el numero de bases de datos disponibles
        /// </summary>
        /// <returns></returns>
        public async Task<CrContactoListadoExt> RecuperarCuentaContactosAsync(
            int IdClienteCrm,
            string Search,
            int PageSize,
            int PageNum,
            short indEspejo,
            MySqlConnection conn)
        {
            _logger.LogInformation("START -->{0}", nameof(RecuperarCuentaContactosAsync));

            //MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
            //conn.ConnectionString = _CrmMySqlcontext.Database.GetDbConnection().ConnectionString;

            var listaItems = new List<CrContacto>();
            var totalResult = 0;
            using (var command = conn.CreateCommand())
            {
                var outParam = new MySqlParameter() { ParameterName = "P_ERROR", DbType = DbType.String, Direction = ParameterDirection.Output };
                var TotalRegParam = new MySqlParameter() { ParameterName = "P_TOTAL_REG", DbType = DbType.Int16, Direction = ParameterDirection.Output };

                command.CommandText = "PROC_RECUPERAR_CUENTA_CONTACTOS";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new MySqlParameter("P_NUM_TDC_SUGAR_C", IdClienteCrm));
                command.Parameters.Add(new MySqlParameter("P_SEARCH", Search));
                command.Parameters.Add(new MySqlParameter("P_PAGE_SIZE", PageSize));
                command.Parameters.Add(new MySqlParameter("P_PAGE_NUMBER", PageNum));
                command.Parameters.Add(outParam);
                command.Parameters.Add(TotalRegParam);
                TraceCommandStoreProcedure(_logger, command.CommandText, command.Parameters);
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            var row = new CrContacto
                            {
                                IdClienteCrm = (int)ObtenerCampoInteger(reader, "ID_CLIENTE_CRM"),
                                IdContactoCrm = (int)ObtenerCampoInteger(reader, "ID_CONTACTO_CRM"),
                                Nombre = ObtenerCampoString(reader, "NOMBRE"),
                                Apellido1 = ObtenerCampoString(reader, "APELLIDO1"),
                                Apellido2 = ObtenerCampoString(reader, "APELLIDO2"),
                                Tlf = ObtenerCampoString(reader, "TLF"),
                                Email = ObtenerCampoString(reader, "EMAIL"),
                                //CifNif = ObtenerCampoString(reader, "CIF_NIF"),
                                indEspejo = indEspejo
                            };

                            listaItems.Add(row);
                        }
                    }
                }
                totalResult = (short)command.Parameters["P_TOTAL_REG"].Value;

            }
            _logger.LogInformation("END -->{0} ", nameof(RecuperarCuentaContactosAsync));
            return new CrContactoListadoExt
            {
                contador = totalResult,
                Listado = listaItems
            };
        }


        //12/09/24 --> Solo se ha encontrado una llamada el 30/08/24 y da error la petición No parece que se esté usando.
        public async Task<int> AddAsync(CrContactoCreacion entity, MySqlConnection conn)
        {
            int NoClient = 0;
            var outParam = new MySqlParameter() { ParameterName = "P_ERROR", DbType = DbType.String, Direction = ParameterDirection.Output };
            using (var command = conn.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "PROC_CREAR_CONTACTO_FROM_CLAVES";
                command.Parameters.Add(new MySqlParameter("P_NO_CLIENT", entity.NoClient));
                command.Parameters.Add(new MySqlParameter("P_NUM_TDC_SUGAR_C", entity.NumTdcSugar));
                command.Parameters.Add(new MySqlParameter("P_ORIGEN_MODULE", entity.OrigenModule));
                //command.Parameters.Add(new MySqlParameter("P_CIF_NIF", entity.CifNif));
                command.Parameters.Add(new MySqlParameter("P_NOMBRE", entity.Nombre));
                command.Parameters.Add(new MySqlParameter("P_APELLIDO_1", entity.Apellido1));
                command.Parameters.Add(new MySqlParameter("P_APELLIDO_2", entity.Apellido2));
                command.Parameters.Add(new MySqlParameter("P_TELEFONO", entity.Telefono));
                command.Parameters.Add(new MySqlParameter("P_TLFMOVIL", entity.TlfMovil));
                command.Parameters.Add(new MySqlParameter("P_TLF_EMPRESA", entity.TlfEmpresa));
                command.Parameters.Add(new MySqlParameter("P_FAX", entity.Fax));
                command.Parameters.Add(new MySqlParameter("P_EMAIL", entity.Email));
                command.Parameters.Add(new MySqlParameter("P_PAIS", entity.Pais));
                command.Parameters.Add(new MySqlParameter("P_ID_PROVINCIA", entity.IdProvincia));
                command.Parameters.Add(new MySqlParameter("P_PROVINCIA", entity.Provincia));
                command.Parameters.Add(new MySqlParameter("P_POBLACION", entity.Poblacion));
                command.Parameters.Add(new MySqlParameter("P_CP", entity.CP));
                command.Parameters.Add(new MySqlParameter("P_TIPO_VIA", entity.TipoVia));
                command.Parameters.Add(new MySqlParameter("P_VIA", entity.Via));
                command.Parameters.Add(new MySqlParameter("P_NUMERO", entity.Numero));
                command.Parameters.Add(new MySqlParameter("P_RESTO", entity.Resto));
                command.Parameters.Add(new MySqlParameter("P_ESPEJO", 0));
                command.Parameters.Add(new MySqlParameter("P_THIRD_P_CUSTOMER", entity.ThirdPCustomer));

                command.Parameters.Add(outParam);
                TraceCommandStoreProcedure(_logger, command.CommandText, command.Parameters);
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            NoClient = (int)ObtenerCampoInteger(reader, "NO_CLIENT");
                        }
                    }
                }
            }

            return NoClient;
        }

        //12/09/24 --> No se han encontrado llamadas en el log
        public async Task<int> UpdateAsync(CrContactoModificacion entity, MySqlConnection conn)
        {
            int NoClient = 0;
            var outParam = new MySqlParameter() { ParameterName = "P_ERROR", DbType = DbType.String, Direction = ParameterDirection.Output };
            using (var command = conn.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "PROC_MODIFICAR_CONTACTO_FROM_CLAVES";
                command.Parameters.Add(new MySqlParameter("P_ID_CONTACTO_CRM", entity.IdContactoCrm));
                command.Parameters.Add(new MySqlParameter("P_NUM_TDC_SUGAR_C", entity.NumTdcSugar));
                command.Parameters.Add(new MySqlParameter("P_ORIGEN_MODULE", entity.OrigenModule));
                //command.Parameters.Add(new MySqlParameter("P_CIF_NIF", entity.CifNif));
                command.Parameters.Add(new MySqlParameter("P_NOMBRE", entity.Nombre));
                command.Parameters.Add(new MySqlParameter("P_APELLIDO_1", entity.Apellido1));
                command.Parameters.Add(new MySqlParameter("P_APELLIDO_2", entity.Apellido2));
                command.Parameters.Add(new MySqlParameter("P_TELEFONO", entity.Telefono));
                command.Parameters.Add(new MySqlParameter("P_EMAIL", entity.Email));
                command.Parameters.Add(new MySqlParameter("P_PAIS", entity.Pais));
                command.Parameters.Add(new MySqlParameter("P_ID_PROVINCIA", entity.IdProvincia));
                command.Parameters.Add(new MySqlParameter("P_PROVINCIA", entity.Provincia));
                command.Parameters.Add(new MySqlParameter("P_POBLACION", entity.Poblacion));
                command.Parameters.Add(new MySqlParameter("P_CP", entity.CP));
                command.Parameters.Add(new MySqlParameter("P_VIA", entity.Via));
                command.Parameters.Add(new MySqlParameter("P_NUMERO", entity.Numero));
                command.Parameters.Add(new MySqlParameter("P_RESTO", entity.Resto));
                command.Parameters.Add(new MySqlParameter("P_IND_LV", entity.IndLv));

                command.Parameters.Add(outParam);
                TraceCommandStoreProcedure(_logger, command.CommandText, command.Parameters);
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            NoClient = (int)ObtenerCampoInteger(reader, "NO_CLIENT");
                        }
                    }
                }
            }

            return NoClient;
        }
    }
}
