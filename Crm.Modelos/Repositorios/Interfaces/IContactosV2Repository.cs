﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Modelos.Repositorios.Interfaces
{
    public interface IContactosV2Repository<CC, CM, CCL>
    {
        Task<int> CrearContacto(CC objContacto);

        Task<int> ModificarContacto(CM objContacto);

        Task<CCL> RecuperarContactosByClienteCrm(int IdClienteCrm, string strBusqueda, int iPaginaInicial, int iNumeroDeFilas);


    }
}
