﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Modelos.Repositorios.Interfaces
{
    public interface IContactosRepository<T,U,V, W>
    {
        Task<U> RecuperarCuentaContactosByNavisionAsync(
            int IdClienteNav,
            string Search,
            int PageSize,
            int PageNum,
            short indEspejo,
            T conn);


        Task<U> RecuperarCuentaContactosAsync(
            int IdClienteCrm,
            string Search,
            int PageSize,
            int PageNum,
            short indEspejo,
            T conn);

        Task<int> AddAsync(V entity, T conn);

        Task<int> UpdateAsync(W entity, T conn);
    }
}
