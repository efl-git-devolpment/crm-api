﻿using Crm.Comun.Exceptions;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;

namespace Crm.Modelos.Repositorios
{
    public class BaseRepository<T>
    {
        private ILogger<T> _logger;

        public static int AnalizarRespuestaOk(SqlParameter res, string errMsg)
        {
            var respuesta = -9;
            if (res == null || res.Value == null)
                throw new DatabaseException(errMsg);

            respuesta = (int)res.Value;

            return AnalizarRespuestaIntOk(respuesta, errMsg);
        }

        public static int AnalizarRespuestaIntOk(int respuesta, string errMsg)
        {
            if (respuesta == -1)
                throw new InputParametersException(errMsg);

            if (respuesta == -2)
                throw new DatabaseException(errMsg);

            if (respuesta == -3)
                throw new RepeatItemException(errMsg);

            return respuesta;
        }

        public static void ValidarUsuario(int? idUic)
        {
            if (idUic == null || idUic <= 0)
                throw new UserAuthException("El usuario autenticado no es válido, compruebe si no ha caducado la sesión");
        }


        internal static void TraceStoreProcedure(ILogger<T> _logger, string nameStoreProcedure, object[] paramsOne)
        {
            var builder = new System.Text.StringBuilder($"EXEC {nameStoreProcedure} ");
            foreach (var parameter in paramsOne)
            {
                if (parameter is SqlParameter)
                {
                    var paramsql = (SqlParameter)parameter;
                    object value = ObtenerValorParametro(paramsql);
                    builder.Append($"{paramsql.ParameterName} = {value}, ");
                }
            }
            builder.Remove(builder.Length - 2, 2);
            builder.Append(";");
            _logger.LogInformation(builder.ToString());
        }

        internal static void TraceStoreProcedureMySql(ILogger<T> _logger, string nameStoreProcedure, object[] paramsOne)
        {
            var builder = new System.Text.StringBuilder($"CALL {nameStoreProcedure} (");
            foreach (var parameter in paramsOne)
            {
                if (parameter is MySqlParameter)
                {
                    var paramsql = (MySqlParameter)parameter;
                    if (paramsql.ParameterName == "P_ERROR" || paramsql.ParameterName == "P_TOTAL_REG")
                    {
                        builder.Append($" @{paramsql.ParameterName}, ");
                    }
                    else
                    {
                        object value = ObtenerValorParametro(paramsql);
                        builder.Append($" {value}, ");
                    }
                }
            }
            builder.Remove(builder.Length - 2, 2);
            builder.Append(")");
            _logger.LogInformation(builder.ToString());
        }

        internal static void TraceCommandStoreProcedure(ILogger<T> _logger, string commandText, DbParameterCollection parameters)
        {
            var builder = new System.Text.StringBuilder($"EXEC {commandText} ");
            foreach (var parameter in parameters)
            {
                if (parameter is DbParameter)
                {
                    var paramsql = (DbParameter)parameter;
                    object value = ObtenerValorParametro(paramsql);
                    builder.Append($"{paramsql.ParameterName} = {value}, ");
                }
            }
            builder.Remove(builder.Length - 2, 2);
            builder.Append(";");
            _logger.LogInformation(builder.ToString());
        }

        internal static void TraceCommandStoreProcedure(ILogger<T> _logger, string commandText, MySqlParameterCollection parameters)
        {
            var builder = new System.Text.StringBuilder($"CALL {commandText} (");
            foreach (var parameter in parameters)
            {
                if (parameter is MySqlParameter)
                {
                    var paramsql = (MySqlParameter)parameter;
                    if (paramsql.ParameterName == "P_ERROR" || paramsql.ParameterName == "P_TOTAL_REG")
                    {
                        builder.Append($" @{paramsql.ParameterName}, ");
                    }
                    else
                    {
                        object value = ObtenerValorParametro(paramsql);
                        builder.Append($" {value}, ");
                    }

                }
            }
            builder.Remove(builder.Length - 2, 2);
            builder.Append(")");
            _logger.LogInformation(builder.ToString());
        }

        private static object ObtenerValorParametro(DbParameter paramsql)
        {
            return paramsql.DbType == System.Data.DbType.String
                        || paramsql.DbType == System.Data.DbType.Boolean
                        || paramsql.DbType == System.Data.DbType.DateTime
                        || paramsql.DbType == System.Data.DbType.DateTime2
                            ? paramsql.Value == null ? "NULL" : $"'{paramsql.Value}'"
                            : paramsql.Value;
        }

        private static object ObtenerValorParametro(MySqlParameter paramsql)
        {
            return paramsql.DbType == System.Data.DbType.String
                        || paramsql.DbType == System.Data.DbType.Boolean
                        || paramsql.DbType == System.Data.DbType.DateTime
                        || paramsql.DbType == System.Data.DbType.DateTime2
                            ? paramsql.Value == null ? "NULL" : $"'{paramsql.Value}'"
                            : paramsql.Value;
        }


        internal int ObtenerCampoInteger(DbDataReader reader, string nombre)
        {
            var ordinal = 0;
            var resultado = 0;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(Int16) && !reader.IsDBNull(ordinal)
                    ? reader.GetInt16(ordinal)
                    : type != null && type == typeof(Int32) && !reader.IsDBNull(ordinal)
                        ? reader.GetInt32(ordinal)
                        : -1;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }


        internal int ObtenerCampoInteger(MySqlDataReader reader, string nombre)
        {
            var ordinal = 0;
            var resultado = 0;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(Int16) && !reader.IsDBNull(ordinal)
                    ? reader.GetInt16(ordinal)
                    : type != null && type == typeof(Int32) && !reader.IsDBNull(ordinal)
                        ? reader.GetInt32(ordinal)
                        : -1;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }



        internal int ObtenerCampoInteger64(DbDataReader reader, string nombre)
        {
            var ordinal = 0;
            var resultado = 0;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(Int64) && !reader.IsDBNull(ordinal)
                    ? reader.GetInt16(ordinal)
                    : type != null && type == typeof(Int64) && !reader.IsDBNull(ordinal)
                        ? reader.GetInt32(ordinal)
                        : -1;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }

        internal int ObtenerCampoInteger64(MySqlDataReader reader, string nombre)
        {
            var ordinal = 0;
            var resultado = 0;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(Int64) && !reader.IsDBNull(ordinal)
                    ? reader.GetInt16(ordinal)
                    : type != null && type == typeof(Int64) && !reader.IsDBNull(ordinal)
                        ? reader.GetInt32(ordinal)
                        : -1;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }

        internal decimal ObtenerCampoDecimal(DbDataReader reader, string nombre)
        {
            var ordinal = 0;
            decimal resultado = 0;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(decimal)
                    ? reader.GetDecimal(ordinal)
                    : (short)-1;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }

        internal decimal ObtenerCampoDecimal(MySqlDataReader reader, string nombre)
        {
            var ordinal = 0;
            decimal resultado = 0;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(decimal)
                    ? reader.GetDecimal(ordinal)
                    : (short)-1;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }

        internal short ObtenerCampoShort(DbDataReader reader, string nombre)
        {
            var ordinal = 0;
            short resultado = 0;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(short)
                    ? reader.GetInt16(ordinal)
                    : (short)-1;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }


        internal short ObtenerCampoShort(MySqlDataReader reader, string nombre)
        {
            var ordinal = 0;
            short resultado = 0;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(short)
                    ? reader.GetInt16(ordinal)
                    : (short)-1;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }

        internal bool ObtenerCampoBool(DbDataReader reader, string nombre)
        {
            var ordinal = 0;
            var resultado = false;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(bool)
                    ? reader.GetBoolean(ordinal)
                    : false;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }


        internal bool ObtenerCampoBool(MySqlDataReader reader, string nombre)
        {
            var ordinal = 0;
            var resultado = false;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(bool)
                    ? reader.GetBoolean(ordinal)
                    : false;
            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }

        internal string ObtenerCampoString(DbDataReader reader, string nombre)
        {
            var ordinal = 0;
            var resultado = "";
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(string) && !reader.IsDBNull(ordinal) ? reader.GetString(ordinal) : resultado;

            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }


        internal string ObtenerCampoString(MySqlDataReader reader, string nombre)
        {
            var ordinal = 0;
            var resultado = "";
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(string) && !reader.IsDBNull(ordinal) ? reader.GetString(ordinal) : resultado;

            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }


        internal DateTime? ObtenerCampoDatetime(DbDataReader reader, string nombre)
        {
            var ordinal = 0;
            DateTime? resultado = null;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(DateTime) && !reader.IsDBNull(ordinal) ? reader.GetDateTime(ordinal) : resultado;

            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }

        internal DateTime? ObtenerCampoDatetime(MySqlDataReader reader, string nombre)
        {
            var ordinal = 0;
            DateTime? resultado = null;
            Type type;
            try
            {
                ordinal = reader.GetOrdinal(nombre);
                type = reader.GetFieldType(ordinal);

                resultado = type != null && type == typeof(DateTime) && !reader.IsDBNull(ordinal) ? reader.GetDateTime(ordinal) : resultado;

            }
            catch (InvalidCastException exGet)
            {
                _logger.LogError("Error {1} --> al obtener el tipo en el campo {0}", nombre, exGet.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error {1} --> al obtener el campo {0}", nombre, ex.Message);
            }

            return resultado;
        }

        internal string convertirFechaBD(DateTime date)
        {
            string strDate = "";
            strDate = date.Year + "/" + date.Month + "/" + date.Day + " " + date.Hour + ":" + date.Minute + ":" + date.Second;

            return strDate;
        }
    }
}
