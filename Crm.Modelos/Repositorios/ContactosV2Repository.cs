﻿using Crm.Modelos.Entidades.Crm;
using Crm.Modelos.Extendidos;
using Crm.Modelos.Repositorios.Interfaces;
using DllApiBase.Modelos;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;


namespace Crm.Modelos.Repositorios
{
    public class ContactosV2Repository : ModeloApiBase<ContactosV2Repository>, IContactosV2Repository<CrContactoCreacion, CrContactoModificacion, CrContactoListado>
    {
        private readonly ILogger<ContactosV2Repository> objLogger;
        private readonly CrmMySqlDbContext objDbContextCrm;

        public ContactosV2Repository(
            CrmMySqlDbContext objDbContextCrmBase,
            ILogger<ContactosV2Repository> objLoggerBase
            )
        {
            objLogger = objLoggerBase;
            objDbContextCrm = objDbContextCrmBase;
        }


        public async Task<int> CrearContacto(CrContactoCreacion objContacto)
        {
            string strDescripcion = String.Empty;
            int iResultado = -1;

            var objConnection = objDbContextCrm.Database.GetDbConnection();

            try
            {
                await objConnection.OpenAsync();

                var outParam = new MySqlParameter() { ParameterName = "P_ERROR", DbType = DbType.String, Direction = ParameterDirection.Output };


                using (var command = objConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "PROC_CREAR_CONTACTO_FROM_CLAVES";
                    command.Parameters.Add(new MySqlParameter("P_NO_CLIENT", objContacto.NoClient));
                    command.Parameters.Add(new MySqlParameter("P_NUM_TDC_SUGAR_C", objContacto.NumTdcSugar));
                    command.Parameters.Add(new MySqlParameter("P_ORIGEN_MODULE", objContacto.OrigenModule));
                    command.Parameters.Add(new MySqlParameter("P_CIF_NIF", objContacto.CifNif));
                    command.Parameters.Add(new MySqlParameter("P_NOMBRE", objContacto.Nombre));
                    command.Parameters.Add(new MySqlParameter("P_APELLIDO_1", objContacto.Apellido1));
                    command.Parameters.Add(new MySqlParameter("P_APELLIDO_2", objContacto.Apellido2));
                    command.Parameters.Add(new MySqlParameter("P_TELEFONO", objContacto.Telefono));
                    command.Parameters.Add(new MySqlParameter("P_TLFMOVIL", objContacto.TlfMovil));
                    command.Parameters.Add(new MySqlParameter("P_TLF_EMPRESA", objContacto.TlfEmpresa));
                    command.Parameters.Add(new MySqlParameter("P_FAX", objContacto.Fax));
                    command.Parameters.Add(new MySqlParameter("P_EMAIL", objContacto.Email));
                    command.Parameters.Add(new MySqlParameter("P_PAIS", objContacto.Pais));
                    command.Parameters.Add(new MySqlParameter("P_ID_PROVINCIA", objContacto.IdProvincia));
                    command.Parameters.Add(new MySqlParameter("P_PROVINCIA", objContacto.Provincia));
                    command.Parameters.Add(new MySqlParameter("P_POBLACION", objContacto.Poblacion));
                    command.Parameters.Add(new MySqlParameter("P_CP", objContacto.CP));
                    command.Parameters.Add(new MySqlParameter("P_TIPO_VIA", objContacto.TipoVia));
                    command.Parameters.Add(new MySqlParameter("P_VIA", objContacto.Via));
                    command.Parameters.Add(new MySqlParameter("P_NUMERO", objContacto.Numero));
                    command.Parameters.Add(new MySqlParameter("P_RESTO", objContacto.Resto));
                    command.Parameters.Add(new MySqlParameter("P_ESPEJO", 0));
                    command.Parameters.Add(new MySqlParameter("P_THIRD_P_CUSTOMER", objContacto.ThirdPCustomer));

                    command.Parameters.Add(outParam);
                    TraceCommandStoreProcedure(objLogger, command.CommandText, command.Parameters);
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (await reader.ReadAsync())
                            {
                                iResultado = (int)ObtenerCampoInteger((MySqlDataReader)reader, "NO_CLIENT");
                            }

                            strDescripcion = "Se añade un contacto nuevo en bbdd.";
                        }
                        else
                        {
                            strDescripcion = "No se añade ningún contacto nuevo.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error -->{0} Mensaje-->{1}", nameof(CrearContacto), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(CrearContacto), ex.Message);
                strDescripcion = ex.Message;
            }
            finally
            {
                objConnection.Close();
            }

            objLogger.LogInformation("END -->{0} Mensaje-->{1}", nameof(CrearContacto), strDescripcion);

            return iResultado;
        }


        public async Task<int> ModificarContacto(CrContactoModificacion objContacto)
        {
            string strDescripcion = String.Empty;
            int iResultado = -1;

            var objConnection = objDbContextCrm.Database.GetDbConnection();

            try
            {
                await objConnection.OpenAsync();

                var outParam = new MySqlParameter() { ParameterName = "P_ERROR", DbType = DbType.String, Direction = ParameterDirection.Output };

                using (var command = objConnection.CreateCommand())
                {

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "PROC_MODIFICAR_CONTACTO_FROM_CLAVES";
                    command.Parameters.Add(new MySqlParameter("P_ID_CONTACTO_CRM", objContacto.IdContactoCrm));
                    command.Parameters.Add(new MySqlParameter("P_NUM_TDC_SUGAR_C", objContacto.NumTdcSugar));
                    command.Parameters.Add(new MySqlParameter("P_ORIGEN_MODULE", objContacto.OrigenModule));
                    command.Parameters.Add(new MySqlParameter("P_CIF_NIF", objContacto.CifNif));
                    command.Parameters.Add(new MySqlParameter("P_NOMBRE", objContacto.Nombre));
                    command.Parameters.Add(new MySqlParameter("P_APELLIDO_1", objContacto.Apellido1));
                    command.Parameters.Add(new MySqlParameter("P_APELLIDO_2", objContacto.Apellido2));
                    command.Parameters.Add(new MySqlParameter("P_TELEFONO", objContacto.Telefono));
                    command.Parameters.Add(new MySqlParameter("P_EMAIL", objContacto.Email));
                    command.Parameters.Add(new MySqlParameter("P_PAIS", objContacto.Pais));
                    command.Parameters.Add(new MySqlParameter("P_ID_PROVINCIA", objContacto.IdProvincia));
                    command.Parameters.Add(new MySqlParameter("P_PROVINCIA", objContacto.Provincia));
                    command.Parameters.Add(new MySqlParameter("P_POBLACION", objContacto.Poblacion));
                    command.Parameters.Add(new MySqlParameter("P_CP", objContacto.CP));
                    command.Parameters.Add(new MySqlParameter("P_VIA", objContacto.Via));
                    command.Parameters.Add(new MySqlParameter("P_NUMERO", objContacto.Numero));
                    command.Parameters.Add(new MySqlParameter("P_RESTO", objContacto.Resto));
                    command.Parameters.Add(new MySqlParameter("P_IND_LV", objContacto.IndLv));

                    command.Parameters.Add(outParam);
                    TraceCommandStoreProcedure(objLogger, command.CommandText, command.Parameters);
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (await reader.ReadAsync())
                            {
                                iResultado = (int)ObtenerCampoInteger((MySqlDataReader)reader, "NO_CLIENT");
                            }

                            strDescripcion = "Se actualiza el contacto en bbdd.";
                        }
                        else
                        {
                            strDescripcion = "No se actualiza el contacto en bbdd.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error -->{0} Mensaje-->{1}", nameof(ModificarContacto), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(ModificarContacto), ex.Message);
                strDescripcion = ex.Message;
            }
            finally
            {
                objConnection.Close();
            }

            objLogger.LogInformation("END -->{0} Mensaje-->{1}", nameof(ModificarContacto), strDescripcion);

            return iResultado;
        }

        /// <summary>
        /// Recupera Contactos por Cliente Crm
        /// </summary>
        /// <returns></returns>
        public async Task<CrContactoListado> RecuperarContactosByClienteCrm(int IdClienteCrm, string strBusqueda, int iPaginaInicial, int iNumeroDeFilas)
        {
            objLogger.LogInformation("START -->{0} Parámetros-->{1},{2},{3},{4}", nameof(RecuperarContactosByClienteCrm), IdClienteCrm.ToString(), strBusqueda, iPaginaInicial.ToString(), iNumeroDeFilas.ToString());


            var objResultado = new CrContactoListado();
            var lstContactos = new List<CrContacto>();
            var intTotal = 0;

            string strDescripcion = String.Empty;

            var objConnection = objDbContextCrm.Database.GetDbConnection();

            try
            {
                await objConnection.OpenAsync();

                using (var command = objConnection.CreateCommand())
                {
                    var outParam = new MySqlParameter() { ParameterName = "P_ERROR", DbType = DbType.String, Direction = ParameterDirection.Output };
                    var TotalRegParam = new MySqlParameter() { ParameterName = "P_TOTAL_REG", DbType = DbType.Int16, Direction = ParameterDirection.Output };

                    command.CommandText = "PROC_RECUPERAR_CUENTA_CONTACTOS";
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new MySqlParameter("P_NUM_TDC_SUGAR_C", IdClienteCrm));
                    command.Parameters.Add(new MySqlParameter("P_SEARCH", strBusqueda));
                    command.Parameters.Add(new MySqlParameter("P_PAGE_SIZE", iNumeroDeFilas));
                    command.Parameters.Add(new MySqlParameter("P_PAGE_NUMBER", iPaginaInicial));
                    command.Parameters.Add(outParam);
                    command.Parameters.Add(TotalRegParam);
                    TraceCommandStoreProcedure(objLogger, command.CommandText, command.Parameters);
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (await reader.ReadAsync())
                            {
                                var row = new CrContacto
                                {
                                    IdClienteCrm = (int)ObtenerCampoInteger(reader, "ID_CLIENTE_CRM"),
                                    IdContactoCrm = (int)ObtenerCampoInteger(reader, "ID_CONTACTO_CRM"),
                                    Nombre = ObtenerCampoString(reader, "NOMBRE"),
                                    Apellido1 = ObtenerCampoString(reader, "APELLIDO1"),
                                    Apellido2 = ObtenerCampoString(reader, "APELLIDO2"),
                                    Tlf = ObtenerCampoString(reader, "TLF"),
                                    Email = ObtenerCampoString(reader, "EMAIL"),
                                    CifNif = ObtenerCampoString(reader, "CIF_NIF"),
                                    indEspejo = (short)ObtenerCampoInteger(reader, "IND_ESPEJO")
                                };

                                lstContactos.Add(row);
                                strDescripcion = "Se recupera contactos de BBDD.";
                            }
                        }
                        else
                        {
                            strDescripcion = "No se recupera ningún contacto en BBDD";
                        }


                    }

                    intTotal = (short)TotalRegParam.Value;


                }
                objResultado.iContador = intTotal;
                objResultado.lstListado = lstContactos;
            }
            catch (Exception ex)
            {

                objLogger.LogInformation("Error -->{0} Mensaje-->{1}", nameof(RecuperarContactosByClienteCrm), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarContactosByClienteCrm), ex.Message);
                strDescripcion = ex.Message;
            }
            finally
            {
                objConnection.Close();
            }

            objLogger.LogInformation("END -->{0} Mensaje-->{1}", nameof(RecuperarContactosByClienteCrm), strDescripcion);
            return objResultado;
        }



    }
}
