﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.IO;
using System;
using DllApiBase.WebApi.Logging;
using Crm.Comun.Configuration;
using Crm.Modelos.Entidades.Busquedas;
using Crm.Modelos.Entidades.Crm;
using Crm.Modelos.Entidades.CrmReplica;
using Crm.WebApi.Extensions;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using DllApiBase.Vistas;
using System.Linq;
using Swashbuckle.AspNetCore.SwaggerUI;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Http;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((ctx, lc) => lc.ReadFrom.Configuration(ctx.Configuration));
var connectionStringCrm = builder.Configuration.GetConnectionString("CrmMySql");
builder.Services.AddDbContext<CrmMySqlDbContext>(options => options.UseMySql(connectionStringCrm, ServerVersion.AutoDetect(connectionStringCrm)));
var connectionStringCrmReplica = builder.Configuration.GetConnectionString("CrmReplicaMySql");
builder.Services.AddDbContext<CrmReplicaMySqlDbContext>(options =>
    options.UseMySql(connectionStringCrmReplica, ServerVersion.AutoDetect(connectionStringCrmReplica)));
var connectionStringCrmBusquedas = builder.Configuration.GetConnectionString("CrmBusquedasMySql");
builder.Services.AddDbContext<CrmBusquedasMySqlDbContext>(options =>
    options.UseMySql(connectionStringCrmBusquedas, ServerVersion.AutoDetect(connectionStringCrmBusquedas)));
builder.Services.RegisterServices();
builder.Services.AddOptions();
builder.Services.Configure<ConfigApi>(builder.Configuration);
builder.Services.AddCors();
builder.Services.RegisterAuth(builder.Configuration);
builder.Services.AddMvc(
     config =>
     {
         config.Filters.Add(typeof(CustomExceptionFilter));
         config.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
         config.EnableEndpointRouting = false;
     }
 );
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.Configure<ApiBehaviorOptions>(apiBehaviorOptions => apiBehaviorOptions.InvalidModelStateResponseFactory = actionContext =>
{
    return new BadRequestObjectResult(new
    {
        eResultado = enumResultado.Error_Parametros,
        sDescripcion = $"Error en los parámetros: {string.Join('/', actionContext.ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage))}"
    });
});
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v3", new OpenApiInfo
    {
        Version = "v3.01.04",
        Title = "API Crm",
        Description = "API que nos permite operar con Crm",
        TermsOfService = new Uri("http://www.lefebvre.es"),
        Contact = new OpenApiContact
        {
            Name = "Lefebvre.es",
            Url = new Uri("http://www.lefebvre.es"),
        },
        License = new OpenApiLicense
        {
            Name = "Propiedad de Lefevbre - El Derecho",
            Url = new Uri("http://www.lefebvre.es")
        }
    });
    var basePath = AppContext.BaseDirectory;
    var xmlPath = Path.Combine(basePath, "Crm.WebApi.xml");
    c.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

app.Use((context, next) =>
{
    context.Request.Path = new PathString("/").Add(context.Request.Path.Value.Replace("//", "/"));
    return next();
});

app.UseRequestLogging();
if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v3/swagger.json", "Api Crm V3.01.04");
    c.RoutePrefix = @"api";
    c.DocExpansion(DocExpansion.None);
});
app.UseAuthentication();
app.UseMvc();



app.Run();