﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Pedidos;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.ComponentModel.DataAnnotations;

namespace Crm.WebApi.Controllers
{
    [ApiController]
    [Route("Pedidos")]
    public class PedidosController : ControllerApiBase
    {
        private readonly ILogger<PedidosController> _logger;
        private readonly IPedidosService<ResultadoBase> svc;

        /// <summary>
        /// Constructor al que se inyectan los servicios aplicables
        /// </summary>
        /// <param name="service">servicio para tratar listas</param>
        /// <param name="logger">servicio de log</param>
        /// <param name="configAccessor"></param>
        public PedidosController(
            IPedidosService<ResultadoBase> service,
            ILogger<PedidosController> logger,
            IOptions<ConfigApi> configAccessor
            ) : base(configAccessor)
        {
            svc = service;
            _logger = logger;
        }


        /// <summary>
        /// Este metodo devuelve si se puede crear un asistente o no
        /// </summary>
        /// <param name="intClasificacion">Id de clasificacion de kojak (1: Electronico, 2:Papel)</param>
        /// <returns></returns>
        [Route("ComprobarPermisoCreacionAsistente")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<TiposPedidos>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult ComprobarPermisoCreacionAsistente(
            [FromQuery]
            [Required(ErrorMessage = "Debe rellenar el campo intClasificacion.")]
            [Range(1,2, ErrorMessage = "El valor de intClasificacion debe ser 1 o 2.")]
            int? intClasificacion)
        {
            ResultadoOperacion<TiposPedidos> objResultado = new ResultadoOperacion<TiposPedidos>();
            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<TiposPedidos>)svc.RecuperarTipoPedido(intClasificacion.Value);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }
    }
}
