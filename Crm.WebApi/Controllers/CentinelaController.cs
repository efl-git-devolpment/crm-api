﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Kojak;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crm.WebApi.Controllers
{
    [Route("Centinela")]
    public class CentinelaController : ControllerApiBase
    {
        private readonly ILogger<CentinelaController> objLogger;
        private readonly ICentinelaService<ResultadoBase> objService;

        /// <summary>
        /// Constructor al que se inyectan los servicios aplicables
        /// </summary>
        /// <param name="objServiceBase">servicio para tratar listas</param>
        /// <param name="objLoggerBase">servicio de log</param>
        /// <param name="configAccessor"></param>
        public CentinelaController(
            ICentinelaService<ResultadoBase> objServiceBase,
            ILogger<CentinelaController> objLoggerBase,
            IOptions<ConfigApi> configAccessor
            ) : base(configAccessor)
        {
            objService = objServiceBase;
            objLogger = objLoggerBase;
        }

        /// <summary>
        /// Recupera los datos del superadministrador de centinela
        /// </summary>
        /// <param name="IdBoletin">Id de boletin</param>
        /// <param name="IdLineaBoletin">Id linea boletin</param>
        /// <returns></returns>
        [Route("RecuperarSuperAdministradorCentinela/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<SuperAdministradorCentinela>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarSuperAdministradorCentinela(
            [FromQuery] int IdBoletin, 
            [FromQuery] int IdLineaBoletin)
        {
            ResultadoOperacion<SuperAdministradorCentinela> objResultado = new ResultadoOperacion<SuperAdministradorCentinela>();

            if (IdBoletin <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdBoletin";
            }
            else if (IdLineaBoletin < 0)
            {

                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdLineaBoletin";
            }
            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<SuperAdministradorCentinela>)objService.RecuperarSuperAdministradorCentinela(IdBoletin, IdLineaBoletin);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
  
        }

        /// <summary>
        ///  Recupera los datos del superadministrador de centinela en presupuestos
        /// </summary>
        /// <param name="IdExpediente">Id del Expediente devuelto por Navision</param>
        /// <param name="IdProductoNav">id del producto Navision</param>
        /// <param name="Variante">Variante</param>
        /// <returns></returns>
        [Route("RecuperarSuperAdministradorCentinelaPresupuesto/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<SuperAdministradorCentinela>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarSuperAdministradorCentinelaPresupuesto(
           [FromQuery] int IdExpediente,
           [FromQuery] string IdProductoNav,
           [FromQuery] string Variante)
        {
            ResultadoOperacion<SuperAdministradorCentinela> objResultado = new ResultadoOperacion<SuperAdministradorCentinela>();

            if (IdExpediente <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdExpediente";

            }
            else if (string.IsNullOrWhiteSpace(IdProductoNav))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdProductoNav";

            }

            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<SuperAdministradorCentinela>)objService.RecuperarSuperAdministradorCentinelaPresupuesto(IdExpediente, IdProductoNav, Variante);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
  
        }
    }
}
