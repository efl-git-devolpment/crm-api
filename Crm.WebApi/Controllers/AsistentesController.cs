﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Asistentes;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Crm.WebApi.Controllers
{
    [Route("Asistentes")]
    public class AsistentesController : ControllerApiBase
    {
        private readonly ILogger<AsistentesController> _logger;
        private readonly IAsistentesService<ResultadoBase> svc;

        /// <summary>
        /// Constructor al que se inyectan los servicios aplicables
        /// </summary>
        /// <param name="service">servicio para tratar listas</param>
        /// <param name="logger">servicio de log</param>
        /// <param name="configAccessor"></param>
        public AsistentesController(
            IAsistentesService<ResultadoBase> service,
            ILogger<AsistentesController> logger,
            IOptions<ConfigApi> configAccessor
            ) : base(configAccessor)
        {
            svc = service;
            _logger = logger;
        }

        //POSIBLE DESUSO --> 12/09/2024 --> este método no ha recibido peticiones
        /// <summary>
        /// Este metodo devuelve si se puede crear un asistente o no
        /// POSIBLE DESUSO
        /// </summary>
        /// <param name="IdClienteCrmFacturacion"></param>
        /// <param name="IdClienteCrmEnvio"></param>
        /// <param name="IdContactoCrm"></param>
        /// <param name="strEmailContacto"></param>
        /// <returns></returns>
        [Route("ComprobarPermisoCreacionAsistente")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<PermitirAsistente>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult ComprobarPermisoCreacionAsistente(
            [FromQuery] int IdClienteCrmFacturacion,
            [FromQuery] int IdClienteCrmEnvio,
            [FromQuery] int IdContactoCrm,
            [FromQuery] string strEmailContacto)
        {
            ResultadoOperacion<PermitirAsistente> objResultado = new ResultadoOperacion<PermitirAsistente>();
            if (IdClienteCrmFacturacion == 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdClienteCrmFacturacion";
            }
            if (IdContactoCrm == 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdContactoCrm";
            }
            if (string.IsNullOrWhiteSpace(strEmailContacto))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo strEmailContacto";
            }

            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<PermitirAsistente>)svc.ComprobarPermisoCreacionAsistente(
                                                                            IdClienteCrmFacturacion: IdClienteCrmFacturacion,
                                                                            IdClienteCrmEnvio: IdClienteCrmEnvio,
                                                                            IdContactoCrm: IdContactoCrm,
                                                                            strEmailContacto: strEmailContacto);
                objResultado.sDescripcion = objResultado.oResultado.codigoResultado ? "La comprobación ha sido correcta." : "La comprobación ha sido incorrecta";
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }
    }
}
