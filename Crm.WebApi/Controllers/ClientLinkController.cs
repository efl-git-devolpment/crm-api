﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Kojak;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crm.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("ClientLink")]
    public class ClientLinkController : ControllerApiBase
    {
        private readonly ILogger<ClientLinkController> objLogguer;
        private readonly IClientLinkService<ResultadoBase> objService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objServiceBase"></param>
        /// <param name="objLogguerBase"></param>
        /// <param name="configAccessor"></param>
        public ClientLinkController(
             IClientLinkService<ResultadoBase> objServiceBase,
             ILogger<ClientLinkController> objLogguerBase,
             IOptions<ConfigApi> configAccessor
             ) : base(configAccessor)
        {
            objService = objServiceBase;
            objLogguer = objLogguerBase;
        }

        //POSIBLE DESUSO --> 12/09/24 --> No se han encontrado logs a esta llamada.
        ///LLAMADA QUE TIENE QUE SER UTILIZADA DESDE EL METODO CreateClientLinkFromProcessAsync DEL API DE CLIENT LINK Y QUITAR EL WEB SERVICE
        /// <summary>
        ///  Recupera la informacion de kojak de lproducto client link
        ///  POSIBLE DESUSO
        /// </summary>
        /// <param name="IdBoletin">Id del boletin</param>
        /// <param name="IdLineaBoletin">Id de la linea del boletin</param>
        /// <returns></returns>
        [Route("RecuperarInfoClientLinkBoletin/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ProductoClientLinkExt>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarInfoClientLinkBoletin(
            [FromQuery] int IdBoletin,
            [FromQuery] int IdLineaBoletin)
        {
            ResultadoOperacion<ProductoClientLinkExt> objResultado = new ResultadoOperacion<ProductoClientLinkExt>();

            if (IdBoletin <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdBoletin";
            }
            else if (IdLineaBoletin < 0)
            {

                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdLineaBoletin";
            }
            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ProductoClientLinkExt>)objService.RecuperarInfoClientLinkBoletin(IdBoletin, IdLineaBoletin);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

        //POSIBLE DESUSO --> 12/09/24 --> No se han encontrado logs a esta llamada.
        /// <summary>
        /// POSIBLE DESUSO
        /// </summary>
        /// <param name="IdExpediente">Id del Expediente devuelto por Navision</param>
        /// <param name="IdProductoNav">Id del producto Navision</param>
        /// <param name="Variante">Variante = UP</param>
        /// <returns></returns>
        [Route("RecuperarInfoClientLinkPresupuesto/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ProductoClientLinkExt>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarInfoClientLinkPresupuesto(
            [FromQuery] int IdExpediente,
            [FromQuery] string IdProductoNav,
            [FromQuery] string Variante)
        {
            ResultadoOperacion<ProductoClientLinkExt> objResultado = new ResultadoOperacion<ProductoClientLinkExt>();

            if (IdExpediente <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdExpediente";

            }
            else if (string.IsNullOrWhiteSpace(IdProductoNav))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdProductoNav";

            }
            else if (string.IsNullOrWhiteSpace(Variante))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo Variante";

            }


            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ProductoClientLinkExt>)objService.RecuperarInfoClientLinkPresupuesto(IdExpediente, IdProductoNav, Variante);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

    }
}
