﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Vistas;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Crm.WebApi.Controllers
{
    [Route("Leads/")]
    [ApiController]
    public class LeadsController : ControllerApiBase
    {
        private readonly ILogger<LeadsController> objLogger;
        private readonly ILeadsService<ResultadoBase, lead> objServiceLeads;

        /// <summary>
        /// Constructor al que se inyectan los servicios aplicables
        /// </summary>
        /// <param name="objServiceLeadsBase">servicio para tratar listas</param>
        /// <param name="objLoggerBase">servicio de log</param>
        /// <param name="configAccessor"></param>
        public LeadsController(
           ILeadsService<ResultadoBase, lead> objServiceLeadsBase,
            ILogger<LeadsController> objLoggerBase,
            IOptions<ConfigApi> configAccessor
            ) : base(configAccessor)
        {
            objServiceLeads = objServiceLeadsBase;
            objLogger = objLoggerBase;
        }

        /// <summary>
        /// Guardar lead en kojak
        /// </summary>
        /// <param name="objLead">Lead</param>
        /// <param name="iIdLead">idLead</param>
        /// <returns></returns>
        [Route("GuardarLeadKojak/")]
        [HttpPost]
        [ProducesResponseType(typeof(ResultadoOperacion<ResultadoKojak>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GuardarLeadKojak([FromBody] lead objLead, [FromQuery] int iIdLead)
        {
            ResultadoOperacion<ResultadoKojak> objResultado = new ResultadoOperacion<ResultadoKojak>();
            if (objLead == null)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe ingresar los datos obligatorios: objLead.";
            }
            else if (iIdLead < 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe ingresar los datos obligatorios: iIdLead.";
            }
            if (string.IsNullOrEmpty(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ResultadoKojak>)await objServiceLeads.GuardarLeadKojak(objLead, iIdLead);
            }
            //objLogger.LogDebug("Resultado -->{0} Mensaje-->{1}", nameof(InsertarLeadsAterrizajesAsync), objResultado.sDescripcion);
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Recupera el resultado de un Lead
        /// </summary>
        /// <param name="iIdLead">id del Lead</param>
        /// <returns></returns>
        [Route("RecuperarResultadoLeadsKojak/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ResultadoKojak>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarResultadoLeadsKojak(
            [FromQuery]
            [Required(ErrorMessage = "Debe ingresar los datos obligatorios: iIdLead.")]
            [Range(1, int.MaxValue, ErrorMessage = "iIdLead tiene que tener un valor superior a 0.")]
            int? iIdLead)
        {
            objLogger.LogDebug("Metodo -->{0} idLead-->{1}", nameof(RecuperarResultadoLeadsKojak), iIdLead);
            var objResultado = (ResultadoOperacion<ResultadoKojak>)await objServiceLeads.RecuperarResultadoLeadsKojak(iIdLead.Value);
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }
    }
}