﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Boletines;
using Crm.Vistas.Crm;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Crm.WebApi.Controllers
{
    [Route("Boletin")]
    public class BoletinController : ControllerApiBase
    {
        private readonly ILogger<BoletinController> objLogger;
        private readonly IBoletinesV2Service<ResultadoBase> objServiceBoletin;

        /// <summary>
        /// Constructor al que se inyectan los servicios aplicables
        /// </summary>
        /// <param name="objServiceBoletinBase">servicio para tratar listas</param>
        /// <param name="objLoggerBase">servicio de log</param>
        /// <param name="objConfigBase"></param>
        public BoletinController(
            IBoletinesV2Service<ResultadoBase> objServiceBoletinBase,
            ILogger<BoletinController> objLoggerBase,
            IOptions<ConfigApi> objConfigBase
            ) : base(objConfigBase)
        {
            objServiceBoletin = objServiceBoletinBase;
            objLogger = objLoggerBase;
        }
        //12/09/2024 --> este método no ha recibido peticiones desde 22/06/2023
        /// <summary>
        /// Este metodo se utiliza para comprobar si se permite crear un boletin
        /// POSIBLE DESUSO
        /// </summary>
        /// <param name="IdClienteCrm">Codigo cliente crm</param>
        /// <param name="strOrigen">Es el origen que hay que pasar a kojak, para el simulador de ofertas por ejemplo es SDO</param>
        /// <param name="IdUsuarioCrm">Campo busqueda por Email</param>
        /// <returns></returns>
        [Route("ComprobarPermisoCreacionBoletin/")]
        [HttpGet]
        [ProducesResponseType(typeof(PermitirBoletin), 200)]
        [ProducesResponseType(typeof(void), 400)]
        public IActionResult ComprobarPermisoCreacionBoletin(
            [FromQuery] int IdClienteCrm,
            [FromQuery] string strOrigen,
            [FromQuery]string IdUsuarioCrm)
        {
            if (IdClienteCrm <= 0 || string.IsNullOrWhiteSpace(IdUsuarioCrm)|| string.IsNullOrWhiteSpace(strOrigen))
                return BadRequest();

            var okOperation = (ResultadoOperacion<PermitirBoletin>) objServiceBoletin.ComprobarPermisoCreacionBoletin(IdClienteCrm, strOrigen, IdUsuarioCrm);

            return new ObjectResult(okOperation.oResultado);
        }

        //POSIBLE DESUSO --> 12/09/2024 --> este método no ha recibido peticiones desde 22/06/2023
        /// <summary>
        /// Este metodo se utiliza para comprobar si se permite crear un boletin
        /// POSIBLE DESUSO
        /// </summary>
        /// <param name="IdClienteCrm">Codigo cliente crm</param>
        /// <param name="strOrigen">Es el origen que hay que pasar a kojak, para el simulador de ofertas por ejemplo es SDO</param>
        /// <param name="IdUsuarioCrm">Campo busqueda por Email</param>
        /// <returns></returns>
        [Route("ComprobarCrearBoletin/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<PermitirBoletin>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult ComprobarCrearBoletin(
            [FromQuery] int IdClienteCrm,
            [FromQuery] string strOrigen,
            [FromQuery] string IdUsuarioCrm)
        {

            ResultadoOperacion<PermitirBoletin> objResultado = new ResultadoOperacion<PermitirBoletin>();

            if (IdClienteCrm <= 0 || string.IsNullOrWhiteSpace(IdUsuarioCrm) || string.IsNullOrWhiteSpace(strOrigen))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: Hay parámetros que están vacios.";
            }
            else {
                objResultado = (ResultadoOperacion<PermitirBoletin>)objServiceBoletin.ComprobarPermisoCreacionBoletin(IdClienteCrm, strOrigen, IdUsuarioCrm);
          
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Recuperar información de un boletín
        /// </summary>
        /// <param name="iIdBoletin">Id del Boletín</param>
        /// <returns></returns>
        [Route("RecuperarBoletinInfo/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<BoletinInfo>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult RecuperarBoletinInfo(
            [FromQuery] int iIdBoletin)
        {

            ResultadoOperacion<BoletinInfo> objResultado = new ResultadoOperacion<BoletinInfo>();

            if (iIdBoletin <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: iIdBoletin tiene que ser mayor que cero.";
            }
            else
            {
                objResultado = (ResultadoOperacion<BoletinInfo>)objServiceBoletin.RecuperarBoletinInfo(iIdBoletin);

            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }
        ///// <summary>
        ///// Recuperar información de un boletín de Genial
        ///// </summary>
        ///// <param name="iIdBoletin">Id del Boletín</param>
        ///// <param name="iLineaBoletin">Línea de Boletín></param>
        ///// <returns></returns>
        //[Route("RecuperarBoletinGenialInfo/")]
        //[HttpGet]
        //[ProducesResponseType(typeof(ResultadoOperacion<BoletinInfo>), StatusCodes.Status200OK)]
        //[ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        //public IActionResult RecuperarBoletinInfo([FromQuery] int iIdBoletin, [FromQuery] int iLineaBoletin)
        //{

        //    ResultadoOperacion<BoletinGenial> objResultado = new ResultadoOperacion<BoletinGenial>();

        //    if ((iIdBoletin <= 0) ||(iLineaBoletin <=0))
        //    {
        //        objResultado.eResultado = enumResultado.Error_Parametros;
        //        objResultado.sDescripcion = "Error en parámetros: iIdBoletin y/o iLineaBoletin tiene que ser mayor que cero.";
        //    }
        //    else
        //    {
        //        objResultado = (ResultadoOperacion<BoletinGenial>)objServiceBoletin.RecuperarBoletinGenialInfo(iIdBoletin, iLineaBoletin);

        //    }

        //    return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        //}
    }
}