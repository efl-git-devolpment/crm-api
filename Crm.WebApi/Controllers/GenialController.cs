﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Crm;
using Crm.Vistas.Kojak;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Crm.WebApi.Controllers
{
    [Route("Genial")]
    public class GenialController : ControllerApiBase
    {
        private readonly ILogger<GenialController> objLogger;
        private readonly IGenialService objService;

        private readonly IBoletinesV2Service<ResultadoBase> objServiceBoletin;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objServiceBase"></param>
        /// <param name="objLoggerBase"></param>
        /// <param name="objConfigBase"></param>
        /// <param name="objServiceBoletinBase"></param>
        public GenialController(
             IGenialService objServiceBase,
             ILogger<GenialController> objLoggerBase,
             IBoletinesV2Service<ResultadoBase> objServiceBoletinBase,
             IOptions<ConfigApi> objConfigBase
             ) : base(objConfigBase)
        {
            objServiceBoletin = objServiceBoletinBase;
            objService = objServiceBase;
            objLogger = objLoggerBase;
        }

        /// <summary>
        /// Recupera la informacion de kojak del producto genial de boletines
        /// </summary>
        /// <param name="IdBoletin"></param>
        /// <param name="IdLineaBoletin"></param>
        /// <returns></returns>
        [Route("RecuperarInfoGenialBoletin/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ProductoGenialExt>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarInfoGenialBoletin(
            [FromQuery] int IdBoletin,
            [FromQuery] int IdLineaBoletin)

        {
            ResultadoOperacion<ProductoGenialExt> objResultado = new ResultadoOperacion<ProductoGenialExt>();

            if (IdBoletin <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdBoletin";
            }
            else if (IdLineaBoletin < 0)
            {

                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdLineaBoletin";
            }
            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ProductoGenialExt>)objService.RecuperarInfoGenialBoletin(IdBoletin, IdLineaBoletin);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }


        /// <summary>
        ///  Recupera la informacion de kojak del producto genial de presupuetos
        ///  POSIBLE DESUSO
        /// </summary>
        /// <param name="IdExpediente">Id del Expediente devuelto por Navision</param>
        /// <param name="IdProductoNav">Id del producto Navision</param>
        /// <param name="Variante"></param>
        /// <returns></returns>
        [Route("RecuperarInfoGenialPresupuesto/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ProductoGenialExt>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarInfoGenialPresupuesto(
            [FromQuery] int IdExpediente,
            [FromQuery] string IdProductoNav,
            [FromQuery] string Variante)
        {
            ResultadoOperacion<ProductoGenialExt> objResultado = new ResultadoOperacion<ProductoGenialExt>();

            if (IdExpediente <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdBoletin";

            }
            else if (string.IsNullOrWhiteSpace(IdProductoNav))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdProductoNav";

            }

            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ProductoGenialExt>)objService.RecuperarInfoGenialPresupuesto(IdExpediente, IdProductoNav, Variante);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

        /// <summary>
        /// Recuperar información de un boletín de Genial
        /// </summary>
        /// <param name="iIdBoletin">Id del Boletín</param>
        /// <param name="iLineaBoletin">Línea de Boletín</param>
        /// <returns></returns>
        [Route("RecuperarBoletinInfo/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<BoletinGenial>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult RecuperarBoletinInfo([FromQuery] int iIdBoletin, [FromQuery] int iLineaBoletin)
        {

            ResultadoOperacion<BoletinGenial> objResultado = new ResultadoOperacion<BoletinGenial>();

            if ((iIdBoletin <= 0) || (iLineaBoletin <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: iIdBoletin y/o iLineaBoletin tiene que ser mayor que cero.";
            }
            else
            {
                objResultado = (ResultadoOperacion<BoletinGenial>)objServiceBoletin.RecuperarBoletinGenialInfo(iIdBoletin, iLineaBoletin);

            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }


    }
}
