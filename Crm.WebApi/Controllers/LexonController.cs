﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Contactos;
using Crm.Vistas.Crm;
using Crm.Vistas.Kojak;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crm.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("Lexon")]
    public class LexonController : ControllerApiBase
    {
        private readonly ILogger<LexonController> objLogger;
        private readonly ILexonService<ResultadoBase> objService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objServiceBase"></param>
        /// <param name="objLoggerBase"></param>
        /// <param name="objConfigBase"></param>
        public LexonController(
             ILexonService<ResultadoBase> objServiceBase,
             ILogger<LexonController> objLoggerBase,
             IOptions<ConfigApi> objConfigBase
             ) : base(objConfigBase)
        {
            objService = objServiceBase;
            objLogger = objLoggerBase;
        }

        /// <summary>
        /// Recupera la informacion de kojak del producto lexon de boletines
        /// </summary>
        /// <param name="IdBoletin"></param>
        /// <param name="IdLineaBoletin"></param>
        /// <returns></returns>
        [Route("RecuperarInfoLexonBoletin/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ProductoLexonExt>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarInfoLexonBoletin(
            [FromQuery] int IdBoletin,
            [FromQuery] int IdLineaBoletin)

        {
            ResultadoOperacion<ProductoLexonExt> objResultado = new ResultadoOperacion<ProductoLexonExt>();

            if (IdBoletin <= 0) 
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdBoletin";
            }
            else if(IdLineaBoletin < 0)
            {

                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdLineaBoletin";
            }
            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ProductoLexonExt>) objService.RecuperarInfoLexonBoletin(IdBoletin, IdLineaBoletin);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        //POSIBLE DESUSO --> 12/09/24 No se han encontrado logs.
        /// <summary>
        ///  Recupera la informacion de kojak del producto lexon de presupuetos
        ///  POSIBLE DESUSO
        /// </summary>
        /// <param name="IdExpediente">Id del Expediente devuelto por Navision</param>
        /// <param name="IdProductoNav">Id del producto Navision</param>
        /// <param name="Variante">Variante = UP</param>
        /// <returns></returns>
        [Route("RecuperarInfoLexonPresupuesto/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ProductoLexonExt>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarInfoLexonPresupuesto(
            [FromQuery] int IdExpediente,
            [FromQuery] string IdProductoNav,
            [FromQuery] string Variante)
        {
            ResultadoOperacion<ProductoLexonExt> objResultado = new ResultadoOperacion<ProductoLexonExt>();



            if (IdExpediente <= 0) {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdExpediente";

            }
            else if (string.IsNullOrWhiteSpace(IdProductoNav))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdProductoNav";

            }
            else if (string.IsNullOrWhiteSpace(Variante))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo Variante";

            }

            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ProductoLexonExt>)objService.RecuperarInfoLexonPresupuesto(IdExpediente, IdProductoNav, Variante);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

        //POSIBLE DESUSO --> 12/09/24 No se han encontrado logs.
        /// <summary>
        ///  Método que devuelve datos basicos de contacto de Kojak
        ///  POSIBLE DESUSO
        /// </summary>
        /// <param name="IdClienteCRM">Id Kojak</param>
        /// <param name="NumSuscripcion">Número de la suscripción</param>
        /// <returns></returns>
        [Route("RecuperarDatosContactoExpedienteBaja/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ResultadoContactoExpedienteBaja>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarDatosContactoExpedienteBaja(
           [FromQuery] int? IdClienteCRM,
           [FromQuery] int? NumSuscripcion)
        {
            ResultadoOperacion<ResultadoContactoExpedienteBaja> objResultado = new ResultadoOperacion<ResultadoContactoExpedienteBaja>();

            if (IdClienteCRM <= 0 || IdClienteCRM == null)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdClienteCRM";

            }
            else if (NumSuscripcion <= 0 || NumSuscripcion == null)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo NumSuscripcion";

            }

            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ResultadoContactoExpedienteBaja>)objService.RecuperarDatosContactoExpedienteBaja(IdClienteCRM.Value, NumSuscripcion.Value);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }
    }
}
