﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Crm;
using DllApiBase.Servicio;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using DllApiBase.Vistas.Cliente;

namespace Crm.WebApi.Controllers
{
    [Route("Cuentas")]
    public class CuentasController : ControllerApiBase
    {
        private readonly ILogger<CuentasController> objLogger;
        private readonly ICuentasService objServiceCuentas;

        private readonly ICuentasV2Service<ResultadoBase> objServiceCuentasV2;


        /// <summary>
        /// Constructor al que se inyectan los servicios aplicables
        /// </summary>
        /// <param name="objServiceCuentasBase">servicio para gestionar cuentas en CRM</param>
        /// <param name="objServiceCuentasV2Base">servicio para gestionar cuentas en CRM refactorizdo</param>
        /// <param name="objLoggerBase">servicio de log</param>
        /// <param name="objConfigBase"></param>
        public CuentasController(
            ICuentasService objServiceCuentasBase,
            ICuentasV2Service<ResultadoBase> objServiceCuentasV2Base,
            ILogger<CuentasController> objLoggerBase,
            IOptions<ConfigApi> objConfigBase
            ) : base(objConfigBase)
        {
            objServiceCuentas = objServiceCuentasBase;
            objServiceCuentasV2 = objServiceCuentasV2Base;
            objLogger = objLoggerBase;    
        }



        /// <summary>
        ///   Carga cuentas indicator
        /// </summary>
        /// <returns></returns>
        [Route("CargarCuentasIndicator/")]
        [HttpPost]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(void), 400)]
        public async Task<ActionResult> CargarCuentasIndicatorAsync()
        {

            int okOperation = await objServiceCuentas.CargarCuentasIndicatorAsync();

            return okOperation >= 0 ? (ActionResult)new ObjectResult(okOperation) : BadRequest();
        }

        /// <summary>
        /// Método para recuperar información de una cuenta, por código Navision o código Crm
        /// </summary>
        /// <param name="IdClienteCrm">Código cliente Crm</param>
        /// <param name="IdClienteNav">Código cliente Navision</param>
        /// <returns></returns>
        [Route("RecuperarInformacionCuenta/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<InfoCuenta>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> RecuperarInformacionCuenta(
            [FromQuery] int? IdClienteCrm,
            [FromQuery] int? IdClienteNav)
        {

            ResultadoOperacion<InfoCuenta> objResultado = new ResultadoOperacion<InfoCuenta>();

            if ((IdClienteCrm == null) && (IdClienteNav == null))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: Se debe rellenar uno de los dos parámetros (IdClienteCrm, IdClienteNav)";
            }
            else if (((IdClienteNav != null) && (IdClienteNav.Value > 0)) || ((IdClienteCrm != null) && (IdClienteCrm.Value > 0)))
            {
                objResultado = (ResultadoOperacion<InfoCuenta>) await objServiceCuentasV2.RecuperarInfoCuenta(IdClienteNav.HasValue ? IdClienteNav.Value : -1, IdClienteCrm.HasValue ? IdClienteCrm.Value : -1);
            }
           else
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: Se debe rellenar uno de los dos parámetros (IdClienteCrm, IdClienteNav) y ser mayor que cero.";
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método para comprobar si un dominio es válido en CRM.
        /// </summary>
        /// <param name="strDominio">Literal de dominio</param>
        /// <returns></returns>
        [Route("ComprobarDominioValido/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoBooleano), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> ComprobarDominioValido([FromQuery] string strDominio)
        {

            ResultadoBooleano objResultado = new ResultadoBooleano();

            if (!String.IsNullOrEmpty(strDominio)) 
            {
                objResultado = (ResultadoBooleano)await objServiceCuentasV2.ComprobarDominioValido(strDominio);
            }
            else 
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetro: strDominio debe estar relleno.";
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método para recuperar los clientes CRM utilizando varios filtros
        /// </summary>
        /// <param name="iTipoBusqueda">Tipo de búsqueda (1 : cuenta, 2 : potencial (lead))</param>
        /// <param name="IdClienteCrm">Código cliente Crm</param>
        /// <param name="IdClienteNav">Código cliente Navision</param>
        /// <param name="IdUsuarioCrm">Campo búsqueda por IdUsuarioCRM (char 36)</param>
        /// <param name="strEmail">Campo búsqueda por Email</param>
        /// <param name="strNombreCompleto">Campo búsqueda por Nombre Completo</param>
        /// <param name="strRazonSocial">Campo búsqueda por Razon Social</param>
        /// <param name="strTelefono">Campo búsqueda por Telefono</param>
        /// <param name="strCifNif">Campo búsqueda por Cif-Nif</param>
        /// <param name="strCampoAOrdenar">Campo por el que se ordenará el resultado</param>
        /// <param name="iTipoOrden">Tipo de orden (1 : ascendente, 2 : descendente)</param>
        /// <param name="iPaginaInicial">Número de pagina (1-> Primera página)</param>
        /// <param name="iNumeroFilas">Tamaño página</param>
        /// <returns></returns>
        [Route("RecuperarCuentas")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoLista<ClientesCrm>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarCuentas([FromQuery] int iTipoBusqueda, [FromQuery] int IdClienteCrm, [FromQuery] int IdClienteNav, [FromQuery] string IdUsuarioCrm, [FromQuery] string strNombreCompleto, [FromQuery] string strRazonSocial, [FromQuery] string strTelefono, [FromQuery] string strEmail, [FromQuery] string strCifNif, [FromQuery] string strCampoAOrdenar, [FromQuery] int iTipoOrden, [FromQuery] int iPaginaInicial, [FromQuery] int iNumeroFilas)
        {

            ResultadoLista<ClientesCrm> objResultado = new ResultadoLista<ClientesCrm>();


            if ((iTipoBusqueda != 1) && (iTipoBusqueda != 2))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: iTipoBusqueda debe ser 1 (cuenta) o 2 (potencial).";
            }
            else if ((!String.IsNullOrWhiteSpace(strEmail)) && (!ServicioHelpers.ValidarEmail(strEmail)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: strEmail no tiene formato correcto.";
            }
            else if (String.IsNullOrWhiteSpace(strCampoAOrdenar))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: strCampoAOrdenar debe estar relleno.";
            }
            else if (String.IsNullOrWhiteSpace(strNombreCompleto) && String.IsNullOrWhiteSpace(strRazonSocial) && String.IsNullOrWhiteSpace(strTelefono) &&
                String.IsNullOrWhiteSpace(strCifNif) && String.IsNullOrWhiteSpace(strEmail) && (IdClienteCrm <=0) && (IdClienteNav <=0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: Al menos uno de estos campos debe estar relleno:  (IdClienteCrm, IdClienteNav, NombreCompleto, RazonSocial, Telefono, Cif_Nif, Email)";
            }
            else if (String.IsNullOrWhiteSpace(IdUsuarioCrm))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: IdUsuarioCrm debe estar relleno.";
            }
            else if ((iTipoOrden != 1) && (iTipoOrden != 2))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: iTipoOrden debe ser 1 (ascendente) o 2 (descendente).";
            }
            else if ((iPaginaInicial <= 0) || (iNumeroFilas <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: los parámetros iPaginaInicial y iNumeroFilas debe ser mayor que cero. ";
            }
            else
            {
                objResultado = (ResultadoLista<ClientesCrm>)await objServiceCuentasV2.RecuperarInfoCuentas(iTipoBusqueda, IdClienteCrm, IdClienteNav, IdUsuarioCrm, strNombreCompleto, strRazonSocial, strTelefono, strEmail, strCifNif, strCampoAOrdenar, iTipoOrden, iPaginaInicial - 1, iNumeroFilas);

            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método para recuperar los usuarios visuales de cuenta Crm
        /// </summary>
        /// <param name="iTipoBusqueda">Tipo de búsqueda (1 : cuenta, 2 : potencial)</param>
        /// <param name="IdClienteCrm">Código cliente Crm</param>
        /// <returns></returns>
        [Route("RecuperarComerciales")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoLista<UsuarioVisual>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarComerciales([FromQuery] int iTipoBusqueda, [FromQuery] int IdClienteCrm)
        {
            
            ResultadoLista<UsuarioVisual> objResultado = new ResultadoLista<UsuarioVisual>();


            if ((iTipoBusqueda != 1) && (iTipoBusqueda != 2))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: iTipoBusqueda debe ser 1 (cuenta) o 2 (potencial).";
            }
            else if (IdClienteCrm<=0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: IdClienteCrm debe ser mayor que cero.";
            }
           
            else
            {
                objResultado = (ResultadoLista<UsuarioVisual>)await objServiceCuentasV2.RecuperarComerciales(iTipoBusqueda, IdClienteCrm);

            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }
    }
}