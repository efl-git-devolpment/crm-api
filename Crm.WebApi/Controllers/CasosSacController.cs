﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.CasoSac;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Crm.WebApi.Controllers
{
    [Route("CasosSac")]
    public class CasosSacController : ControllerApiBase
    {
        private readonly ILogger<CasosSacController> _logger;
        private readonly ICasosSacService<ResultadoBase, CasoSac> svc;

        /// <summary>
        /// Constructor al que se inyectan los servicios aplicables
        /// </summary>
        /// <param name="service">servicio para tratar listas</param>
        /// <param name="logger">servicio de log</param>
        /// <param name="configAccessor"></param>
        public CasosSacController(
            ICasosSacService<ResultadoBase, CasoSac> service,
            ILogger<CasosSacController> logger,
            IOptions<ConfigApi> configAccessor
            ) : base(configAccessor)
        {
            svc = service;
            _logger = logger;
        }

        /// <summary>
        /// Crea un caso sac en crm
        /// </summary>
        /// <param name="objPeticion">Datos del caso sac</param>
        /// <returns></returns>
        [Route("CrearCasoSac")]
        [HttpPost]
        [ProducesResponseType(typeof(ResultadoCadena), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult CrearCasoSac(
            [FromBody] CasoSac objPeticion)
        {
            ResultadoCadena objResultado = new ResultadoCadena();
            if (objPeticion == null)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar los datos del caso sac";
            }

            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoCadena)svc.CrearCasoSac(objPeticion);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }


        /// <summary>
        /// Modifica un caso sac
        /// </summary>
        /// <param name="objPeticion"></param>
        /// <returns></returns>
        [Route("ModificarCasoSac")]
        [HttpPost]
        [ProducesResponseType(typeof(ResultadoCadena), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult ModificarCasoSac(
            [FromBody] CasoSac objPeticion)
        {
            ResultadoCadena objResultado = new ResultadoCadena();
            if (objPeticion == null)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar los datos del caso sac";
            }

            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoCadena)svc.ModificarCasoSac(objPeticion);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

        /// <summary>
        /// Recupera los tipos de caso sac necesarios al crear o modificar un caso sac
        /// </summary>
        /// <returns></returns>
        [Route("RecuperarTipoCasoSac")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoLista<TipoCasoSac>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarTipoCasoSac()
        {
            ResultadoLista<TipoCasoSac> objResultado = new ResultadoLista<TipoCasoSac>();


            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoLista<TipoCasoSac>)svc.RecuperarTipoCasoSac();
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }
    }
}
