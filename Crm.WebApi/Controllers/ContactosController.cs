﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Contactos;
using Crm.Vistas.Crm;
using DllApiBase.Servicio;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySqlConnector;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Crm.WebApi.Controllers
{
    [Route("Contactos/")]
    public class ContactosController : ControllerApiBase
    {
        private readonly ILogger<ContactosController> objLogger;
        private readonly IContactosService<ContactoListadoExt, ContactoCreacion, MySqlConnection, ContactoModificacion> objServiceContactos;
        private readonly IContactosV2Service<ResultadoBase> objServiceContactosV2;

        /// <summary>
        /// Constructor al que se inyectan los servicios aplicables
        /// </summary>
        /// <param name="objServiceContactosBase">servicio para gestionar los contactos en CRM</param>
        /// <param name="objServiceContactosV2Base">servicio para gestionar los contactos en CRM refactorizado</param>
        /// <param name="objLoggerBase">servicio de log</param>
        /// <param name="objConfigBase"></param>
        public ContactosController(
            IContactosService<ContactoListadoExt, ContactoCreacion, MySqlConnection, ContactoModificacion> objServiceContactosBase,
            IContactosV2Service<ResultadoBase> objServiceContactosV2Base,
            ILogger<ContactosController> objLoggerBase,
            IOptions<ConfigApi> objConfigBase
            ) : base(objConfigBase)
        {
            objServiceContactos = objServiceContactosBase;
            objServiceContactosV2 = objServiceContactosV2Base;
            objLogger = objLoggerBase;

        }

        ////POSIBLE DESUSO --> 12/09/24 --> No se encuentran logs desde 20/10/23
        ///// <summary>
        ///// Metodo que recupera los contactos de una cuenta filtrando por su codigo navision
        ///// POSIBLE DESUSO
        ///// </summary>
        ///// <param name="IdClienteNav">Codigo navision</param>
        ///// <param name="Search">Campo busqueda por Email</param>
        ///// <param name="PageNum">Numero de pagina. La primera pagina es la 0</param>
        ///// <param name="PageSize">Tamaño pagina</param>
        ///// <returns></returns>
        ////Este metodo es el equivalente a RecuperarCuentaContactosByNavision de wcfsugar
        //[Route("RecuperarContactosPorCodigoNavision/")]
        //[HttpGet]
        //[ProducesResponseType(typeof(ContactoListadoExt), 200)]
        //[ProducesResponseType(typeof(void), 400)]
        //public async Task<IActionResult> RecuperarCuentaContactosByNavisionAsync(
        //    [FromQuery] int IdClienteNav,
        //    [FromQuery] string Search,
        //    [FromQuery] int PageNum,
        //    [FromQuery] int PageSize)
        //{
        //    objLogger.LogDebug("Metodo -->{0} IdClienteNav-->{1}, Search-->{1}, PageNum-->{1}, PageSize-->{1}", nameof(RecuperarCuentaContactosByNavisionAsync), IdClienteNav, Search, PageNum, PageSize);
        //    if (IdClienteNav <= 0)
        //        return BadRequest();

        //    var okOperation = await objServiceContactos.RecuperarCuentaContactosByNavisionAsync(IdClienteNav, Search, PageNum, PageSize);

        //    return new ObjectResult(okOperation);
        //}

        ////POSIBLE DESUSO --> 12/09/24 --> Solo hay una llamada el 30/08/24 que da error. No tiene pinta que se esté usando
        ///// <summary>
        ///// Metodo que recupera los contactos de una cuenta filtrando por su codigo crm
        ///// POSIBLE DESUSO
        ///// </summary>
        ///// <param name="IdClienteCrm">Codigo cliente crm</param>
        ///// <param name="Search">Campo busqueda por Email</param>
        ///// <param name="PageNum">Numero de pagina. La primera pagina es la 0</param>
        ///// <param name="PageSize">Tamaño pagina</param>
        ///// <returns></returns>
        ////Este metodo es el equivalente a RecuperarCuentaContactos de wcfsugar
        //[Route("RecuperarContactos/")]
        //[HttpGet]
        //[ProducesResponseType(typeof(ContactoListadoExt), 200)]
        //[ProducesResponseType(typeof(void), 400)]
        //public async Task<IActionResult> RecuperarCuentaContactosAsync(
        //    [FromQuery] int IdClienteCrm,
        //    [FromQuery] string Search,
        //    [FromQuery] int PageNum,
        //    [FromQuery] int PageSize)
        //{
        //    objLogger.LogDebug("Metodo -->{0} IdClienteNav-->{1}, Search-->{1}, PageNum-->{1}, PageSize-->{1}", nameof(RecuperarCuentaContactosAsync), IdClienteCrm, Search, PageNum, PageSize);
        //    if (IdClienteCrm <= 0)
        //        return BadRequest();

        //    var okOperation = await objServiceContactos.RecuperarCuentaContactosAsync(IdClienteCrm, Search, PageNum, PageSize);

        //    return new ObjectResult(okOperation);
        //}

        ////POSIBLE DESUSO --> 12/09/24 --> Solo hay una llamada el 30/08/24 que da error. No tiene pinta que se esté usando
        ///// <summary>
        /////   Crea un contacto en el Crm
        /////   POSIBLE DESUSO
        ///// </summary>
        ///// <param name="Contacto">Datos del contacto que se quiere crear</param>
        ///// <returns></returns>
        ///// <remarks>Este método corresponde con AddContacto y AddContactoOfThird de wcfsugar</remarks>
        //[Route("CrearContacto/")]
        //[HttpPost]
        //[ProducesResponseType(typeof(int), 200)]
        //[ProducesResponseType(typeof(void), 400)]
        //public async Task<ActionResult> AddAsync([FromBody] ContactoCreacion Contacto)
        //{
        //    try
        //    {

        //        if (Contacto == null)
        //        {
        //            return BadRequest();
        //        }
        //        else
        //        {
        //            if (string.IsNullOrWhiteSpace(Contacto.Email) || Contacto.NumTdcSugar < 0)
        //                return BadRequest();
        //        }

        //        int okOperation = await objServiceContactos.AddAsync(Contacto);

        //        return okOperation >= 0 ? (ActionResult)new ObjectResult(okOperation) : BadRequest();
        //    }
        //    catch (Exception ex)
        //    {
        //        objLogger.LogError($"ERROR --> {nameof(AddAsync)}. Texto --> {ex}");
        //        return BadRequest(ex.Message);
        //    }
        //}

        ////POSIBLE DESUSO --> 12/09/24 --> Solo hay una llamada el 30/08/24 que da error. No tiene pinta que se esté usando
        ///// <summary>
        /////   Crea un contacto en el Crm
        /////   POSIBLE DESUSO
        ///// </summary>
        ///// <param name="NumTdcSugar">Id de la cuenta de kojak</param>
        ///// <param name="OrigenModule">Módulo origen</param>
        ///// <param name="Nombre">Nombre</param>
        ///// <param name="Apellido1">Primer apellido</param>
        ///// <param name="Apellido2">Segundo apellido</param>
        ///// <param name="Telefono">Telefono</param>
        ///// <param name="Email">Emails</param>
        ///// <param name="CifNif">CifNif del contacto</param>
        ///// <returns></returns>
        ///// <remarks>Este metodo corresponde con AddContactoFromClaves de wcfsugar</remarks>
        //[Route("CrearContactoClaves/")]
        //[HttpPost]
        //[ProducesResponseType(typeof(int), 200)]
        //[ProducesResponseType(typeof(void), 400)]
        //public async Task<ActionResult> AddAsync(
        //    [FromQuery] int NumTdcSugar,
        //    [FromQuery] int OrigenModule,
        //    [FromQuery] string Nombre,
        //    [FromQuery] string Apellido1,
        //    [FromQuery] string Apellido2,
        //    [FromQuery] string Telefono,
        //    [FromQuery] string Email,
        //    [FromQuery] string CifNif)
        //{
        //    if (NumTdcSugar <= 0 || string.IsNullOrWhiteSpace(Email))
        //    {
        //        return BadRequest();
        //    }
        //    int okOperation = await objServiceContactos.AddContactoClavesAsync(NumTdcSugar, OrigenModule, Nombre, Apellido1, Apellido2, Telefono, Email, CifNif);

        //    return okOperation >= 0 ? (ActionResult)new ObjectResult(okOperation) : BadRequest();
        //}

        ////POSIBLE DESUSO --> 12/09/24 --> No hay llamadas en el log.
        ///// <summary>
        /////   Modifica un contacto en el Crm
        /////   POSIBLE DESUSO
        ///// </summary>
        ///// <param name="IdContactoCrm">Id contacto crm</param>
        ///// <param name="NumTdcSugar">Id de la cuenta de kojak</param>
        ///// <param name="OrigenModule">Modulo origen</param>
        ///// <param name="Nombre">Nombre</param>
        ///// <param name="Apellido1">Primer apellido</param>
        ///// <param name="Apellido2">Segundo apellido</param>
        ///// <param name="Telefono">Telefono</param>
        ///// <param name="Email">Emails</param>
        ///// <param name="CifNif">CifNif</param>
        ///// <returns></returns>
        ///// <remarks>Este metodo corresponde con UpdateContactoFromClaves de wcfsugar</remarks>
        //[Route("ModificarContactoClaves/")]
        //[HttpPut]
        //[ProducesResponseType(typeof(int), 200)]
        //[ProducesResponseType(typeof(void), 400)]
        //public async Task<ActionResult> AddAsync(
        //    [FromQuery] int IdContactoCrm,
        //    [FromQuery] int NumTdcSugar,
        //    [FromQuery] int OrigenModule,
        //    [FromQuery] string Nombre,
        //    [FromQuery] string Apellido1,
        //    [FromQuery] string Apellido2,
        //    [FromQuery] string Telefono,
        //    [FromQuery] string Email,
        //    [FromQuery] string CifNif)
        //{
        //    if (IdContactoCrm <= 0 || NumTdcSugar <= 0 || string.IsNullOrWhiteSpace(Email))
        //    {
        //        return BadRequest();
        //    }
        //    int okOperation = await objServiceContactos.updateContactoClaves(IdContactoCrm, NumTdcSugar, OrigenModule, Nombre, Apellido1, Apellido2, Telefono, Email, CifNif);

        //    return okOperation >= 0 ? (ActionResult)new ObjectResult(okOperation) : BadRequest();
        //}

        ////POSIBLE DESUSO --> 12/09/24 --> No se han encontrado llamadas en el log
        ///// <summary>
        /////   Modifica un contacto en el Crm
        /////   POSIBLE DESUSO
        ///// </summary>
        ///// <param name="contacto">Datos del contacto</param>
        ///// <returns></returns>
        ///// <remarks>Este metodo corresponde con UpdateContactoFromClaves de wcfsugar</remarks>
        //[Route("ModificarContactoCompleto/")]
        //[HttpPut]
        //[ProducesResponseType(typeof(int), 200)]
        //[ProducesResponseType(typeof(void), 400)]
        //public async Task<ActionResult> AddAsync(
        //    [FromBody] ContactoModificacion contacto)
        //{
        //    if (contacto == null)
        //    {
        //        return BadRequest();
        //    }
        //    int okOperation = await objServiceContactos.updateContactoCompleto(contacto);

        //    return okOperation >= 0 ? (ActionResult)new ObjectResult(okOperation) : BadRequest();
        //}

        /// <summary>
        /// Recupera el contacto administrador de Espacio Cliente.
        /// </summary>
        /// <param name="IdClienteNav">Codigo Cliente Navision</param>
        /// <returns></returns>
        [Route("RecuperarContactoAdministrador/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ContactoAdministrador>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarContactoAdministrador(int IdClienteNav)
        {
            ResultadoOperacion<ContactoAdministrador> objResultado = new ResultadoOperacion<ContactoAdministrador>();

            if (IdClienteNav <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: IdClienteNav debe ser mayor que cero.";
            }
            else
            {
                objResultado = (ResultadoOperacion<ContactoAdministrador>)await objServiceContactosV2.RecuperarContactoAdministrador(IdClienteNav);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

        /// <summary>
        /// Recupera contactos duplicados
        /// </summary> 
        /// <param name="IdClienteCrm">Id Cliente Crm</param>
        /// <param name="iTipoBusqueda">Tipo de búsqueda (1 : account, 2 : lead)</param>
        /// <param name="strNombre">Nombre del contacto</param>
        /// <param name="strApellido1">Primer apellido del contacto</param>
        /// <param name="strApellido2">Segundo apellido del contacto</param>
        /// <param name="strEmail">Email del contacto</param>
        /// <returns></returns>
        [Route("RecuperarContactosDuplicados/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoLista<ContactoDuplicado>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarContactosDuplicados(int IdClienteCrm, int iTipoBusqueda, string strNombre, string strApellido1, string strApellido2, string strEmail)
        {
            ResultadoLista<ContactoDuplicado> objResultado = new ResultadoLista<ContactoDuplicado>();

            if (String.IsNullOrWhiteSpace(strNombre) || String.IsNullOrWhiteSpace(strApellido1))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: strNombre y strApellido1 deben ir rellenos.";
            }
            else if ((iTipoBusqueda != 1) && (iTipoBusqueda != 2))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: iTipoBusqueda debe ser 1 (account) o 2 (lead).";
            }
            else if ((!String.IsNullOrWhiteSpace(strEmail)) && (!ServicioHelpers.ValidarEmail(strEmail)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: strEmail no tiene formato correcto.";
            }

            else
            {
                objResultado = (ResultadoLista<ContactoDuplicado>)await objServiceContactosV2.RecuperarContactosDuplicados(IdClienteCrm, iTipoBusqueda, strNombre, strApellido1, strApellido2, strEmail);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

        /// <summary>
        /// Método para crear un nuevo contacto de CRM
        /// </summary>
        /// <param name="objPeticion">Objeto petición contacto</param>
        /// <returns></returns>
        [Route("InsertarContacto/")]
        [HttpPost]
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<ActionResult> InsertarContacto([FromBody] InsertarContactoPeticion objPeticion)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1}", nameof(InsertarContacto), objPeticion != null ? JsonConvert.SerializeObject(objPeticion) : "sin valor");


            ResultadoEntero objResultado = new ResultadoEntero();

            if (objPeticion == null)
            {
                objResultado.iResultado = -1;
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no valido: El objeto esta vacio.";
            }
            else
            {

                if (objPeticion.IdClienteCrm <= 0)
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.IdClienteCrm debe ser mayor que cero.";
                }
                else if (!ServicioHelpers.ValidarEmail(objPeticion.strEmail)) // debe ser obligatorio, no puede ser vacio.
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strEmail no esta relleno o no tiene formato correcto.";
                }
                else if ((String.IsNullOrWhiteSpace(objPeticion.strNombre)) || (String.IsNullOrWhiteSpace(objPeticion.strApellido1)))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strNombre o objPeticion.strApellido1 no están rellenos";
                }
                else if ((!String.IsNullOrWhiteSpace(objPeticion.strTelefono)) && (!ServicioHelpers.ValidarTelefono(objPeticion.strTelefono, false)))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strTelefono no tiene formato correcto.";
                }
                else if ((!String.IsNullOrWhiteSpace(objPeticion.strCifNif)) && (!ServicioHelpers.ValidarNIF_CIF_NIE(objPeticion.strCifNif)))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strCifNif no tiene formato correcto.";
                }
                else
                {
                    if (!objPeticion.bIndCheckDuplicados)
                    {
                        objResultado = (ResultadoEntero)await objServiceContactosV2.InsertarContacto(objPeticion.IdClienteCrm, objPeticion.intModuloOrigen, objPeticion.strNombre, objPeticion.strApellido1, objPeticion.strApellido2, objPeticion.strTelefono, objPeticion.strEmail, objPeticion.strCifNif, objPeticion.bIndTerceros);
                    }
                    else
                    {

                        var objExisteDuplicados = (ResultadoLista<ContactoDuplicado>)await objServiceContactosV2.RecuperarContactosDuplicados(objPeticion.IdClienteCrm, 1, objPeticion.strNombre, objPeticion.strApellido1, objPeticion.strApellido2, objPeticion.strEmail);

                        if (objExisteDuplicados != null)
                        {
                            if ((objExisteDuplicados.eResultado == enumResultado.Ok_Recuperar) && (objExisteDuplicados.iContador == 0))
                            {
                                objResultado = (ResultadoEntero)await objServiceContactosV2.InsertarContacto(objPeticion.IdClienteCrm, objPeticion.intModuloOrigen, objPeticion.strNombre, objPeticion.strApellido1, objPeticion.strApellido2, objPeticion.strTelefono, objPeticion.strEmail, objPeticion.strCifNif, objPeticion.bIndTerceros);
                            }
                            else
                            {
                                objResultado.eResultado = enumResultado.Error_Validacion;
                                objResultado.sDescripcion = "Error al crear un contacto. Hay " + objExisteDuplicados.iContador.ToString() + " contacto(s).";
                                objResultado.iResultado = -3;
                            }
                        }
                        else
                        {
                            objResultado.sDescripcion = "Error interno en la comprobación de duplicados en CRM";
                            objResultado.iResultado = -2;
                            objResultado.eResultado = enumResultado.Error_Validacion;
                        }
                    }


                }
            }

            objLogger.LogDebug("END -->{0} Parámetros -->{1}", nameof(InsertarContacto), objResultado != null ? objResultado.iResultado.ToString() : "sin valor");

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método para modificar un contacto de CRM
        /// </summary>
        /// <param name="objPeticion">Objeto petición contacto</param>
        /// <returns></returns>
        [Route("ModificarContacto/")]
        [HttpPut]
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<ActionResult> ModificarContacto([FromBody] ModificarContactoPeticion objPeticion)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1}", nameof(ModificarContacto), objPeticion != null ? JsonConvert.SerializeObject(objPeticion) : "sin valor");

            ResultadoEntero objResultado = new ResultadoEntero();

            if (objPeticion == null)
            {
                objResultado.iResultado = -1;
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no valido: El objeto esta vacio.";
            }
            else
            {
                if (objPeticion.IdContactoCrm <= 0)
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.IdContactoCrm debe ser mayor que cero.";
                }
                else if ((String.IsNullOrWhiteSpace(objPeticion.strNombre)) || (String.IsNullOrWhiteSpace(objPeticion.strApellido1)))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strNombre o objPeticion.strApellido1 no están rellenos";
                }
                else if (objPeticion.IdClienteCrm <= 0)
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.IdClienteCrm debe ser mayor que cero.";
                }
                else if ((objPeticion.intTipoEmail < 0) || (objPeticion.intTipoEmail > 2))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.intTipoEmail debe tener los valores 0, 1 o 2.";
                }
                else if (!ServicioHelpers.ValidarEmail(objPeticion.strEmail)) // debe ser obligatorio, no puede ser vacio.
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strEmail no esta relleno o no tiene formato correcto.";
                }
                else if (!ServicioHelpers.ValidarEmail(objPeticion.strEmailAnt) && (!String.IsNullOrEmpty(objPeticion.strEmailAnt)))
                {

                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strEmailAnt no tiene formato correcto.";

                }
                else if ((!String.IsNullOrWhiteSpace(objPeticion.strTelefono)) && (!ServicioHelpers.ValidarTelefono(objPeticion.strTelefono, false)))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strTelefono no tiene formato correcto.";
                }
                else if ((!String.IsNullOrWhiteSpace(objPeticion.strCifNif)) && (!ServicioHelpers.ValidarNIF_CIF_NIE(objPeticion.strCifNif)))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strCifNif no tiene formato correcto.";
                }
                else
                {
                    int iHayDuplicadosCrm = 0;

                    if (!objPeticion.bIndCheckDuplicados)
                    {
                        objResultado = (ResultadoEntero)await objServiceContactosV2.ModificarContacto(objPeticion.IdContactoCrm, objPeticion.IdClienteCrm, objPeticion.intModuloOrigen, objPeticion.strNombre, objPeticion.strApellido1, objPeticion.strApellido2, objPeticion.strTelefono, objPeticion.strEmail, objPeticion.strEmailAnt, objPeticion.strCifNif, objPeticion.intTipoEmail);
                    }
                    else
                    {
                        var objExisteDuplicados = (ResultadoLista<ContactoDuplicado>)await objServiceContactosV2.RecuperarContactosDuplicados(objPeticion.IdClienteCrm, 1, objPeticion.strNombre, objPeticion.strApellido1, objPeticion.strApellido2, objPeticion.strEmail);


                        if ((objExisteDuplicados != null) && (objExisteDuplicados.lstResultado != null))
                            foreach (ContactoDuplicado itemContacto in objExisteDuplicados.lstResultado)
                            {

                                if (itemContacto.NUM_CRM != objPeticion.IdContactoCrm)
                                {
                                    objLogger.LogDebug("Continue -->{0} Hay duplicado con IdContacto -->{1},{2}", nameof(ModificarContacto), itemContacto.NUM_CRM.ToString(), objPeticion.IdContactoCrm.ToString());

                                    iHayDuplicadosCrm++;

                                }

                            }

                        if (iHayDuplicadosCrm == 0)
                        {

                            objResultado = (ResultadoEntero)await objServiceContactosV2.ModificarContacto(objPeticion.IdContactoCrm, objPeticion.IdClienteCrm, objPeticion.intModuloOrigen, objPeticion.strNombre, objPeticion.strApellido1, objPeticion.strApellido2, objPeticion.strTelefono, objPeticion.strEmail, objPeticion.strEmailAnt, objPeticion.strCifNif, objPeticion.intTipoEmail);

                        }
                        else
                        {
                            objResultado.sDescripcion = "Error al modificar un contacto. Hay " + iHayDuplicadosCrm.ToString() + " duplicado(s).";
                            objResultado.iResultado = -3;
                        }
                    }
                }
            }

            objLogger.LogDebug("END -->{0} Parámetros -->{1}", nameof(ModificarContacto), objResultado != null ? objResultado.iResultado.ToString() : "sin valor");


            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método para modificar el email de un contacto de CRM
        /// </summary>
        /// <param name="objPeticion">Objeto petición contacto</param>
        /// <returns></returns>
        [Route("ModificarEmailContacto/")]
        [HttpPut]
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<ActionResult> ModificarEmailContacto([FromBody] ModificarEmailContactoPeticion objPeticion)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1}", nameof(ModificarEmailContacto), objPeticion != null ? JsonConvert.SerializeObject(objPeticion) : "sin valor");

            ResultadoEntero objResultado = new ResultadoEntero();

            if (objPeticion == null)
            {
                objResultado.iResultado = -1;
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no valido: El objeto esta vacio.";
            }
            else
            {
                if (objPeticion.IdContactoCrm <= 0)
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.IdContactoCrm debe ser mayor que cero.";
                }
                else if ((objPeticion.intModuloOrigen <= 0) || (objPeticion.intModuloOrigen > 3))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.intModuloOrigen debe tener los valores  1, 2 o 3.";
                }
              
                else if (!ServicioHelpers.ValidarEmail(objPeticion.strEmailNuevo)) // debe ser obligatorio, no puede ser vacio.
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strEmailNuevo no esta relleno o no tiene formato correcto.";
                }
                else if (!ServicioHelpers.ValidarEmail(objPeticion.strEmailAnterior)) // debe ser obligatorio, no puede ser vacio.
                {

                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strEmailAnterior no tiene formato correcto.";

                }
                else
                { 
                   objResultado = (ResultadoEntero)await objServiceContactosV2.ModificarContactoEntidad(objPeticion.IdContactoCrm, objPeticion.intModuloOrigen,objPeticion.strEmailNuevo, objPeticion.strEmailAnterior, objPeticion.sIdContactoCrm);

                }
            }

            objLogger.LogDebug("END -->{0} Parámetros -->{1}", nameof(ModificarEmailContacto), objResultado != null ? objResultado.iResultado.ToString() : "sin valor");


            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método para recuperar información de un contacto, por código Crm
        /// </summary>
        /// <param name="IdContactoCrm">Código contacto Crm</param>
        /// <returns></returns>
        [Route("RecuperarContacto/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<InfoContacto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> RecuperarContacto([FromQuery] int IdContactoCrm)
        {

            ResultadoOperacion<InfoContacto> objResultado = new ResultadoOperacion<InfoContacto>();

            if (IdContactoCrm <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: IdContactoCrm debe ser mayor que cero.";
            }
            else
            {
                objResultado = (ResultadoOperacion<InfoContacto>)await objServiceContactosV2.RecuperarInfoContacto(IdContactoCrm);
            }


            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método para recuperar información de validez de un contacto, por código Crm
        /// </summary>
        /// <param name="IdContactoCrm">Código contacto Crm</param>
        /// <returns></returns>
        [Route("RecuperarContactoValido/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<InfoValidoContacto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> RecuperarContactoValido([FromQuery] int IdContactoCrm)
        {

            ResultadoOperacion<InfoValidoContacto> objResultado = new ResultadoOperacion<InfoValidoContacto>();

            if (IdContactoCrm <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: IdContactoCrm debe ser mayor que cero.";
            }
            else
            {
                objResultado = (ResultadoOperacion<InfoValidoContacto>)await objServiceContactosV2.RecuperarInfoValidoContacto(IdContactoCrm);
            }


            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }


        //POSIBLE DESUSO --> 12/09/24 No se encuentran logs desde el 22/09/23
        /// <summary>
        /// Método que recupera los contactos de una cuenta filtrando por su código crm
        /// POSIBLE DESUSO
        /// </summary>
        /// <param name="IdClienteCrm">Código cliente crm</param>
        /// <param name="strBusqueda">Campo búsqueda por Email</param>
        /// <param name="iPaginaInicial">Número de pagina (1-> Primera página)</param>
        /// <param name="iNumeroFilas">Tamaño página</param>
        /// <returns></returns>
        [Route("RecuperarContactosByClienteCrm/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoLista<Contacto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarContactosByClienteCrm([FromQuery] int IdClienteCrm, [FromQuery] string strBusqueda, [FromQuery] int iPaginaInicial, [FromQuery] int iNumeroFilas)
        {
            objLogger.LogDebug("Metodo -->{0} -- Parámetros {1},{2},{3},{4}", nameof(RecuperarContactosByClienteCrm), IdClienteCrm, strBusqueda, iPaginaInicial, iNumeroFilas);

            ResultadoLista<Contacto> objResultado = new ResultadoLista<Contacto>();

            if ((IdClienteCrm <= 0) || (iPaginaInicial <= 0) || (iNumeroFilas <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: Todos los campos deben estar rellenados.";
            }
            else
                objResultado = (ResultadoLista<Contacto>)await objServiceContactosV2.RecuperarContactosByClienteCrm(IdClienteCrm, strBusqueda, iPaginaInicial - 1, iNumeroFilas);


            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método que recupera los contactos de una Cuenta
        /// </summary>
        /// <param name="IdClienteCrm">Código cliente CRM</param>
        /// <param name="IdClienteNav">Código cliente Nav/BC</param>
        /// <param name="strNombre">Campo filtrado por nombre</param>
        /// <param name="strApellidos">Campo filtrado por apellidos</param>
        /// <param name="strEmail">Campo filtrado por email</param>
        /// <param name="strCifNif">Campo filtrado por cif/nif</param>
        /// <param name="strTelefono">Campo filtrado por teléfono</param>
        /// <param name="strBusqueda">Campo búsqueda general (nombre, apellidos, email, cif/nif, teléfono)</param>
        /// <param name="iPaginaInicial">Número de pagina (1-> Primera página)</param>
        /// <param name="iNumeroFilas">Tamaño página</param>
        /// <returns>Recupera los contactos por los criterios de búsqueda indicados</returns>
        [Route("RecuperarContactosCuenta/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarContactosCuenta([FromQuery] int IdClienteCrm, [FromQuery] int IdClienteNav, [FromQuery] string strNombre, [FromQuery] string strApellidos, [FromQuery] string strEmail, [FromQuery] string strCifNif, [FromQuery] string strTelefono, [FromQuery] string strBusqueda, [FromQuery] int iPaginaInicial, [FromQuery] int iNumeroFilas)
        {
            objLogger.LogDebug("Metodo -->{0} -- Parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", nameof(RecuperarContactosCuenta), IdClienteCrm, IdClienteNav, strNombre, strApellidos, strEmail, strCifNif, strTelefono, strBusqueda, iPaginaInicial, iNumeroFilas);

            ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm> objResultado = new ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm>();

            if ((IdClienteCrm <= 0) && (IdClienteNav <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: IdClienteCrm y/o IdClienteNav debe estar relleno y mayor que cero.";
            }
            else if ((iPaginaInicial <= 0) || (iNumeroFilas <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: iPaginaInicial y iNumeroFilas deben ser mayor que cero.";
            }
            else
                objResultado = (ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm>)await objServiceContactosV2.RecuperarContactos(1, IdClienteCrm, IdClienteNav, strNombre, strApellidos, strEmail, strCifNif, strTelefono, strBusqueda, iPaginaInicial -1, iNumeroFilas);


            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }


        /// <summary>
        /// Método que recupera los contactos de un Potencial
        /// </summary>
        /// <param name="IdClienteCrm">Código cliente CRM</param>
        /// <param name="strNombre">Campo filtrado por nombre</param>
        /// <param name="strApellidos">Campo filtrado por apellidos</param>
        /// <param name="strEmail">Campo filtrado por email</param>
        /// <param name="strCifNif">Campo filtrado por cif/nif</param>
        /// <param name="strTelefono">Campo filtrado por teléfono</param>
        /// <param name="strBusqueda">Campo búsqueda general (nombre, apellidos, email, cif/nif, teléfono)</param>
        /// <param name="iPaginaInicial">Número de pagina (1-> Primera página)</param>
        /// <param name="iNumeroFilas">Tamaño página</param>
        /// <returns>Recupera los contactos por los criterios de búsqueda indicados</returns>
        [Route("RecuperarContactosPotencial/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarContactosPotencial([FromQuery] int IdClienteCrm, [FromQuery] string strNombre, [FromQuery] string strApellidos, [FromQuery] string strEmail, [FromQuery] string strCifNif, [FromQuery] string strTelefono, [FromQuery] string strBusqueda, [FromQuery] int iPaginaInicial, [FromQuery] int iNumeroFilas)
        {
            objLogger.LogDebug("Metodo -->{0} -- Parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9}", nameof(RecuperarContactosPotencial), IdClienteCrm, strNombre, strApellidos, strEmail, strCifNif, strTelefono, strBusqueda, iPaginaInicial, iNumeroFilas);

            ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm> objResultado = new ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm>();

            if (IdClienteCrm <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: IdClienteCrm debe estar relleno y mayor que cero.";
            }
            else if ((iPaginaInicial <= 0) || (iNumeroFilas <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Error en parámetros: iPaginaInicial y iNumeroFilas deben ser mayor que cero.";
            }
            else
                objResultado = (ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm>)await objServiceContactosV2.RecuperarContactos(2, IdClienteCrm, -1, strNombre, strApellidos, strEmail, strCifNif, strTelefono, strBusqueda, iPaginaInicial - 1, iNumeroFilas);


            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }
    }
}