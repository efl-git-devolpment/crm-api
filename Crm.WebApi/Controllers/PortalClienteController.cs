﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Kojak;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Crm.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("PortalCliente")]
    public class PortalClienteController : ControllerApiBase
    {

        private readonly ILogger<PortalClienteController> objLogger;
        private readonly IPortalClienteService<ResultadoBase> objService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objServiceBase"></param>
        /// <param name="objLoggerBase"></param>
        /// <param name="configAccessor"></param>
        public PortalClienteController(
             IPortalClienteService<ResultadoBase> objServiceBase,
             ILogger<PortalClienteController> objLoggerBase,
             IOptions<ConfigApi> configAccessor
             ) : base(configAccessor)
        {
            objService = objServiceBase;
            objLogger = objLoggerBase;
        }

        //POSIBLE DESUSO --> 12/09/24 No se han encontrado llamadas en el log.
        /// <summary>
        /// Recupera la informacion de kojak del producto lexon de boletines
        /// POSIBLE DESUSO
        /// </summary>
        /// <param name="IdBoletin"></param>
        /// <param name="IdLineaBoletin"></param>
        /// <returns></returns>
        [Route("RecuperarInfoPortalClienteBoletin/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ProductoPortalClienteExt>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarInfoPortalClienteBoletin(
            [FromQuery] int IdBoletin,
            [FromQuery] int IdLineaBoletin)
        {
            ResultadoOperacion<ProductoPortalClienteExt> objResultado = new ResultadoOperacion<ProductoPortalClienteExt>();

            if (IdBoletin <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdBoletin";
            }
            else if (IdLineaBoletin < 0)
            {

                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdLineaBoletin";
            }
            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ProductoPortalClienteExt>)objService.RecuperarInfoPortalClienteBoletin(IdBoletin, IdLineaBoletin);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

        //POSIBLE DESUSO --> 12/09/24 No se han encontrado llamadas en el log.
        /// <summary>
        ///  Recupera la informacion de kojak del producto Portal Cliente de presupuetos
        ///  POSIBLE DESUSO
        /// </summary>
        /// <param name="IdExpediente">Id del Expediente devuelto por Navision</param>
        /// <param name="IdProductoNav">Id del producto Navision</param>
        /// <param name="Variante"></param>
        /// <returns></returns>
        [Route("RecuperarInfoPortalClientePresupuesto/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<ProductoPortalClienteExt>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarInfoPortalClientePresupuesto(
            [FromQuery] int IdExpediente,
            [FromQuery] string IdProductoNav,
            [FromQuery] string Variante)
        {
            ResultadoOperacion<ProductoPortalClienteExt> objResultado = new ResultadoOperacion<ProductoPortalClienteExt>();

            if (IdExpediente <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdExpediente";

            }
            else if (string.IsNullOrWhiteSpace(IdProductoNav))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo IdProductoNav";

            }

            if (string.IsNullOrWhiteSpace(objResultado.sDescripcion))
            {
                objResultado = (ResultadoOperacion<ProductoPortalClienteExt>)objService.RecuperarInfoPortalClientePresupuesto(IdExpediente, IdProductoNav, Variante);
            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }



    }
}
