﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crm.Comun.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Crm.WebApi.Controllers
{
    /// <summary>
    ///   controlador base para crear métodos de uso común como el trabajo con claims, acceso a clases de configuración...etc
    /// </summary>
    public class BaseController : Controller
    {
        internal ConfigApi MyConfig { get; private set; }

        /// <summary>
        ///   Constructor en el que inyectamos la configuración
        /// </summary>
        /// <param name="configAccessor"></param>
        public BaseController(
            IOptions<ConfigApi> configAccessor
            )
        {
            MyConfig = configAccessor.Value;
        }

        /// <summary>
        ///   Obtiene los datos asociados al token de usuario
        /// </summary>
        /// <param name="timeExpiration"></param>
        /// <param name="idUser">        </param>
        [ApiExplorerSettings(IgnoreApi = true)]
        public void ObtenerClaims(out long timeExpiration, out string idUser)
        {
            var current = HttpContext.User;

            timeExpiration = ObtenerTiempoExpiracion();
            idUser = ObtenerIdUsuario();
        }

        /// <summary>
        ///   Obtiene el id de usuario del sistema para su uso a lo largo de la aplicación
        /// </summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        public string ObtenerIdUsuario()
        {
            var current = HttpContext.User;
            var idUser = "EsError";
            if (current.HasClaim(c => c.Type == MyConfig.Jwt.Claims.id))
            {
                idUser = current.Claims.FirstOrDefault(c => c.Type == MyConfig.Jwt.Claims.id)?.Value;
            }

            return idUser;
        }

        /// <summary>
        ///   Obtiene el tiempo de espiración del token
        /// </summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        public long ObtenerTiempoExpiracion()
        {
            var current = HttpContext.User;
            long timeExpiration = 0;
            if (current.HasClaim(c => c.Type == MyConfig.Jwt.Claims.exp))
            {
                timeExpiration = long.Parse(current.Claims.FirstOrDefault(c => c.Type == MyConfig.Jwt.Claims.exp)?.Value);
            }
            return timeExpiration;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        private bool CheckTimeStampValido(long? timestamp)
        {
            if (timestamp == null)
                return false;
            var utc = DateTime.UtcNow.AddSeconds(MyConfig.Jwt.Caducidad);
            long unixTime = ((DateTimeOffset)utc).ToUnixTimeSeconds();
            var timeValid = unixTime - timestamp;
            return (timeValid > 0);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        private static bool CheckIduserValido(string idUser)
        {
            return !string.IsNullOrEmpty(idUser) && idUser.StartsWith("E", System.StringComparison.Ordinal);
        }
    }
}