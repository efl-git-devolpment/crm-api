﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Crm.WebApi.Extensions
{
    /// <summary>
    /// permite gestionar los errores de una manera centralizada, incluyendo los personalizados
    /// * ver: http://www.talkingdotnet.com/global-exception-handling-in-aspnet-core-webapi/
    /// * ver: https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/filters?view=aspnetcore-2.0#exception-filters
    /// * ver: https://weblog.west-wind.com/posts/2016/Oct/16/Error-Handling-and-ExceptionFilter-Dependency-Injection-for-ASPNET-Core-APIs
    /// </summary>
    public class CustomExceptionFilter : IExceptionFilter
    {
        private readonly ILogger<CustomExceptionFilter> _Logger;

        /// <summary>
        /// Constructor para inyectar las dependencias
        /// </summary>
        /// <param name="logger"></param>
        public CustomExceptionFilter(
            ILogger<CustomExceptionFilter> logger
            )
        {
            _Logger = logger;
        }

        /// <summary>
        /// Captura la excepción para analizar el tipo y convertir la respuesta a un JSON
        /// Descomentar y crear tipos específicos de excepción según se necesiten
        /// </summary>
        /// <param name="context"></param>
        public void OnException(ExceptionContext context)
        {
            var status = HttpStatusCode.InternalServerError;
            var message = String.Empty;

            var exceptionType = context.Exception.GetType();
            if (exceptionType == typeof(UnauthorizedAccessException))
            {
                message = "Acceso no autorizado";
                status = HttpStatusCode.Unauthorized;
            }
            //if (exceptionType == typeof(UserAuthException))
            //{
            //    message = "Datos de acceso no validos o usuario no autorizado";
            //    status = HttpStatusCode.Unauthorized;
            //}
            else if (exceptionType == typeof(NotImplementedException))
            {
                message = "Ha ocurrido un error en el servidor";
                status = HttpStatusCode.NotImplemented;
            }
            //else if (exceptionType == typeof(RepeatItemException))
            //{
            //    // message = context.Exception.ToString();
            //    message = "Hay un elemento en la base de datos que impide realizar la operación";
            //    status = HttpStatusCode.InternalServerError;
            //}
            //else if (exceptionType == typeof(InputParametersException))
            //{
            //    message = "Hay un error con los parámetros necesarios para realizar la operación en la base de datos";
            //    status = HttpStatusCode.InternalServerError;
            //}
            //else if (exceptionType == typeof(DatabaseException))
            //{

            //    message = "Error al ejecutar el procedimiento en la base de datos";
            //    status = HttpStatusCode.InternalServerError;
            //}
            else
            {
                message = context.Exception.Message;
                status = HttpStatusCode.NotFound;
            }
            context.ExceptionHandled = true;

            var response = context.HttpContext.Response;
            response.StatusCode = (int)status;
            response.ContentType = "application/json";
            _Logger.LogError(new EventId(0), context.Exception, message);
            var err = message + " -> " + context.Exception.Message;
            response.WriteAsync(err);
        }
    }
}
