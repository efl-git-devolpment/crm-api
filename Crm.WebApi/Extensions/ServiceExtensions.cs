﻿
using Crm.Modelos.Extendidos;
using Crm.Modelos.Repositorios;
using Crm.Modelos.Repositorios.Interfaces;
using Crm.Servicio.Interfaces;
using Crm.Servicio.Services;
using Crm.Vistas.CasoSac;
using Crm.Vistas.Contactos;
using Crm.Vistas.Crm;
using Crm.Vistas.LibreriaVirtual;
using Crm.Vistas.Vistas;
using DllApiBase.Vistas;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using MySqlConnector;
using Serilog;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crm.WebApi.Extensions
{
    public static class ServiceExtensions
    {
        /// <summary>
        ///   Nos permite mantener ConfigureServices más limpio y mantenible https://www.infoworld.com/article/3232636/application-development/how-to-use-dependency-injection-in-aspnet-core.html
        ///   1. SINGLETON: solo se crea una instancia que es compartida por todos los clientes.
        ///   2. SCOPED: Una instancia por cada petición.
        ///   3. TRANSIENT: No será compartidos y se crea cada vez que se requiera.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {
            services.AddScoped<ILeadsService<ResultadoBase, lead>, LeadsService>();

            services.AddScoped<IAsistentesService<ResultadoBase>, AsistentesV2Service>();
            services.AddScoped<IPedidosService<ResultadoBase>, PedidosService>();

            services.AddScoped<IContactosRepository<MySqlConnection, CrContactoListadoExt, CrContactoCreacion, CrContactoModificacion>, ContactosRepository>();
            services.AddScoped<IContactosV2Repository<CrContactoCreacion, CrContactoModificacion, CrContactoListado>, ContactosV2Repository>();

            services.AddScoped<IContactosService<ContactoListadoExt, ContactoCreacion, MySqlConnection, ContactoModificacion>, ContactosService>();
            services.AddScoped<IContactosV2Service<ResultadoBase>, ContactosV2Service>();

            services.AddScoped<ICuentasService, CuentasService>();

            services.AddScoped<ICuentasV2Service<ResultadoBase>, CuentasV2Service>();

            services.AddScoped<ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto>, CrmService>();

            services.AddScoped<ILibreriaVirtualService<UsuariosIndicator>, LibreriaVirtualService>();
            services.AddScoped<IBoletinesV2Service<ResultadoBase>, BoletinesService>();
            services.AddScoped<ICentinelaService<ResultadoBase>, CentinelaService>();
            services.AddScoped<ICasosSacService<ResultadoBase, CasoSac>, CasosSacService>();
            services.AddScoped<IClientLinkService<ResultadoBase>, ClientLinkService>();
            services.AddScoped<ILexonService<ResultadoBase>, LexonService>();
            services.AddScoped<IPortalClienteService<ResultadoBase>, PortalClienteService>();
            services.AddScoped<IGenialService, GenialService>();
            return services;
        }

        /// <summary>
        /// Registramos la autenticación por JWT tokens
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterAuth(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
            })
             .AddJwtBearer(o =>
             {
                 o.IncludeErrorDetails = true;

                 o.RequireHttpsMetadata = false;
                 o.SaveToken = true;
                 o.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateIssuer = false,
                     ValidateAudience = false,
                     IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]))
                 };
                 o.Events = new JwtBearerEvents
                 {
                     OnAuthenticationFailed = context =>
                     {
                         Log.Error("OnAuthenticationFailed ->  {0}", context.Exception.Message);
                         return Task.CompletedTask;
                     },
                     OnTokenValidated = context =>
                     {
                         var accessToken = context.SecurityToken as JwtSecurityToken;
                         Log.Debug("OnTokenValidation -> {0}", accessToken.Claims.ToString());
                         return Task.CompletedTask;
                     },
                     OnMessageReceived = context =>
                     {
                         Log.Debug("OnMessageReceived -> {0}", context.Options.SecurityTokenValidators.FirstOrDefault().ToString());
                         return Task.CompletedTask;
                     },
                     OnChallenge = context =>
                     {
                         var accessToken = context.Options.SecurityTokenValidators.FirstOrDefault();
                         Log.Debug("OnMessageReceived -> {0}", accessToken.ToString());
                         return Task.CompletedTask;
                     }
                 };
             });
            return services;
        }
    }
}
