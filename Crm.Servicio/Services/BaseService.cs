﻿using Crm.Comun.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;

namespace Crm.Servicio.Services
{
    public class BaseService<T>
    {
        internal ConfigApi MyConfig { get; private set; }
        private ILogger<T> _logger;
        public BaseService()
        { }

        public BaseService(
            IOptions<ConfigApi> configAccessor
            )
        {
            MyConfig = configAccessor.Value;
        }

        public class Aes256CbcEncrypter
        {
            private static readonly Encoding encoding = Encoding.UTF8;

            public static string Encrypt(string plainText, string key)
            {
                try
                {
                    RijndaelManaged aes = new RijndaelManaged();
                    aes.KeySize = 256;
                    aes.BlockSize = 128;
                    aes.Padding = PaddingMode.PKCS7;
                    aes.Mode = CipherMode.CBC;

                    aes.Key = encoding.GetBytes(key);
                    aes.GenerateIV();

                    ICryptoTransform AESEncrypt = aes.CreateEncryptor(aes.Key, aes.IV);
                    byte[] buffer = encoding.GetBytes(plainText);
                    byte[] encryptedText = AESEncrypt.TransformFinalBlock(buffer, 0, buffer.Length);


                    List<byte> list = new List<byte>();
                    list.AddRange(aes.IV);
                    list.AddRange(encryptedText);

                    byte[] c = list.ToArray();
                    return Convert.ToBase64String(c);
                }
                catch (Exception e)
                {
                    throw new Exception("Error encrypting: " + e.Message);
                }
            }

        }

        public string Get(string url, string metodo, string route, params string[] lstParams)
        {
            string result = "";
            // Construir la llamada
            var strParams = GetParams(lstParams);
            HttpClient client = new HttpClient();
            string urlCompleta = url + metodo + route + strParams;
            HttpResponseMessage response = client.GetAsync(urlCompleta).Result;
            if (response.IsSuccessStatusCode)
            {

                result = response.Content.ReadAsStringAsync().Result.ToString();
            }

            return result;
        }


        public string Post(string url, string metodo, object objeto, string route, params string[] lstParams)
        {
            string result = "";
            // Construir la llamada
            var strParams = GetParams(lstParams);
            HttpClient client = new HttpClient();
            string inputJson = JsonConvert.SerializeObject(objeto);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(url + metodo + route + strParams, inputContent).Result;
            if (response.IsSuccessStatusCode)
            {

                result = response.Content.ReadAsStringAsync().Result.ToString();
            }

            return result;
        }


        public string Put(string url, string metodo, object objeto, string route, params string[] lstParams)
        {
            string result = "";
            // Construir la llamada
            var strParams = GetParams(lstParams);
            HttpClient client = new HttpClient();
            string inputJson = JsonConvert.SerializeObject(objeto);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PutAsync(url + metodo + route + strParams, inputContent).Result;
            if (response.IsSuccessStatusCode)
            {

                result = response.Content.ReadAsStringAsync().Result.ToString();
            }

            return result;
        }


        public string Delete(string url, string metodo, string route, params string[] lstParams)
        {
            string result = "";
            // Construir la llamada
            var strParams = GetParams(lstParams);
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(url + metodo + route + strParams).Result;
            if (response.IsSuccessStatusCode)
            {

                result = response.Content.ReadAsStringAsync().Result.ToString();
            }

            return result;
        }

        public string GetParams(params string[] lstParams)
        {
            var strParams = "";
            if (lstParams.Length > 0)
            {
                strParams = "?";
                for (int i = 0; i < lstParams.Length; i++)
                    strParams += (lstParams[i] + "&");
                strParams = strParams.Substring(0, strParams.Length - 1);
            }

            return strParams;
        }
    }
}
