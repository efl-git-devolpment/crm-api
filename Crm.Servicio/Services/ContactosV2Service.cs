﻿using AutoMapper;
using Crm.Comun.Configuration;
using Crm.Modelos.Extendidos;
using Crm.Modelos.Repositorios.Interfaces;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Contactos;
using Crm.Vistas.Crm;
using Crm.Vistas.LibreriaVirtual;
using DllApiBase.Servicio;
using DllApiBase.Vistas;
using DllApiBase.Vistas.Permisos;
//using Microsoft.AspNetCore.Mvc.Formatters.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Crm.Servicio.Services
{
    public class ContactosV2Service : ServicioApiBase<ContactosV2Service>, IContactosV2Service<ResultadoBase>
    {
        private readonly ILogger<ContactosV2Service> objLogger;
        private readonly IContactosV2Repository<CrContactoCreacion, CrContactoModificacion, CrContactoListado> objModelContactos;
        private readonly ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas,ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> objServiceCrm;
        private readonly ConfigApi objConfig;
        private readonly ExternalV2Service<ContactosV2Service> objServicioExternal;
        private readonly IMapper objMapper;

        public ContactosV2Service(
            IContactosV2Repository<CrContactoCreacion, CrContactoModificacion, CrContactoListado> objModelContactosBase,
            ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> objServiceCrmBase,
            ILogger<ContactosV2Service> objLoggerBase,
            IOptions<ConfigApi> objConfigBase,
            IMapper objMapperBase
            ) : base(objConfigBase, objLoggerBase)
        {
            objModelContactos = objModelContactosBase;
            objServiceCrm = objServiceCrmBase;
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
            objServicioExternal = new ExternalV2Service<ContactosV2Service>(objConfigBase, objLoggerBase);
            objMapper = objMapperBase;
        }

        /// <summary>
        /// Método para recuperar el Contacto Administrador en CRM
        /// </summary>
        /// <param name="IdClienteNav">Id Cliente Navision</param>
        /// <returns></returns>
        public async Task<ResultadoBase> RecuperarContactoAdministrador(int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarContactoAdministrador), IdClienteNav);

            ResultadoOperacion<ContactoAdministrador> objResultado = new ResultadoOperacion<ContactoAdministrador>();

            try
            {
                // recuperar usuario con el permiso de Administrador

                var objResultadoUsuarioAdministrador = (ResultadoLista<PermisoUsuarios>)this.objServicioExternal.RecuperarUsuariosPermiso(IdClienteNav, objConfig.strPermisoGestor);
                if ((objResultadoUsuarioAdministrador != null) && (objResultadoUsuarioAdministrador.eResultado == enumResultado.Ok_Recuperar))
                {
                    if ((objResultadoUsuarioAdministrador.lstResultado != null) && (objResultadoUsuarioAdministrador.lstResultado.Count > 0))
                    {
                        string strIdUsuarioPro = objResultadoUsuarioAdministrador.lstResultado[0].nnavision;

                        //var objResultadoPrueba = (ResultadoLista<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>)this.objServicioExternal.RecuperarUsuariosProducto(51, 0, 0, string.Empty, 0, 0, 0, string.Empty, string.Empty, 1, 10);
                        var objResultadoUsuario = (ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>)this.objServicioExternal.RecuperarUsuarioProducto(IdClienteNav, strIdUsuarioPro);

                        if ((objResultadoUsuario != null) && (objResultadoUsuario.eResultado == enumResultado.Ok_Recuperar))
                        {
                            objResultado.oResultado = new ContactoAdministrador();
                            objResultado.oResultado.IdClienteNav = IdClienteNav;
                            objResultado.oResultado.IdContactoCrm = objResultadoUsuario.oResultado.idContactoCrm;
                            objResultado.oResultado.IdUsuarioPro = objResultadoUsuario.oResultado.numNavision;
                            objResultado.oResultado.strEmail = objResultadoUsuario.oResultado.email;

                            objResultado.sDescripcion = "Encontrado Contacto administrador " + objResultado.oResultado.IdContactoCrm.ToString();
                            objResultado.eResultado = enumResultado.Ok_Recuperar;

                        }
                        else
                        {
                            objResultado.sDescripcion = "No se encontrado Contacto administrador para ClienteNav " + IdClienteNav.ToString();
                            objResultado.eResultado = enumResultado.Error_Validacion;
                        }
                    }
                    else
                    {
                        objResultado.sDescripcion = "No se encontrado Contacto administrador para ClienteNav " + IdClienteNav.ToString();
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarContactoAdministrador), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarContactoAdministrador), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarContactoAdministrador), objResultado.sDescripcion);
            return objResultado;
        }
        /// <summary>
        /// Recuperar los contactos duplicados desde CRM.
        /// </summary>
        /// <param name="IdClienteCrm">Id Cliente Crm</param>
        /// <param name="iTipoBusqueda">tipo de búsqueda (1 : account, 2 : lead)</param>
        /// <param name="strNombre"></param>
        /// <param name="strApellido1"></param>
        /// <param name="strApellido2"></param>
        /// <param name="strEmail"></param>
        /// <returns></returns>
        public async Task<ResultadoBase> RecuperarContactosDuplicados(int IdClienteCrm, int iTipoBusqueda, string strNombre, string strApellido1, string strApellido2, string strEmail)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6}", nameof(RecuperarContactosDuplicados), IdClienteCrm.ToString(), iTipoBusqueda.ToString(), strNombre, strApellido1, strApellido2, strEmail);

            ResultadoLista<ContactoDuplicado> objResultado = new ResultadoLista<ContactoDuplicado>();

            try
            {
                string strOrigen = "web";
                string strModude = iTipoBusqueda == 1 ? "account" : "lead";

                var objResultadoCRM = (ResultadoDuplicadoContactos)await objServiceCrm.ComprobarDuplicadosContacto(strOrigen, IdClienteCrm, strModude, strNombre, strApellido1, strApellido2, strEmail);

                if ((objResultadoCRM != null))
                {
                    objResultado.sDescripcion = "Respuesta desde CRM : " + objResultadoCRM.mensajeResultado;
                    if (objResultadoCRM.codigoResultado > 0)
                    {
                        objResultado.lstResultado = new List<ContactoDuplicado>();
                        objResultado.lstResultado.AddRange(objResultadoCRM.lstContactos);

                        objResultado.iContador = objResultado.lstResultado.Count;
                        objLogger.LogDebug("CONTINUE --> {0} - Se han recuperado {1} duplicados con mensaje '{2}' ", nameof(RecuperarContactosDuplicados), objResultadoCRM.lstContactos.Count.ToString(), objResultadoCRM.mensajeResultado);
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                    }
                    else
                    {


                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                        objResultado.iContador = 0;
                        objLogger.LogDebug("CONTINUE --> {0} - No se ha recuperado duplicados con mensaje '{1}' ", nameof(RecuperarContactosDuplicados), objResultadoCRM.mensajeResultado);


                    }
                }
                else
                {
                    objResultado.lstResultado = null;
                    objResultado.iContador = 0;
                    objResultado.sDescripcion = "No se ha recuperado información de CRM.";
                    objLogger.LogDebug("CONTINUE --> {0} - No se ha recuperado info de CRM", nameof(RecuperarContactosDuplicados));

                    objResultado.eResultado = enumResultado.Error_Validacion;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarContactosDuplicados), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarContactosDuplicados), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarContactosDuplicados), objResultado.sDescripcion);
            return objResultado;
        }

        /// <summary>
        /// Recuperar los contactos duplicados desde CRM.
        /// </summary>
        /// <param name="IdClienteCrm">Id Cliente Crm</param>
        /// <param name="strBusqueda">cambpo libre de búsqueda</param>
        /// <param name="iPaginaInicial">página de inicio</param>
        /// <param name="iNumeroDeFilas">número de filas</param>
        /// <param name="indEspejo">indicativo de contacto espejo</param>
        /// <returns></returns>
        public async Task<ResultadoBase> RecuperarContactosByClienteCrm(int IdClienteCrm, string strBusqueda, int iPaginaInicial, int iNumeroDeFilas)
        {
            objLogger.LogDebug("START -->{0} Parámetros-->{1},{2},{3},{4}", nameof(RecuperarContactosByClienteCrm), IdClienteCrm.ToString(), strBusqueda, iPaginaInicial.ToString(), iNumeroDeFilas.ToString());


            ResultadoLista<Contacto> objResultado = new ResultadoLista<Contacto>();
            try
            {
                var objResultadoBBDD = await objModelContactos.RecuperarContactosByClienteCrm(IdClienteCrm, strBusqueda, iPaginaInicial, iNumeroDeFilas);

                if (objResultadoBBDD != null)
                {
                    if ((objResultadoBBDD.iContador > 0) && (objResultadoBBDD.lstListado != null))
                    {
                        objResultado.lstResultado = objMapper.Map<List<Contacto>>(objResultadoBBDD.lstListado);
                        objResultado.iContador = objResultadoBBDD.iContador;
                        objResultado.sDescripcion = "Se han recuperado " + objResultadoBBDD.iContador.ToString() + " contacto(s).";
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                    }
                    else
                    {
                        objResultado.sDescripcion = "No se han recuperado ningún contacto.";
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                    }
                }
                else
                {
                    objResultado.sDescripcion = "Error al recuperar clientes de bbdd.";
                    objResultado.eResultado = enumResultado.Error_Validacion;
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarContactosByClienteCrm), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarContactosByClienteCrm), ex);

            }

            objLogger.LogDebug("END -->{0} Parámetros -->{1}", nameof(RecuperarContactosByClienteCrm), objResultado.sDescripcion);
            return objResultado;

        }

        /// <summary>
        /// Método para la creación de un nuevo contacto en CRM.
        /// </summary>
        /// <param name="IdClienteCrm"></param>
        /// <param name="intOrigenModule"></param>
        /// <param name="strNombre"></param>
        /// <param name="strApellido1"></param>
        /// <param name="strApellido2"></param>
        /// <param name="strTelefono"></param>
        /// <param name="strEmail"></param>
        /// <param name="strCifNif"></param>
        /// <param name="bIndTercero"></param>
        /// <returns></returns>
        public async Task<ResultadoBase> InsertarContacto(int IdClienteCrm, int intOrigenModule, string strNombre, string strApellido1, string strApellido2, string strTelefono, string strEmail, string strCifNif, bool bIndTercero)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9}", nameof(InsertarContacto), IdClienteCrm.ToString(), intOrigenModule.ToString(), strNombre, strApellido1, strApellido2, strTelefono, strEmail, strCifNif, bIndTercero.ToString());

            ResultadoEntero objResultado = new ResultadoEntero();

            try
            {
                string strOrigen = "web";
                string strModude = intOrigenModule == 2 ? "lead" : "account";

                ResultadoOperacionContacto objResultadoCrm = await objServiceCrm.InsertarContacto(strOrigen, IdClienteCrm, strModude, strNombre, strApellido1, strApellido2, strEmail, strCifNif, strTelefono, false, bIndTercero);

                if (objResultadoCrm != null)
                {

                    if ((objResultadoCrm.codigoResultado == 0) && (!String.IsNullOrWhiteSpace(objResultadoCrm.idContacto)) && (!String.IsNullOrWhiteSpace(objResultadoCrm.num_crm)))
                    {
                        objResultado.iResultado = Convert.ToInt32(objResultadoCrm.num_crm);
                        objResultado.sDescripcion = "Creado nuevo contacto con Id " + objResultado.iResultado;
                        objResultado.eResultado = enumResultado.Ok_Insertado;
                    }
                    else
                    {
                        objResultado.iResultado = -1;
                        objResultado.sDescripcion = "No se ha podido crear nuevo contacto (" + objResultadoCrm.mensajeResultado + ")";
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(InsertarContacto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(InsertarContacto), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(InsertarContacto), objResultado.sDescripcion);
            return objResultado;
        }

        /// <summary>
        /// Método para la actualización de un contacto existente en CRM.
        /// </summary>
        /// <param name="IdContactoCrm"></param>
        /// <param name="IdClienteCrm"></param>
        /// <param name="intOrigenModule"></param>
        /// <param name="strNombre"></param>
        /// <param name="strApellido1"></param>
        /// <param name="strApellido2"></param>
        /// <param name="strTelefono"></param>
        /// <param name="strEmail"></param>
        /// <param name="strCifNif"></param>
        /// <returns></returns>
        public async Task<ResultadoBase> ModificarContacto(int IdContactoCrm, int IdClienteCrm, int intOrigenModule, string strNombre, string strApellido1, string strApellido2, string strTelefono, string strEmail, string strEmailAnt, string strCifNif, int intTipoEmail)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", nameof(ModificarContacto), IdContactoCrm.ToString(), IdClienteCrm.ToString(), intOrigenModule.ToString(), strNombre, strApellido1, strApellido2, strTelefono, strEmail, strEmailAnt, strCifNif, intTipoEmail.ToString());

            ResultadoEntero objResultado = new ResultadoEntero();

            try
            {
                string strOrigen = "web";
                string strModude = intOrigenModule == 2 ? "lead" : "account";

                string strTipoEmail = String.Empty;

                if (intTipoEmail == 1) strTipoEmail = "E";
                else if (intTipoEmail == 2) strTipoEmail = "T";
                else strTipoEmail = String.Empty;

                ResultadoOperacionContacto objResultadoCrm = await objServiceCrm.ModificarContacto(IdContactoCrm, strOrigen, IdClienteCrm, strModude, strNombre, strApellido1, strApellido2, strEmail, strEmailAnt, strCifNif, strTelefono, strTipoEmail);

                if (objResultadoCrm != null)
                {

                    if ((objResultadoCrm.codigoResultado == 0) && (!String.IsNullOrWhiteSpace(objResultadoCrm.idContacto)) && (!String.IsNullOrWhiteSpace(objResultadoCrm.num_crm)))
                    {
                        objResultado.iResultado = Convert.ToInt32(objResultadoCrm.num_crm);
                        objResultado.sDescripcion = "Modificado contacto con Id " + objResultado.iResultado;
                        objResultado.eResultado = enumResultado.Ok_Modificado;
                    }
                    else
                    {
                        if ((objResultadoCrm.codigoResultado == -3) || ((objResultadoCrm.codigoResultado == -2))) objResultado.iResultado = -5; // errores controlados desde CRM
                        else objResultado.iResultado = -1;
                        objResultado.sDescripcion = "No se ha podido modificar el contacto (" + objResultadoCrm.mensajeResultado + ")";
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ModificarContacto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ModificarContacto), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(ModificarContacto), objResultado.sDescripcion);
            return objResultado;

        }

        /// <summary>
        /// Método para recuperar información de un contacto por su código CRM
        /// </summary>
        /// <param name="IdContactoCrm">Id Contacto Crm</param>
        /// <returns></returns>
        public async Task<ResultadoBase> RecuperarInfoContacto(int IdContactoCrm)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarInfoContacto), IdContactoCrm.ToString());

            ResultadoOperacion<InfoContacto> objResultado = new ResultadoOperacion<InfoContacto>();

            try
            {


                var objResultadoCrm = (ResultadoInfoContacto)await objServiceCrm.RecuperarInfoContacto(IdContactoCrm);
                if (objResultadoCrm != null)
                {
                    if ((objResultadoCrm.codigoResultado == 0) && (objResultadoCrm.contacto != null))
                    {
                        if (String.IsNullOrEmpty(objResultadoCrm.contacto.id_contacto))
                        {
                            objResultado.sDescripcion = "Error recuperado en servicio CRM : " + objResultadoCrm.mensajeResultado;
                            objResultado.eResultado = enumResultado.Error_Validacion;
                        }
                        else
                        {
                            objResultado.oResultado = objMapper.Map<InfoContacto>(objResultadoCrm.contacto);


                            objResultado.sDescripcion = "Encontrado información de contacto para " + IdContactoCrm.ToString();
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                        }
                    }
                    else
                    {
                        objResultado.sDescripcion = "Error recuperado en servicio CRM : " + objResultadoCrm.mensajeResultado;
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }
                else
                {
                    objResultado.sDescripcion = "No se recuera información desde servicio CRM.";
                    objResultado.eResultado = enumResultado.Error_Validacion;
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInfoContacto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInfoContacto), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarInfoContacto), objResultado.sDescripcion);
            return objResultado;
        }

        /// <summary>
        /// Método para recuperar información de validez de un contacto por su código CRM
        /// </summary>
        /// <param name="IdContactoCrm">Id Contacto Crm</param>
        /// <returns></returns>
        public async Task<ResultadoBase> RecuperarInfoValidoContacto(int IdContactoCrm)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarInfoValidoContacto), IdContactoCrm.ToString());

            ResultadoOperacion<InfoValidoContacto> objResultado = new ResultadoOperacion<InfoValidoContacto>();

            try
            {


                var objResultadoCrm = (ResultadoInfoValidoContacto)await objServiceCrm.RecuperarInfoValidoContacto(IdContactoCrm);
                if (objResultadoCrm != null)
                {
                    if ((objResultadoCrm.codigo == 0))
                    {

                        objResultado.oResultado = new InfoValidoContacto();
                        objResultado.oResultado.bloqueado = objResultadoCrm.bloqueado;
                        objResultado.oResultado.cancelado = objResultadoCrm.cancelado;
                        objResultado.oResultado.duplicado_con_id = objResultadoCrm.duplicado_con_id;
                        objResultado.oResultado.duplicado_con_modulo = objResultadoCrm.duplicado_con_modulo;


                        objResultado.sDescripcion = "Encontrado información de contacto para " + IdContactoCrm.ToString();
                        objResultado.eResultado = enumResultado.Ok_Recuperar;

                    }
                    else
                    {
                        objResultado.sDescripcion = "Error recuperado en servicio CRM : " + objResultadoCrm.descripcion;
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }
                else
                {
                    objResultado.sDescripcion = "No se recuera información desde servicio CRM.";
                    objResultado.eResultado = enumResultado.Error_Validacion;
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInfoContacto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInfoContacto), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarInfoContacto), objResultado.sDescripcion);
            return objResultado;
        }

        /// <summary>
        /// Convertir el objeto recuperado de Crm al común de ConctactoCRM
        /// </summary>
        /// <param name="objContacto"></param>
        /// <returns></returns>
        private DllApiBase.Vistas.Contactos.ContactoCrm ConvertirContactoCrm(Crm.Vistas.Crm.ContactoCrm objContacto)
        {
            DllApiBase.Vistas.Contactos.ContactoCrm objNuevo = new DllApiBase.Vistas.Contactos.ContactoCrm();
            objNuevo.iIdContactoCrm = objContacto.num_crm;

            objNuevo.strCifNif = objContacto.nif;
            objNuevo.strNombre = objContacto.nombre;
            objNuevo.strApellido1 = objContacto.apellido_1;
            objNuevo.strApellido2 = objContacto.apellido_2;
 
            objNuevo.sIdContactoCrm = objContacto.id;
            objNuevo.strTelefono = objContacto.telefono;
            objNuevo.indBloqueo = String.IsNullOrEmpty(objContacto.bloqueo) ? Convert.ToInt16(objContacto.bloqueo) : (short)0;
            objNuevo.indEspejo = (short)objContacto.espejo;
            objNuevo.indCancelacion = !String.IsNullOrEmpty(objContacto.cancelacion) ? Convert.ToInt16(objContacto.cancelacion) : (short)0;
            objNuevo.lstEmails = new List<string>();

            // pendiente del cambio de CRM para incluir varios emails;l
            foreach (EmailContacto objItem in objContacto.emails)
            {
                if (objItem.principal == 1)
                    objNuevo.lstEmails.Insert(0,objItem.email);
                else objNuevo.lstEmails.Add(objItem.email);
            }

            return objNuevo;

        }

        /// <summary>
        /// Método para la actualización de un contacto existente en CRM.
        /// </summary>
        /// <param name="iTipoBusqueda">Tipo Cuenta : 1 - Cuenta, 2 - Potencial</param>
        /// <param name="IdClienteCrm">Id del Cliente CRM</param>
        /// <param name="IdClienteNav">Id del Cliente Navision/BC</param>
        /// <param name="strNombre">Campo búsqueda por Nombre</param>
        /// <param name="strApellidos">Campo búsqueda por Apellidos</param>
        /// <param name="strEmail">Campo búsqueda por Email</param>
        /// <param name="strTelefono">Campo búsqueda por Telefono</param>
        /// <param name="strCifNif">Campo búsqueda por Cif-Nif</param>
        /// <param name="strBusqueda">Campo búsqueda genérica por nombre, apellidos, email, nif/cif y teléfono</param>
        /// <param name="iPaginaInicial">Número de pagina (0 -> Primera página)</param>
        /// <param name="iNumeroFilas">Tamaño página</param>
        /// <returns></returns>
        public async Task<ResultadoBase> RecuperarContactos(int iTipoBusqueda, int IdClienteCrm, int IdClienteNav, string strNombre, string strApellidos, string strEmail, string strCifNif, string strTelefono, string strBusqueda, int iPaginaInicial, int iNumeroFilas)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", nameof(RecuperarContactos), iTipoBusqueda.ToString(), IdClienteCrm.ToString(), IdClienteNav.ToString(), strNombre, strApellidos, strEmail, strCifNif, strTelefono, strBusqueda, iPaginaInicial.ToString(), iNumeroFilas.ToString());

            ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm> objResultado = new ResultadoLista<DllApiBase.Vistas.Contactos.ContactoCrm>();

            try
            {
                string strOrigen = "web";

                ResultadoContactosCuenta objResultadoCrm = await objServiceCrm.RecuperarContactos(strOrigen, iTipoBusqueda, IdClienteCrm, IdClienteNav, strNombre, strApellidos, strEmail, strCifNif, strTelefono, strBusqueda, iPaginaInicial, iNumeroFilas);

                if (objResultadoCrm != null)
                {

                    if (objResultadoCrm.codigoResultado == 0)
                    {
                        objResultado.iContador = Convert.ToInt32(objResultadoCrm.numero_total_contactos);
                        objResultado.sDescripcion = "Recuperado " + objResultadoCrm.lstContactos.Count.ToString() + " contacto(s).";
                        objResultado.eResultado = enumResultado.Ok_Recuperar;

                        objResultado.lstResultado = new List<DllApiBase.Vistas.Contactos.ContactoCrm>();
                        foreach(Crm.Vistas.Crm.ContactoCrm objItem in objResultadoCrm.lstContactos)
                        {
                            objResultado.lstResultado.Add(this.ConvertirContactoCrm(objItem));
                        }
                    }
                    else
                    {
                   
                        objResultado.sDescripcion = "No se ha podido recuperar contactos (" + objResultadoCrm.mensajeResultado + ")";
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.iContador = 0;
                        objResultado.lstResultado = null;
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarContactos), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarContactos), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarContactos), objResultado.sDescripcion);
            return objResultado;

        }

        /// <summary>
        /// Método para la actualización de un contacto existente en CRM.
        /// </summary>
        /// <param name="IdContactoCrm"></param>
        /// <param name="IdClienteCrm"></param>
        /// <param name="intOrigenModule"></param>
        /// <param name="strNombre"></param>
        /// <param name="strApellido1"></param>
        /// <param name="strApellido2"></param>
        /// <param name="strTelefono"></param>
        /// <param name="strEmail"></param>
        /// <param name="strCifNif"></param>
        /// <returns></returns>
        public async Task<ResultadoBase> ModificarContactoEntidad(int IdContactoCrm, int intOrigeModule,  string strEmailNuevo, string strEmailAnt, string strIdContacto)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5}", nameof(ModificarContactoEntidad), IdContactoCrm.ToString(), intOrigeModule.ToString(), strEmailNuevo, strEmailAnt, strIdContacto);

            ResultadoEntero objResultado = new ResultadoEntero();

            try
            {
                string strOrigen = "web";
                string strModude = "contact"; // intOrigenModule = 3
                if (intOrigeModule == 1) strModude = "lead";
                else if (intOrigeModule == 2) strModude = "account";

                ResultadoModificarEmailContacto objResultadoCrm = await objServiceCrm.ModificarContactoEntidad(IdContactoCrm, strOrigen, strModude, strEmailNuevo, strEmailAnt, strIdContacto);

                if (objResultadoCrm != null)
                {

                    if (objResultadoCrm.codigoResultado == "0")
                    {
                        objResultado.iResultado = 1;
                        objResultado.sDescripcion = "Modificado email del Contacto de " + strEmailAnt + " a " +strEmailNuevo;
                        objResultado.eResultado = enumResultado.Ok_Modificado;
                    }
                    else
                    {
                        objResultado.iResultado = -1;
                        objResultado.sDescripcion = "No se ha podido modificar el email del Contacto (" + objResultadoCrm.mensajeResultado + ")";
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ModificarContactoEntidad), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ModificarContactoEntidad), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(ModificarContactoEntidad), objResultado.sDescripcion);
            return objResultado;

        }

    }
}
