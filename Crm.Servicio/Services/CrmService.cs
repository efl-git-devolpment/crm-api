﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Crm;
using Crm.Vistas.LibreriaVirtual;
using DllApiBase.Servicio;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Servicio.Services
{
    public class CrmService : ServicioApiBase<CrmService>, ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto>
    {
        private readonly ILogger<CrmService> objLogger;
        private readonly ILibreriaVirtualService<UsuariosIndicator> objServiceLV;
        private readonly ConfigApi objConfig;

        public CrmService(
            ILogger<CrmService> objLoggerBase,
            ILibreriaVirtualService<UsuariosIndicator> objServiceLVBase,
            IOptions<ConfigApi> objConfigBase
            ) : base(objConfigBase, objLoggerBase)
        {
            objServiceLV = objServiceLVBase;
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
        }

        public async Task<int> CargarContactoCRM(int iNoCliente)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(CargarContactoCRM), iNoCliente.ToString());

            int intResultado = -1;
            string strDescripcon = string.Empty;

            try
            {

                HttpClient objClient = new HttpClient();
                string strParams = String.Format("/{0}", iNoCliente);
                string strUrl = String.Format("{0}{1}{2}", $"{objConfig.Urls.Evolution.Domain}", "/cargaExterna/cargaBD/CLI/WEB", strParams);

                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                objLogger.LogDebug("Continue -->{0} Llamada a CRM -->{1}", nameof(CargarContactoCRM), strUrl);

                HttpResponseMessage objResponse = objClient.GetAsync(strUrl).Result;

                if (objResponse.IsSuccessStatusCode)
                {
                    string strResultadoHttp = await objResponse.Content.ReadAsStringAsync();

                    objLogger.LogDebug("Continue -->{0} Respuesta-->{1}", nameof(CargarContactoCRM), strResultadoHttp);


                    ResultadoContacto objResultadoCrm = JsonConvert.DeserializeObject<ResultadoContacto>(strResultadoHttp);

                    if (objResultadoCrm != null)
                    {
                        objLogger.LogDebug("Continue -->{0} CodigoRespuesta -->{1}", nameof(CargarContactoCRM), objResultadoCrm.codigoRespuesta.ToString());

                        if (objResultadoCrm.codigoRespuesta == "0")
                        {
                            intResultado = !String.IsNullOrWhiteSpace(objResultadoCrm.num_sugar) ? Convert.ToInt32(objResultadoCrm.num_sugar) : -1;
                            strDescripcon = "Resultado al llamar a CargaExterna: " + intResultado.ToString();
                        }
                        else
                        {
                            List<ResultadoContacto_MensajeRespuestaKO> listMensaje = JsonConvert.DeserializeObject<List<ResultadoContacto_MensajeRespuestaKO>>(objResultadoCrm.mensajeRespuesta.ToString());

                            if ((listMensaje != null) && (listMensaje.Count > 0))
                            {
                                intResultado = -1;
                                strDescripcon = "Mensaje respuesta";

                                foreach (ResultadoContacto_MensajeRespuestaKO objItem in listMensaje)
                                {
                                    strDescripcon = strDescripcon + objItem.ID_EXTERNO + " - " + objItem.ERROR;
                                }

                            }
                        }
                    }
                    else
                    {
                        strDescripcon = "No hay resultado válido en la llamada a CRM.";
                    }
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(CargarContactoCRM), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(CargarContactoCRM), ex);
                intResultado = -2;
            }
            objLogger.LogDebug("END --> {0} con resultado : {1}", nameof(CargarContactoCRM), strDescripcon);
            return intResultado;
        }

        public async Task<ResultadoCuenta> CargarCuentaIndicatorEnCrm(UsuariosIndicator objCuenta, string strOrigenSolicitud)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(CargarCuentaIndicatorEnCrm), objCuenta != null ? "Objeto objCuenta" : "null", strOrigenSolicitud);

            ResultadoCuenta objResultado = new ResultadoCuenta();
            try
            {
                string strResultadoHttp;


                var formData = new Dictionary<string, string>();
                formData.Add("ORIGEN_SOLICITUD", strOrigenSolicitud);
                formData.Add("ID", objCuenta.IdUsuarioIndicator.ToString());
                formData.Add("ORIGEN", "WBI");
                formData.Add("NOMBRE_EMPRESA", objCuenta.NombreEmpresa);
                formData.Add("NOMBRE", objCuenta.Nombre);
                formData.Add("APELLIDOS", objCuenta.Apellidos);
                formData.Add("CIF_NIF", objCuenta.CifNif);
                formData.Add("TELEFONO", objCuenta.Tlf);
                formData.Add("EMAIL", objCuenta.Email);
                formData.Add("PAIS", objCuenta.DirIdPais);
                formData.Add("POBLACION", objCuenta.DirLocalidad);
                formData.Add("CODIGO_POSTAL", objCuenta.DirCpostal);
                formData.Add("DIRECCION", objCuenta.DirNombre);
                string strUrl = String.Format("{0}{1}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/indicator/gestion/cliente").ToLower();

                objLogger.LogDebug("Continue -->{0} Llamada a CRM -->{1}", nameof(CargarCuentaIndicatorEnCrm), strUrl);

                var content = new FormUrlEncodedContent(formData);

                using (var httpClient = new HttpClient())
                {
                    var httpResponse = await httpClient.PostAsync(strUrl, content);

                    strResultadoHttp = await httpResponse.Content.ReadAsStringAsync();
                    if (!string.IsNullOrWhiteSpace(strResultadoHttp))
                    {
                        objLogger.LogDebug("Continue -->{0} Resultado devuelto Crm-->{1}", nameof(CargarCuentaIndicatorEnCrm), strResultadoHttp);

                        objResultado = JsonConvert.DeserializeObject<ResultadoCuenta>(strResultadoHttp);
                        if (objResultado != null && objResultado.codigoResultado >= 0)
                        {
                            if (objResultado.codigoResultado == 1)
                            {
                                foreach (var item in objResultado.listadoCoincidentes)
                                {
                                    objServiceLV.GuardarClienteCoincidenteIndicator(objCuenta.IdUsuarioIndicator, item.modulo, (int)item.numero, item.nombre, 1);
                                }

                            }

                            //result = resultadocontacto.numeroCuenta;
                        }
                        else
                        {
                            objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(CargarCuentaIndicatorEnCrm), objResultado.mensajeResultado);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(CargarCuentaIndicatorEnCrm), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(CargarCuentaIndicatorEnCrm), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(CargarCuentaIndicatorEnCrm));

            return objResultado;
        }

        public async Task<ResultadoDuplicadoContactos> ComprobarDuplicadosContacto(string strOrigen, int IdClienteCrm, string strModule, string strNombre, string strApellido1, string strApellido2, string strEmail)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7}", nameof(ComprobarDuplicadosContacto), strOrigen, IdClienteCrm.ToString(), strModule, strNombre, strApellido1, strApellido2, strEmail);

            ResultadoDuplicadoContactos objResultado = new ResultadoDuplicadoContactos();

            try
            {


                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/controlDuplicadosContacto");

                Dictionary<string, string> objBody = new Dictionary<string, string>();
                objBody.Add("origen", strOrigen);
                objBody.Add("origen_id", String.Empty);
                objBody.Add("origen_num_crm", IdClienteCrm.ToString());
                objBody.Add("origen_module", strModule);
                objBody.Add("nombre", strNombre);
                objBody.Add("apellido_1", strApellido1);
                objBody.Add("apellido_2", String.IsNullOrEmpty(strApellido2) ? String.Empty : strApellido2);
                objBody.Add("email", String.IsNullOrEmpty(strEmail) ? String.Empty : strEmail);

                string inputJson = objBody != null ? JsonConvert.SerializeObject(objBody) : String.Empty;

                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                objLogger.LogDebug("Continue -->{0} Llamada a CRM -->{1}", nameof(ComprobarDuplicadosContacto), strUrl);

                HttpResponseMessage objResponse = await objClient.PostAsync(strUrl, inputContent);

                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = objResponse.Content.ReadAsStringAsync().Result;

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue --> Resultado recuperado en {0} Resultado devuelto CRM -->{1}", nameof(ComprobarDuplicadosContacto), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoDuplicadoContactos>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> Recuperado info en  {0} con mensaje  -->{1}", nameof(ComprobarDuplicadosContacto), objResultado.mensajeResultado);
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> No se ha recuperado info en {0} a la url {1}", nameof(ComprobarDuplicadosContacto), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> No se ha recibido ninguna respuesta en {0} a la url {1}", nameof(ComprobarDuplicadosContacto), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta a la url {1}", nameof(ComprobarDuplicadosContacto), strUrl);
                }


            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ComprobarDuplicadosContacto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ComprobarDuplicadosContacto), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(ComprobarDuplicadosContacto));

            return objResultado;
        }

        /// <summary>
        /// Recuperar Info de Boletin
        /// </summary>
        /// <param name="iIdBoletin">Id del Boletin</param>
        /// <returns></returns>
        public async Task<ResultadoDameBoletinInfo> RecuperarBoletinInfo(int iIdBoletin)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarBoletinInfo), iIdBoletin.ToString());
            ResultadoDameBoletinInfo objResultado = new ResultadoDameBoletinInfo();

            try
            {
                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}{2}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/dameInfoBoletin/", iIdBoletin.ToString());

                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                HttpResponseMessage objResponse = objClient.GetAsync(strUrl).Result;

                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = await objResponse.Content.ReadAsStringAsync();

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue -->{0} Resultado recuperado en CRM -->{1}", nameof(RecuperarBoletinInfo), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoDameBoletinInfo>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info con mensaje  --> {1},{2}", nameof(RecuperarBoletinInfo), objResultado.codigoResultado, objResultado.mensajeResultado);
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info en la url {1}", nameof(RecuperarBoletinInfo), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(RecuperarBoletinInfo), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta a la url {1}", nameof(RecuperarBoletinInfo), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarBoletinInfo), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarBoletinInfo), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(RecuperarBoletinInfo));
            return objResultado;
        }


        public async Task<ResultadoOperacionContacto> InsertarContacto(string strOrigen, int IdClienteCrm, string strModule, string strNombre, string strApellido1, string strApellido2, string strEmail, string strCifNif, string strTelefono, bool bIndEspejo, bool bIndTercero)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", nameof(InsertarContacto), strOrigen, IdClienteCrm.ToString(), strModule, strNombre, strApellido1, strApellido2, strEmail, strCifNif, strTelefono, bIndEspejo.ToString(), bIndTercero.ToString());

            ResultadoOperacionContacto objResultado = new ResultadoOperacionContacto();

            try
            {


                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/creaContacto");

                Dictionary<string, string> objBody = new Dictionary<string, string>();
                objBody.Add("origen", strOrigen);
                objBody.Add("origen_id_crm", IdClienteCrm.ToString());
                objBody.Add("origen_module", strModule);
                objBody.Add("nombre", strNombre);
                objBody.Add("apellido_1", strApellido1);
                objBody.Add("apellido_2", String.IsNullOrEmpty(strApellido2) ? String.Empty : strApellido2);
                objBody.Add("email", String.IsNullOrEmpty(strEmail) ? String.Empty : strEmail);
                objBody.Add("telefono_ppal", strTelefono);
                objBody.Add("documento", strCifNif);
                objBody.Add("third_p_customer", bIndTercero ? "1" : "0");
                objBody.Add("f_espejo", bIndEspejo ? "1" : "0");
                objBody.Add("fecha_creacion", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));

                string inputJson = objBody != null ? JsonConvert.SerializeObject(objBody) : String.Empty;

                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                objLogger.LogDebug("Continue -->{0} Llamada a CRM -->{1}", nameof(InsertarContacto), strUrl);

                HttpResponseMessage objResponse = await objClient.PostAsync(strUrl, inputContent);


                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = objResponse.Content.ReadAsStringAsync().Result;

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue --> Resultado recuperado en {0} Resultado devuelto CRM -->{1}", nameof(InsertarContacto), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoOperacionContacto>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info en con mensaje  -->{1},{2},{3},{4},{5}", nameof(InsertarContacto), objResultado.codigoResultado, objResultado.mensajeResultado, objResultado.idContacto, objResultado.num_crm, objResultado.info_carga);
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info en  a la url {1}", nameof(InsertarContacto), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(InsertarContacto), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta en la url {1}", nameof(InsertarContacto), strUrl);
                }


            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(InsertarContacto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(InsertarContacto), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(InsertarContacto));

            return objResultado;
        }


        public async Task<ResultadoOperacionContacto> ModificarContacto(int idContactoCrm, string strOrigen, int IdClienteCrm, string strModule, string strNombre, string strApellido1, string strApellido2, string strEmail, string strEmailAnt, string strCifNif, string strTelefono, string strTipoEmail)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", nameof(ModificarContacto), idContactoCrm.ToString(), strOrigen, IdClienteCrm.ToString(), strModule, strNombre, strApellido1, strApellido2, strEmail, strEmailAnt, strCifNif, strTelefono, strTipoEmail);

            ResultadoOperacionContacto objResultado = new ResultadoOperacionContacto();

            try
            {


                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/actualizaContacto");

                Dictionary<string, string> objBody = new Dictionary<string, string>();
                objBody.Add("id_contacto_crm", idContactoCrm.ToString());
                objBody.Add("origen", strOrigen);
                objBody.Add("origen_id_crm", IdClienteCrm.ToString());
                objBody.Add("origen_module", strModule);
                objBody.Add("nombre", strNombre);
                objBody.Add("apellido_1", strApellido1);
                objBody.Add("apellido_2", String.IsNullOrEmpty(strApellido2) ? String.Empty : strApellido2);
                objBody.Add("email", String.IsNullOrEmpty(strEmail) ? String.Empty : strEmail);
                objBody.Add("email_origen_peticion", String.IsNullOrEmpty(strEmailAnt) ? String.Empty : strEmailAnt);
                objBody.Add("email_tipo_actualizacion", String.IsNullOrEmpty(strTipoEmail) ? String.Empty : strTipoEmail);

                objBody.Add("telefono_ppal", strTelefono);
                objBody.Add("documento", strCifNif);
                objBody.Add("fecha_modificacion", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));

                string inputJson = objBody != null ? JsonConvert.SerializeObject(objBody) : String.Empty;

                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                objLogger.LogDebug("Continue -->{0} Llamada a CRM -->{1}", nameof(ModificarContacto), strUrl);

                HttpResponseMessage objResponse = await objClient.PostAsync(strUrl, inputContent);


                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = objResponse.Content.ReadAsStringAsync().Result;

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue --> Resultado recuperado en {0} Resultado devuelto CRM -->{1}", nameof(ModificarContacto), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoOperacionContacto>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info en con mensaje  -->{1},{2},{3},{4},{5}", nameof(ModificarContacto), objResultado.codigoResultado, objResultado.mensajeResultado, objResultado.idContacto, objResultado.num_crm, objResultado.info_carga);
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info en  a la url {1}", nameof(ModificarContacto), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(ModificarContacto), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta en la url {1}", nameof(ModificarContacto), strUrl);
                }


            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ModificarContacto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ModificarContacto), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(ModificarContacto));

            return objResultado;
        }

        /// <summary>
        /// Recuperar Info de la cuenta por código de Navision / Código Crm
        /// </summary>
        /// <param name="IdClienteNav">Id del Cliente Navision</param>
        /// <param name="IdClienteCrm">Id del Cliente CRM</param>
        /// <returns></returns>
        public async Task<ResultadoObtenerInfoCuenta> RecuperarInfoCuenta(int IdClienteNav, int IdClienteCrm)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarInfoCuenta), IdClienteNav.ToString());
            ResultadoObtenerInfoCuenta objResultado = new ResultadoObtenerInfoCuenta();

            try
            {
                string strMetodo = "obtenerInfoCuentaConCodNavision";
                int intCodigoCuenta = IdClienteNav;

                if (IdClienteCrm > 0)
                {
                    strMetodo = "obtenerInfoCuentaConCodKojak";
                    intCodigoCuenta = IdClienteCrm;
                }
                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}{2}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/" + strMetodo + "/", intCodigoCuenta.ToString());

                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                HttpResponseMessage objResponse = objClient.GetAsync(strUrl).Result;

                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = await objResponse.Content.ReadAsStringAsync();

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue -->{0} Resultado recuperado en CRM -->{1}", nameof(RecuperarInfoCuenta), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoObtenerInfoCuenta>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info con mensaje  --> {1},{2}", nameof(RecuperarInfoCuenta), objResultado.codigoResultado, objResultado.mensajeResultado);
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info la url {1}", nameof(RecuperarInfoCuenta), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(RecuperarInfoCuenta), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta a la url {1}", nameof(RecuperarInfoCuenta), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInfoCuenta), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInfoCuenta), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(RecuperarInfoCuenta));
            return objResultado;
        }

        /// <summary>
        /// Comprobar si un dominio es válido en CRM
        /// </summary>
        /// <param name="strDominio">Literal del dominio</param>
        /// <returns></returns>
        public async Task<bool?> ComprobarDominioValido(string strDominio)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(ComprobarDominioValido), strDominio);
            bool? bResultado = false;

            try
            {
                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}{2}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/esDominioExcluir/", strDominio);

                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                HttpResponseMessage objResponse = objClient.GetAsync(strUrl).Result;

                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = await objResponse.Content.ReadAsStringAsync();

                    bResultado = Convert.ToBoolean(strResultado);

                    objLogger.LogDebug("Continue --> {0} Hay respuesta {1} en la url {2}", nameof(ComprobarDominioValido), bResultado.ToString(), strUrl);
                }
                else
                {
                    bResultado = null;
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta a la url {1}", nameof(ComprobarDominioValido), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ComprobarDominioValido), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ComprobarDominioValido), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(ComprobarDominioValido));
            return bResultado;
        }


        /// <summary>
        /// Recuperar Info de un contacto
        /// </summary>
        /// <param name="IdContactoCrm">Id del Contacto CRM</param>
        /// <returns></returns>
        public async Task<ResultadoInfoContacto> RecuperarInfoContacto(int IdContactoCrm)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarInfoContacto), IdContactoCrm.ToString());
            ResultadoInfoContacto objResultado = new ResultadoInfoContacto();

            try
            {

                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}{2}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/obtenerInfoContacto/", IdContactoCrm.ToString());

                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                HttpResponseMessage objResponse = objClient.GetAsync(strUrl).Result;

                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = await objResponse.Content.ReadAsStringAsync();

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue -->{0} Resultado recuperado en CRM -->{1}", nameof(RecuperarInfoContacto), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoInfoContacto>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info con mensaje  --> {1},{2}", nameof(RecuperarInfoContacto), objResultado.codigoResultado, objResultado.mensajeResultado);
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info en la url {1}", nameof(RecuperarInfoContacto), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(RecuperarInfoContacto), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta a la url {1}", nameof(RecuperarInfoContacto), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInfoContacto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInfoContacto), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(RecuperarInfoContacto));
            return objResultado;
        }

        /// <summary>
        /// Recuperar Info de Validez de un contacto
        /// </summary>
        /// <param name="IdContactoCrm">Id del Contacto CRM</param>
        /// <returns></returns>
        public async Task<ResultadoInfoValidoContacto> RecuperarInfoValidoContacto(int IdContactoCrm)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarInfoValidoContacto), IdContactoCrm.ToString());
            ResultadoInfoValidoContacto objResultado = new ResultadoInfoValidoContacto();

            try
            {

                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/cliente_valido/contacto/" + IdContactoCrm.ToString() + "? origen=web");

                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                HttpResponseMessage objResponse = objClient.GetAsync(strUrl).Result;

                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = await objResponse.Content.ReadAsStringAsync();

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue -->{0} Resultado recuperado en CRM -->{1}", nameof(RecuperarInfoValidoContacto), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoInfoValidoContacto>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info con mensaje  --> {1},{2}", nameof(RecuperarInfoValidoContacto), objResultado.codigo.ToString(), objResultado.descripcion);
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info en la url {1}", nameof(RecuperarInfoValidoContacto), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(RecuperarInfoValidoContacto), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta a la url {1}", nameof(RecuperarInfoValidoContacto), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInfoValidoContacto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInfoValidoContacto), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(RecuperarInfoValidoContacto));
            return objResultado;
        }


        /// <summary>
        /// Recuperar Información de CRM de una cuenta
        /// </summary>
        /// <param name="iTipoBusqueda">Tipo Cuenta : 1 - Cuenta, 2 - Potencial</param>
        /// <param name="strOrigen">Origen de los datos</param>
        /// <param name="IdClienteCrm">Id del Cliente CRM</param>
        /// <param name="IdClienteNav">Id del Cliente Navision/BC</param>
        /// <param name="IdUsuarioCrm">Campo búsqueda por IdUsuarioCRM (char 36)</param>
        /// <param name="strEmail">Campo búsqueda por Email</param>
        /// <param name="strNombreCompleto">Campo búsqueda por Nombre Completo</param>
        /// <param name="strRazonSocial">Campo búsqueda por Razon Social</param>
        /// <param name="strTelefono">Campo búsqueda por Telefono</param>
        /// <param name="strCifNif">Campo búsqueda por Cif-Nif</param>
        /// <param name="strCampoAOrdenar">Campo por el que se ordenará el resultado</param>
        /// <param name="strOrden">Tipo de orden</param>
        /// <param name="iPaginaInicial">Número de pagina (1-> Primera página)</param>
        /// <param name="iNumeroFilas">Tamaño página</param>
        /// <returns></returns>
        public async Task<ResultadoRecuperarInfoCuentas> RecuperarInfoCuentas(int iTipoBusqueda, string strOrigen, int IdClienteCrm, int IdClienteNav, string IdUsuarioCrm, string strNombreCompleto, string strRazonSocial, string strTelefono, string strEmail, string strCifNif, string strCampoAOrdenar, string strOrden, int iPaginaInicial, int iNumeroFilas)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14}", nameof(RecuperarInfoCuentas), iTipoBusqueda.ToString(), strOrigen, IdClienteCrm.ToString(), IdClienteNav.ToString(), IdUsuarioCrm, strNombreCompleto, strRazonSocial, strTelefono, strEmail, strCifNif, strCampoAOrdenar, strOrden, iPaginaInicial.ToString(), iNumeroFilas.ToString());

            ResultadoRecuperarInfoCuentas objResultado = new ResultadoRecuperarInfoCuentas();

            try
            {




                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/recuperaInfoClientes");
                //string strUrl = "http://led-pre-crm-temp/web_service/recuperaInfoClientes";

                Dictionary<string, string> objBody = new Dictionary<string, string>();
                objBody.Add("origen", strOrigen);
                objBody.Add("IndTipoBusqueda", iTipoBusqueda.ToString());
                objBody.Add("CodigoClienteNavision", IdClienteNav > 0 ? IdClienteNav.ToString() : String.Empty); ;
                objBody.Add("CodigoClienteCrm", IdClienteCrm > 0 ? IdClienteCrm.ToString() : String.Empty);
                objBody.Add("NombreCompleto", strNombreCompleto);
                objBody.Add("RazonSocial", !String.IsNullOrEmpty(strRazonSocial) ? strRazonSocial : String.Empty);
                objBody.Add("Telefono", !String.IsNullOrEmpty(strTelefono) ? strTelefono : String.Empty);
                objBody.Add("Email", !String.IsNullOrEmpty(strEmail) ? strEmail : String.Empty);
                objBody.Add("Cif_Nif", !String.IsNullOrEmpty(strCifNif) ? strCifNif : String.Empty);
                objBody.Add("IdUsuarioCRM", IdUsuarioCrm);
                objBody.Add("CampoAOrdenar", strCampoAOrdenar);
                objBody.Add("IndOrdenar", strOrden);
                objBody.Add("NumeroFilas", iNumeroFilas.ToString());
                objBody.Add("FilaInicial", iPaginaInicial.ToString());

                string inputJson = objBody != null ? JsonConvert.SerializeObject(objBody) : String.Empty;

                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                objLogger.LogDebug("Continue -->{0} Llamada a CRM -->{1}", nameof(ModificarContacto), strUrl);

                HttpResponseMessage objResponse = await objClient.PostAsync(strUrl, inputContent);


                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = objResponse.Content.ReadAsStringAsync().Result;

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue --> Resultado recuperado en {0} Resultado devuelto CRM -->{1}", nameof(RecuperarInfoCuentas), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoRecuperarInfoCuentas>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info en con mensaje  -->{1},{2},{3}", nameof(RecuperarInfoCuentas), objResultado.codigoResultado, objResultado.mensajeResultado, objResultado.numero_total_clientes.ToString());
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info en  a la url {1}", nameof(RecuperarInfoCuentas), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(RecuperarInfoCuentas), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta en la url {1}", nameof(RecuperarInfoCuentas), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInfoCuentas), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInfoCuentas), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(RecuperarInfoCuentas));

            return objResultado;
        }

        /// <summary>
        /// Recuperar Info de Boletin de Genial
        /// </summary>
        /// <param name="iIdBoletin">Id del Boletín de Genial</param>
        /// <param name="iLineaBoletin">LInea de Boletín</param>
        /// <returns></returns>
        public async Task<ResultadoBoletinGenial> RecuperarBoletinGenial(int iIdBoletin, int iLineaBoletin)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarBoletinGenial), iIdBoletin.ToString(), iLineaBoletin.ToString());
            ResultadoBoletinGenial objResultado = new ResultadoBoletinGenial();

            try
            {
                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}{2}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/infoGenial/BOLETIN/", iIdBoletin.ToString()+"/"+ iLineaBoletin.ToString());

                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                HttpResponseMessage objResponse = objClient.GetAsync(strUrl).Result;

                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = await objResponse.Content.ReadAsStringAsync();

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue -->{0} Resultado recuperado en CRM -->{1}", nameof(RecuperarBoletinGenial), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoBoletinGenial>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info con mensaje  --> {1},{2}", nameof(RecuperarBoletinGenial), objResultado.encontrado, objResultado.productoGenial!=null?" con producto genial ":" sin producto genial");
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info en la url {1}", nameof(RecuperarBoletinGenial), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(RecuperarBoletinGenial), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta a la url {1}", nameof(RecuperarBoletinInfo), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarBoletinGenial), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarBoletinGenial), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(RecuperarBoletinGenial));
            return objResultado;
        }

        /// <summary>
        /// Recuperar Info de Usuarios visuales de Cuenta
        /// </summary>
        /// <param name="iTipoBusqueda">Tipo Cuenta : 1 - Cuenta, 2 - Potencial (lead)</param>
        /// <param name="IdClienteCrm">Id del Cliente CRM</param>
        /// <returns></returns>
        public async Task<ResultadoDameInfoComercial> RecuperarInfoComercial(int iTipoBusqueda, int IdClienteCrm)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarInfoComercial), iTipoBusqueda.ToString(), IdClienteCrm.ToString());
            ResultadoDameInfoComercial objResultado = new ResultadoDameInfoComercial();

            try
            {
                string strModulo = "cuenta";
                if (iTipoBusqueda == 2) strModulo = "potencial";

                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}{2}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/dameInformacionVisualUsuario/" + strModulo + "/", IdClienteCrm.ToString());

                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");


                HttpResponseMessage objResponse = objClient.GetAsync(strUrl).Result;

                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = await objResponse.Content.ReadAsStringAsync();

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue -->{0} Resultado recuperado en CRM -->{1}", nameof(ResultadoDameInfoComercial), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoDameInfoComercial>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info con mensaje  --> {1},{2}", nameof(ResultadoDameInfoComercial), objResultado.codigoResultado.ToString(), objResultado.listadoUsuarios != null ? " con usuarios_comercilaes " : " sin usuarios_comercilaes");
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info en la url {1}", nameof(ResultadoDameInfoComercial), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(ResultadoDameInfoComercial), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta a la url {1}", nameof(ResultadoDameInfoComercial), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarBoletinGenial), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarBoletinGenial), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(RecuperarBoletinGenial));
            return objResultado;
        }

        /// <summary>
        /// Recuperar Contactos desde CRM
        /// </summary>
        /// <param name="strOrigen">Origen de los datos</param>
        /// <param name="iTipoBusqueda">Tipo Cuenta : 1 - Cuenta, 2 - Potencial</param>
        /// <param name="IdClienteCrm">Id del Cliente CRM</param>
        /// <param name="IdClienteNav">Id del Cliente Navision/BC</param>
        /// <param name="strNombre">Campo búsqueda por Nombre</param>
        /// <param name="strApellidos">Campo búsqueda por Apellidos</param>
        /// <param name="strEmail">Campo búsqueda por Email</param>
        /// <param name="strTelefono">Campo búsqueda por Telefono</param>
        /// <param name="strCifNif">Campo búsqueda por Cif-Nif</param>
        /// <param name="strBusqueda">Campo búsqueda genérica por nombre, apellidos, email, nif/cif y teléfono</param>
        /// <param name="iPaginaInicial">Número de pagina (0 -> Primera página)</param>
        /// <param name="iNumeroFilas">Tamaño página</param>
        /// <returns></returns>
        public async Task<ResultadoContactosCuenta> RecuperarContactos(string strOrigen, int iTipoBusqueda, int IdClienteCrm,  int IdClienteNav, string strNombre, string strApellidos, string strEmail, string strCifNif, string strTelefono, string strBusqueda, int iPaginaInicial, int iNumeroFilas)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", nameof(RecuperarContactos), strOrigen, iTipoBusqueda.ToString(), IdClienteCrm.ToString(), IdClienteNav.ToString(), strNombre, strApellidos, strEmail, strCifNif, strTelefono, strBusqueda, iPaginaInicial.ToString(), iNumeroFilas.ToString());

            ResultadoContactosCuenta objResultado = new ResultadoContactosCuenta();

            try
            {

                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/recuperaContactosCuenta");

                if (iTipoBusqueda == 2) strUrl = String.Format("{0}{1}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/recuperaContactosPotencial");

                Dictionary<string, string> objBody = new Dictionary<string, string>();
                objBody.Add("origen", strOrigen);
                objBody.Add("numKojak", IdClienteCrm > 0 ? IdClienteCrm.ToString() : String.Empty);
                if (IdClienteNav > 0) objBody.Add("numBC", IdClienteNav.ToString());
                objBody.Add("numeroResultados", iNumeroFilas.ToString());
                objBody.Add("pagina", iPaginaInicial.ToString());
                objBody.Add("filtroNombre", String.IsNullOrEmpty(strNombre)?String.Empty:strNombre);
                objBody.Add("filtroApellidos", String.IsNullOrEmpty(strApellidos) ? String.Empty : strApellidos); 
                objBody.Add("filtroEmail", String.IsNullOrEmpty(strEmail) ? String.Empty : strEmail); 
                objBody.Add("filtroNif", String.IsNullOrEmpty(strCifNif) ? String.Empty : strCifNif); 
                objBody.Add("filtroTelefono", String.IsNullOrEmpty(strTelefono) ? String.Empty : strTelefono); 
                objBody.Add("filtro", String.IsNullOrEmpty(strBusqueda) ? String.Empty : strBusqueda);

                string inputJson = objBody != null ? JsonConvert.SerializeObject(objBody) : String.Empty;

                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                objLogger.LogDebug("Continue -->{0} Llamada a CRM -->{1}", nameof(ResultadoContactosCuenta), strUrl);

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(strUrl),
                    Content = new StringContent(JsonConvert.SerializeObject(objBody), Encoding.UTF8, "application/json"),
                };

                HttpResponseMessage objResponse = objClient.SendAsync(request).Result;

                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = objResponse.Content.ReadAsStringAsync().Result;

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue --> Resultado recuperado en {0} Resultado devuelto CRM -->{1}", nameof(RecuperarContactos), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoContactosCuenta>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> Recuperado info en  {0} con mensaje  -->{1}", nameof(RecuperarContactos), objResultado.mensajeResultado);
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> No se ha recuperado info en {0} a la url {1}", nameof(RecuperarContactos), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> No se ha recibido ninguna respuesta en {0} a la url {1}", nameof(RecuperarContactos), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta a la url {1}", nameof(RecuperarContactos), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarContactos), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarContactos), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(RecuperarContactos));

            return objResultado;
        }

        /// <summary>
        /// Modifica el email de un Contacto a nivel de Entidad
        /// </summary>
        /// <param name="idContactoCrm">Identificador entero del Contacto</param>
        /// <param name="strOrigen">Origen de los datos</param>
        /// <param name="strModule">módulo de la entidad que contiene el email a modificar. Posibles valores (lead, account, contact)</param>
        /// <param name="strEmailNuevo">Nuevo email que se añadirá a la entidad</param>
        /// <param name="strEmailAnt">Email existente en la entidad</param>
        /// <param name="strIdContacto"></param>
        /// <returns></returns>
        public async Task<ResultadoModificarEmailContacto> ModificarContactoEntidad(int idContactoCrm, string strOrigen, string strModule, string strEmailNuevo, string strEmailAnt,  string strIdContacto)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6}", nameof(ModificarContactoEntidad), idContactoCrm.ToString(), strOrigen, strModule, strEmailNuevo, strEmailAnt, strIdContacto);

            ResultadoModificarEmailContacto objResultado = new ResultadoModificarEmailContacto();

            try
            {


                HttpClient objClient = new HttpClient();
                string strUrl = String.Format("{0}{1}", $"{objConfig.Urls.Evolution.Domain}", "/web_service/actualizaEmailEntidad");

                Dictionary<string, string> objBody = new Dictionary<string, string>();
                objBody.Add("origen_num_crm", idContactoCrm.ToString());
                objBody.Add("origen", strOrigen);
                objBody.Add("origen_id", strIdContacto);
                objBody.Add("origen_module", strModule);
                objBody.Add("email_existente", String.IsNullOrEmpty(strEmailAnt) ? String.Empty : strEmailAnt);
                objBody.Add("email_nuevo", String.IsNullOrEmpty(strEmailNuevo) ? String.Empty : strEmailNuevo);
       
                string inputJson = objBody != null ? JsonConvert.SerializeObject(objBody) : String.Empty;

                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                objClient.DefaultRequestHeaders.Accept.Clear();
                objClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                objClient.DefaultRequestHeaders.Add("Accept-Language", "es-ES");

                objLogger.LogDebug("Continue -->{0} Llamada a CRM -->{1}", nameof(ModificarContactoEntidad), strUrl);

                HttpResponseMessage objResponse = await objClient.PostAsync(strUrl, inputContent);


                if (objResponse.StatusCode == HttpStatusCode.OK)
                {
                    string strResultado = objResponse.Content.ReadAsStringAsync().Result;

                    if (!string.IsNullOrWhiteSpace(strResultado))
                    {
                        objLogger.LogDebug("Continue --> Resultado recuperado en {0} Resultado devuelto CRM -->{1}", nameof(ModificarContactoEntidad), strResultado);

                        objResultado = JsonConvert.DeserializeObject<ResultadoModificarEmailContacto>(strResultado);
                        if (objResultado != null)
                        {
                            objLogger.LogDebug("Continue --> {0} Recuperado info en con mensaje  -->{1},{2}", nameof(ModificarContactoEntidad), objResultado.codigoResultado, objResultado.mensajeResultado);
                        }
                        else
                        {
                            objLogger.LogDebug("Continue --> {0} No se ha recuperado info en  a la url {1}", nameof(ModificarContactoEntidad), strUrl);
                        }
                    }
                    else
                    {
                        objLogger.LogDebug("Continue --> {0} Hay respuesta Ok pero sin contenido en la url {1}", nameof(ModificarContactoEntidad), strUrl);
                    }
                }
                else
                {
                    objLogger.LogDebug("Continue --> {0} No se ha recibido ninguna respuesta en la url {1}", nameof(ModificarContactoEntidad), strUrl);
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ModificarContactoEntidad), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ModificarContactoEntidad), ex);
            }
            objLogger.LogDebug("END --> {0}", nameof(ModificarContactoEntidad));

            return objResultado;
        }

    }
}
