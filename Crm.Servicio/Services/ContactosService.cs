﻿using AutoMapper;
using Crm.Comun.Configuration;
using Crm.Modelos.Entidades.Crm;
using Crm.Modelos.Entidades.CrmReplica;
using Crm.Modelos.Extendidos;
using Crm.Modelos.Repositorios.Interfaces;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Contactos;
using Crm.Vistas.Crm;
using Crm.Vistas.LibreriaVirtual;
using DllApiBase.Servicio;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Crm.Servicio.Services
{
    public class ContactosService : ServicioApiBase<ContactosService>, IContactosService<ContactoListadoExt, ContactoCreacion, MySqlConnection, ContactoModificacion>
    {
        /****************************PARA RECUPERAR CONTACTOS HAY QUE LLAMAR A REPLICA Y PARA CREAR  CONTACTO O MODIFICAR HAY QUE LLAMAR AL NORMAL*******/
        private readonly CrmReplicaMySqlDbContext _contextReplica;
        private readonly CrmMySqlDbContext _context;
        private readonly ILogger<ContactosService> objLogger;
        private readonly IContactosRepository<MySqlConnection, CrContactoListadoExt, CrContactoCreacion, CrContactoModificacion> _repo;
        private readonly ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> objServiceCrm;
        private readonly ConfigApi objConfig;
        private readonly ExternalV2Service<ContactosService> objServicioExternal;
        private readonly IMapper objMapper;

        public ContactosService(
            CrmReplicaMySqlDbContext contextReplica,
            CrmMySqlDbContext context,
            IContactosRepository<MySqlConnection, CrContactoListadoExt, CrContactoCreacion, CrContactoModificacion> repo,
            ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> objServiceCrmBase,
            ILogger<ContactosService> objLoggerBase,
            IOptions<ConfigApi> objConfigBase,
            IMapper objMapperBase
            ) : base(objConfigBase, objLoggerBase)
        {
            _contextReplica = contextReplica;
            _context = context;
            _repo = repo;
            objServiceCrm = objServiceCrmBase;
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
            objServicioExternal = new ExternalV2Service<ContactosService>(objConfigBase, objLoggerBase);
            objMapper = objMapperBase;
        }

        public async Task<ContactoListadoExt> RecuperarCuentaContactosByNavisionAsync(
                int IdClienteNav,
                string Search,
                int PageNum,
                int PageSize)
        {
            CrContactoListadoExt listaExt = new CrContactoListadoExt();
            listaExt.Listado = new List<CrContacto>();
            MySqlConnection conn = new MySqlConnection();
            conn.ConnectionString = _contextReplica.Database.GetDbConnection().ConnectionString;
            conn.Open();
            try
            {

                listaExt = await _repo.RecuperarCuentaContactosByNavisionAsync(
                IdClienteNav: IdClienteNav,
                Search: Search,
                PageNum: PageNum,
                PageSize: PageSize,
                indEspejo: 1,
                conn: conn);
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarCuentaContactosByNavisionAsync), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarCuentaContactosByNavisionAsync), ex);
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            var listaMapeada = objMapper.Map<ContactoListadoExt>(listaExt);
            listaMapeada.Listado = objMapper.Map<IEnumerable<Contacto>>(listaExt.Listado);
            return listaMapeada;

        }



        public async Task<ContactoListadoExt> RecuperarCuentaContactosAsync(
            int IdClienteCrm,
            string Search,
            int PageNum,
            int PageSize)
        {
            CrContactoListadoExt listaExt = new CrContactoListadoExt();
            listaExt.Listado = new List<CrContacto>();
            MySqlConnection conn = new MySqlConnection();
            conn.ConnectionString = _contextReplica.Database.GetDbConnection().ConnectionString;
            conn.Open();
            try
            {

                listaExt = await _repo.RecuperarCuentaContactosAsync(
                IdClienteCrm: IdClienteCrm,
                Search: Search,
                PageNum: PageNum,
                PageSize: PageSize,
                indEspejo: 1,
                conn: conn);
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarCuentaContactosAsync), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarCuentaContactosAsync), ex);
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            var listaMapeada = objMapper.Map<ContactoListadoExt>(listaExt);
            listaMapeada.Listado = objMapper.Map<IEnumerable<Contacto>>(listaExt.Listado);
            return listaMapeada;

        }

        public async Task<int> AddAsync(ContactoCreacion entity)
        {
            return await Task.Run(async () =>
            {
                int result = -1;
                MySqlConnection conn = new MySqlConnection();
                conn.ConnectionString = _context.Database.GetDbConnection().ConnectionString;
                conn.Open();
                try
                {
                    if (entity.ThirdPCustomer <= 0)
                    {
                        entity.ThirdPCustomer = 1;
                    }
                    result = await AddContactoAsync(entity, conn);
                }
                catch (Exception ex)
                {
                    objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(AddAsync), ex.Message);
                    objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(AddAsync), ex);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return result;
            });
        }

        public async Task<int> AddContactoAsync(ContactoCreacion entity, MySqlConnection conn)
        {
            int result = -1;

            int NoClient = -1;
            var itemAdd = objMapper.Map<ContactoCreacion, CrContactoCreacion>(entity);
            NoClient = await _repo.AddAsync(itemAdd, conn);
            if (NoClient > 0)
            {
                int resultUrl = await objServiceCrm.CargarContactoCRM(NoClient);
                if (resultUrl > 0)
                {
                    result = resultUrl;
                }
                else
                {
                    objLogger.LogError("ContactosService.AddContactoAsync  Error al llamar a la URL");
                }
            }

            return result;

        }

        public async Task<int> AddContactoClavesAsync(
            int NumTdcSugar,
            int OrigenModule,
            string Nombre,
            string Apellido1,
            string Apellido2,
            string Telefono,
            string Email,
            string CifNif)
        {
            return await Task.Run(async () =>
            {
                int result = -1;
                MySqlConnection conn = new MySqlConnection();
                conn.ConnectionString = _context.Database.GetDbConnection().ConnectionString;
                conn.Open();
                try
                {
                    ContactoCreacion contacto = new ContactoCreacion();
                    contacto.NumTdcSugar = NumTdcSugar;
                    contacto.OrigenModule = OrigenModule;
                    contacto.Nombre = Nombre;
                    contacto.Apellido1 = Apellido1;
                    contacto.Apellido2 = Apellido2;
                    contacto.Telefono = Telefono;
                    contacto.Email = Email;
                    contacto.CifNif = CifNif;
                    contacto.ThirdPCustomer = 1;
                    result = await AddContactoAsync(contacto, conn);
                }
                catch (Exception ex)
                {
                    objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(AddAsync), ex.Message);
                    objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(AddAsync), ex);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return result;
            });
        }

        public async Task<int> updateContactoClaves(
            int IdContactoCrm,
            int NumTdcSugar,
            int OrigenModule,
            string Nombre,
            string Apellido1,
            string Apellido2,
            string Telefono,
            string Email,
            string CifNif)
        {
            return await Task.Run(async () =>
            {
                int result = -1;
                MySqlConnection conn = new MySqlConnection();
                conn.ConnectionString = _context.Database.GetDbConnection().ConnectionString;
                conn.Open();
                try
                {
                    ContactoModificacion contacto = new ContactoModificacion();
                    contacto.IdContactoCrm = IdContactoCrm;
                    contacto.NumTdcSugar = NumTdcSugar;
                    contacto.OrigenModule = OrigenModule;
                    contacto.Nombre = Nombre;
                    contacto.Apellido1 = Apellido1;
                    contacto.Apellido2 = Apellido2;
                    contacto.Telefono = Telefono;
                    contacto.Email = Email;
                    contacto.CifNif = CifNif;
                    contacto.IndLv = 0;
                    result = await UpdateContactoAsync(contacto, conn);
                }
                catch (Exception ex)
                {
                    objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(updateContactoClaves), ex.Message);
                    objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(updateContactoClaves), ex);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return result;
            });
        }

        public async Task<int> updateContactoCompleto(
            ContactoModificacion contacto)
        {
            return await Task.Run(async () =>
            {
                int result = -1;
                MySqlConnection conn = new MySqlConnection();
                conn.ConnectionString = _context.Database.GetDbConnection().ConnectionString;
                conn.Open();
                try
                {

                    result = await UpdateContactoAsync(contacto, conn);
                }
                catch (Exception ex)
                {
                    objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(updateContactoCompleto), ex.Message);
                    objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(updateContactoCompleto), ex);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return result;
            });
        }

        public async Task<int> UpdateContactoAsync(ContactoModificacion entity, MySqlConnection conn)
        {
            int result = -1;

            int NoClient = -1;
            var itemModify = objMapper.Map<ContactoModificacion, CrContactoModificacion>(entity);
            NoClient = await _repo.UpdateAsync(itemModify, conn);
            if (NoClient > 0)
            {
                int resultUrl = await objServiceCrm.CargarContactoCRM(NoClient);
                if (resultUrl > 0)
                {
                    result = resultUrl;
                }
                else
                {
                    objLogger.LogError("ContactosService.AddContactoAsync  Error al llamar a la URL");
                }
            }

            return result;

        }


    }
}
