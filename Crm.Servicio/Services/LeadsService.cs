﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.IO;
using DllApiBase.Servicio;
using DllApiBase.Vistas;
using DllApiBase.Comun.Configuracion;
using DllApiBase.Servicio.PeticionesHttp;

namespace Crm.Servicio.Services
{
    public class LeadsService : ServicioApiBase<LeadsService>, ILeadsService<ResultadoBase, lead>
    {
        private readonly ILogger<LeadsService> objLogger;
        public static ConfigApiBase objConfig { get; private set; }
        public static ConfigApi objConfigLocal { get; private set; }
        private PeticionesHttpApi<LeadsService> objPeticionesHttpApi;
        public LeadsService(
            ILogger<LeadsService> objLoggerBase,
            IOptions<ConfigApiBase> objConfigBase,
            IOptions<ConfigApi> configAccessor) : base(objConfigBase, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
            objConfigLocal = configAccessor.Value;
            objPeticionesHttpApi = new PeticionesHttpApi<LeadsService>(objLogger);
        }


        public async Task<ResultadoBase> GuardarLeadKojak(lead objLead, int iIdLead)
        {
          
            objLogger.LogDebug("START --> {0} con parámetros objLead : {1} , idLead : {2}", nameof(GuardarLeadKojak), JsonConvert.SerializeObject(objLead), iIdLead.ToString());
            //metodo para guardar en kojak los datos.
            return await Task.Run(() =>
            {
                ResultadoOperacion<ResultadoKojak> objResultado = new ResultadoOperacion<ResultadoKojak>();
                string url = $"{objConfigLocal.Urls.Evolution.Domain}" + $"{objConfigLocal.Urls.Evolution.CargaLeads}" + "/" + iIdLead;

                try
                {
                    using (HttpClient client = new HttpClient())
                    {
                        client.Timeout = TimeSpan.FromMinutes(3);
                        //string result = "";
                        client.BaseAddress = new Uri(url);
                        string value = "LeadsWeb-LibreriaVirtual";
                        string key = "aZ4DQEVxGadr9lgxvfW95HhTDvnItb0l";
                        objLead.origen_solicitud = ServicioHelpers.Aes256CbcEncrypter.Encrypt(value, key);
                        string inputJson = JsonConvert.SerializeObject(objLead);
                        //objLogger.LogDebug("LeadService.GuardarLeadKojak idlead: {0} con json {1}", idLead.ToString(), inputJson);
                        HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(url, inputContent).Result;
                        string contenido = response.Content.ReadAsStringAsync().Result;
                        //objLogger.LogDebug("Obtenemos resultados --> {0}", resultado);
                        if (response.IsSuccessStatusCode)
                        {
                            objResultado.oResultado = JsonConvert.DeserializeObject<ResultadoKojak>(contenido);
                        }
                    }
                    //    objResultado.oResultado = new ResultadoKojak();
                    //objResultado.oResultado = objPeticionesHttpApi.PostApi<ResultadoKojak>($"{objConfigLocal.Urls.Evolution.Domain}", $"{objConfigLocal.Urls.Evolution.CargaLeads}" + "/" + iIdLead.ToString(), "", objLead);

                    if (objResultado.oResultado != null)
                    {
                        if (objResultado.oResultado.codigoResultado >= 0)
                        {
                            objResultado.eResultado = enumResultado.Ok_Insertado;
                            objResultado.sDescripcion = "Se ha insertado el lead en crm correctamente.";
                        }
                        
                        else 
                        {
                            objResultado.eResultado = enumResultado.Error_BBDD;
                            objResultado.sDescripcion = "No ha insertado el lead en crm: " + objResultado.oResultado.mensajeResultado;
                        }
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Excepcion;
                        objResultado.sDescripcion = "Error interno al llamar a Kojak.";
                    }

             

                }
                catch (Exception ex)
                {
                    objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(GuardarLeadKojak), ex.Message);
                    objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(GuardarLeadKojak), ex);
                    objResultado.eResultado = enumResultado.Error_Excepcion;
                    objResultado.sDescripcion = ex.Message;
                }
                objLogger.LogDebug("END --> {0} con resultado {1}", nameof(GuardarLeadKojak), objResultado.sDescripcion);
                return objResultado;
            });

           
        }



        public async Task<ResultadoBase> RecuperarResultadoLeadsKojak(int iIdLead)
        {
            objLogger.LogDebug("START --> {0} con parámetros idLead : {1}", nameof(RecuperarResultadoLeadsKojak), iIdLead.ToString());

            return await Task.Run(() =>
            {
                ResultadoOperacion<ResultadoKojak> objResultado = new ResultadoOperacion<ResultadoKojak>();
                string url = $"{objConfigLocal.Urls.Evolution.Domain}/" + iIdLead;

                try
                {
                    objResultado.oResultado = new ResultadoKojak();
                    objResultado.oResultado = objPeticionesHttpApi.GetApi<ResultadoKojak>($"{objConfigLocal.Urls.Evolution.Domain}", $"{objConfigLocal.Urls.Evolution.ResultadoLeadsSinCuenta}" + "/" + iIdLead, "");

                    if (objResultado.oResultado != null)
                    {
                        if (objResultado.oResultado.codigoResultado >= 0)
                        {
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                            objResultado.sDescripcion = "Se ha recuperado los datos del lead de crm.";
                        }

                        else
                        {
                            objResultado.eResultado = enumResultado.Error_BBDD;
                            objResultado.sDescripcion = "No ha recuperado el lead.";
                        }
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Excepcion;
                        objResultado.sDescripcion = "Error interno al llamar a Kojak.";
                    }

                    //using (HttpClient client = new HttpClient())
                    //{
                    //    client.Timeout = TimeSpan.FromMinutes(3);
                    //    //string result = "";
                    //    client.BaseAddress = new Uri(url);
                    //    string value = "LeadsWeb-LibreriaVirtual";
                    //    string key = "aZ4DQEVxGadr9lgxvfW95HhTDvnItb0l";
                    //    objLead.origen_solicitud = Aes256CbcEncrypter.Encrypt(value, key);
                    //    string inputJson = JsonConvert.SerializeObject(objLead);
                    //    //objLogger.LogDebug("LeadService.GuardarLeadKojak idlead: {0} con json {1}", idLead.ToString(), inputJson);
                    //    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    //    HttpResponseMessage response = client.PostAsync(url, inputContent).Result;
                    //    string contenido = response.Content.ReadAsStringAsync().Result;
                    //    //objLogger.LogDebug("Obtenemos resultados --> {0}", resultado);
                    //    if (response.IsSuccessStatusCode)
                    //    {
                    //        objResultado.oResultado = JsonConvert.DeserializeObject<ResultadoKojak>(contenido);
                    //    }
                    //}

                }
                catch (Exception ex)
                {
                    objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarResultadoLeadsKojak), ex.Message);
                    objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarResultadoLeadsKojak), ex);
                    objResultado.eResultado = enumResultado.Error_Excepcion;
                    objResultado.sDescripcion = ex.Message;
                }
                objLogger.LogDebug("END --> {0} con resultado {1}", nameof(RecuperarResultadoLeadsKojak), objResultado.sDescripcion);
                return objResultado;
            });

        }

        }
    }
