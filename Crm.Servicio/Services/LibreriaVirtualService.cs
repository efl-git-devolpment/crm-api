﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.LibreriaVirtual;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Services
{
    public class LibreriaVirtualService : BaseService<LibreriaVirtualService>, ILibreriaVirtualService<UsuariosIndicator>
    {
        private readonly ILogger<LibreriaVirtualService> _logger;

        public LibreriaVirtualService(
            ILogger<LibreriaVirtualService> logger,
            IOptions<ConfigApi> configAccessor
            ) : base(configAccessor)
        {
            _logger = logger;
        }

        public List<UsuariosIndicator> RecuperarCuentasIndicatorPedidos()
        {
            List<UsuariosIndicator> resultado = new List<UsuariosIndicator>();

            try
            {
                string resultadoLlamada;

                resultadoLlamada = this.Get($"{MyConfig.Urls.LibreriaVirtual.Domain}", "Indicator/RecuperarCuentasIndicatorPedidos", "");
                if (!string.IsNullOrWhiteSpace(resultadoLlamada))
                {

                    resultado = JsonConvert.DeserializeObject<List<UsuariosIndicator>>(resultadoLlamada);

                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarCuentasIndicatorPedidos), ex);
            }

            return resultado;
        }



        public int GuardarCuentaIndicator(int idUsuarioIndicator, int IdClienteCrm, int? IdContactoCrm, int idUic)
        {
            int result = -1;

            try
            {
                string resultadoLlamada;
                string route = "";

                resultadoLlamada = this.Put(
                    $"{MyConfig.Urls.LibreriaVirtual.Domain}", 
                    "/Indicator/GuardarCuentaIndicatorCrm", 
                    null, 
                    route, 
                    String.Format("idUsuarioIndicator={0}", idUsuarioIndicator), 
                    String.Format("IdClienteCrm={0}", IdClienteCrm), 
                    String.Format("IdContactoCrm={0}", IdContactoCrm), 
                    String.Format("idUic={0}", idUic));
                if (!string.IsNullOrWhiteSpace(resultadoLlamada))
                {
                    result = Convert.ToInt32(resultadoLlamada);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error -->{0} Mensaje-->{1}", nameof(GuardarCuentaIndicator), ex);
                result = -2;
            }

            return result;
        }



        public int GuardarClienteCoincidenteIndicator(int idUsuarioIndicator, string strModulo, int IdCrm, string strNombre,  int idUic)
        {
            int result = -1;

            try
            {
                string resultadoLlamada;
                string route = "";

                resultadoLlamada = this.Put(
                    $"{MyConfig.Urls.LibreriaVirtual.Domain}",
                    "/Indicator/GuardarClienteCoincidenteIndicatorCrm",
                    null,
                    route,
                    String.Format("idUsuarioIndicator={0}", idUsuarioIndicator),
                    String.Format("strModulo={0}", strModulo),
                    String.Format("IdCrm={0}", IdCrm),
                    String.Format("strNombre={0}", strNombre),
                    String.Format("idUic={0}", idUic));
                if (!string.IsNullOrWhiteSpace(resultadoLlamada))
                {
                    result = Convert.ToInt32(resultadoLlamada);
                    if (result < 0)
                    {
                        _logger.LogError("Error -->{0} Error al guardar contacto indicator idUsuarioIndicator: {1}, strModulo: {2}, IdClienteCrm:{3} , strNombre:{4}", nameof(GuardarClienteCoincidenteIndicator), idUsuarioIndicator.ToString(),strModulo, IdCrm.ToString(), strNombre);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error -->{0} Mensaje-->{1}", nameof(GuardarClienteCoincidenteIndicator), ex);
                result = -2;
            }

            return result;
        }







    }
}
