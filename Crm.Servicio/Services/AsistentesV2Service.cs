﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Asistentes;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Services
{
    public class AsistentesV2Service : ServicioApiBase<AsistentesV2Service>, IAsistentesService<ResultadoBase>
    {
        private readonly ILogger<AsistentesV2Service> _logger;
        internal ConfigApi objConfig { get; private set; }
        private PeticionesHttpApi<AsistentesV2Service> objPeticionesHttpApi;

        public AsistentesV2Service(
            IOptions<ConfigApi> configAccessor,
            ILogger<AsistentesV2Service> logger
            ) : base(configAccessor, logger)
        {
            _logger = logger;
            objConfig = configAccessor.Value;
            objPeticionesHttpApi = new PeticionesHttpApi<AsistentesV2Service>(_logger);
        }

        public ResultadoBase ComprobarPermisoCreacionAsistente(int IdClienteCrmFacturacion, int IdClienteCrmEnvio, int IdContactoCrm, string strEmailContacto)
        {
            ResultadoOperacion<PermitirAsistente> objPermitirAsistente = new ResultadoOperacion<PermitirAsistente>();
            objPermitirAsistente.oResultado = new PermitirAsistente();
            string url = $"{objConfig.Urls.Evolution.Domain}" + "/web_service/permisoCreacionAsistente/" + IdClienteCrmFacturacion.ToString() + "/" + IdClienteCrmEnvio + "/" + IdContactoCrm + "/" + strEmailContacto;
            objPermitirAsistente.oResultado = objPeticionesHttpApi.GetApi<PermitirAsistente>(url, "", "");
            return objPermitirAsistente;

        }
    }
}
