﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Kojak;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Services
{
    public class ClientLinkService : ServicioApiBase<ClientLinkService>, IClientLinkService<ResultadoBase>
    {

        private readonly ILogger<ClientLinkService> objLogger;
        private PeticionesHttpApi<ClientLinkService> objPeticionesHttpApi;
        internal ConfigApi objConfig { get; private set; }

        public ClientLinkService(
           ILogger<ClientLinkService> objLoggerBase,
           IOptions<ConfigApi> configAccessor
           ) : base(configAccessor, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = configAccessor.Value;
            objPeticionesHttpApi = new PeticionesHttpApi<ClientLinkService>(objLogger);
        }

        public ResultadoBase RecuperarInfoClientLinkBoletin(int IdBoletin, int IdLineaBoletin)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdBoletin: {1} , IdLineaBoletin: {2}  ", nameof(RecuperarInfoClientLinkBoletin), IdBoletin.ToString(), IdLineaBoletin.ToString());
            ResultadoOperacion<ProductoClientLinkExt> objProductoClientLink = new ResultadoOperacion<ProductoClientLinkExt>();
            try
            {
                objProductoClientLink.oResultado = new ProductoClientLinkExt();
                objProductoClientLink.oResultado = objPeticionesHttpApi.GetApi<ProductoClientLinkExt>($"{objConfig.Urls.Evolution.Domain}", "/web_service/infoClientLink/BOLETIN/" + IdBoletin.ToString() + "/" + IdLineaBoletin, "");
               
                if (objProductoClientLink.oResultado != null)
                {
                    objProductoClientLink.eResultado = enumResultado.Ok_Recuperar;
                
                    if (objProductoClientLink.oResultado != null && objProductoClientLink.oResultado.encontrado == true)
                    {
                        objProductoClientLink.sDescripcion = "Se ha encontrado la información del producto client link";
                    }
                    else if (objProductoClientLink.oResultado != null && objProductoClientLink.oResultado.encontrado == false)
                    {
                        objProductoClientLink.sDescripcion = "No se ha encontrado la información del producto client link";
                    }
                }
                else
                {
                    objProductoClientLink.eResultado = enumResultado.Error_Excepcion;
                    objProductoClientLink.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoClientLinkBoletin), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoClientLinkBoletin), ex);
                //conn.Close();
                objProductoClientLink.eResultado = enumResultado.Error_Excepcion;
                objProductoClientLink.sDescripcion = "Error interno.";
            }
            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarInfoClientLinkBoletin), JsonConvert.SerializeObject(objProductoClientLink.oResultado));
            return objProductoClientLink;

        }

        public ResultadoBase RecuperarInfoClientLinkPresupuesto(int IdExpediente, string IdProductoNav, string Variante)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdExpediente: {1} , IdProductoNav: {2} , Variante: {3}   ", nameof(RecuperarInfoClientLinkPresupuesto), IdExpediente.ToString(), IdProductoNav, Variante);
            ResultadoOperacion<ProductoClientLinkExt> objProductoClientLink = new ResultadoOperacion<ProductoClientLinkExt>();
            try
            {
                objProductoClientLink.oResultado = new ProductoClientLinkExt();
                objProductoClientLink.oResultado = objPeticionesHttpApi.GetApi<ProductoClientLinkExt>($"{objConfig.Urls.Evolution.Domain}", "/web_service/infoClientLink/PRESUPUESTO/" + IdExpediente.ToString() + "/" + IdProductoNav + "/" + Variante, "");
                
                if (objProductoClientLink.oResultado != null)
                {
                    objProductoClientLink.eResultado = enumResultado.Ok_Recuperar;
               
                    if (objProductoClientLink.oResultado != null && objProductoClientLink.oResultado.encontrado == true)
                    {
                        objProductoClientLink.sDescripcion = "Se ha encontrado la información del producto client link";
                    }
                    else if (objProductoClientLink.oResultado != null && objProductoClientLink.oResultado.encontrado == false)
                    {
                        objProductoClientLink.sDescripcion = "No se ha encontrado la información del producto client link";
                    }
                }
                else
                {
                    objProductoClientLink.eResultado = enumResultado.Error_Excepcion;
                    objProductoClientLink.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoClientLinkPresupuesto), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoClientLinkPresupuesto), ex);
                //conn.Close();
                objProductoClientLink.eResultado = enumResultado.Error_Excepcion;
                objProductoClientLink.sDescripcion = "Error interno.";
            }
            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarInfoClientLinkPresupuesto), JsonConvert.SerializeObject(objProductoClientLink.oResultado));
            return objProductoClientLink;

        }
    }
}
