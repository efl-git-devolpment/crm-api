﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using DllApiBase.Vistas.Permisos;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.ServiceModel;

namespace Crm.Servicio
{
    public class ExternalV2Service<T> : ServicioApiBase<T>, IExternalV2Service<ResultadoBase>
    {

        private readonly PeticionesHttpApi<T> objPeticionesHttpApi;

        internal ConfigApi objConfig { get; private set; }
        internal static ILogger<T> objLogger { get; private set; }

        public ExternalV2Service(
            IOptions<ConfigApi> objConfigBase,
            ILogger<T> objLoggerBase
            ) : base(objConfigBase, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;

            objPeticionesHttpApi = new PeticionesHttpApi<T>(objLoggerBase);

            var basicHttpBindingClaves = new BasicHttpBinding(BasicHttpSecurityMode.None)
            {
                MaxReceivedMessageSize = int.MaxValue,
                MaxBufferSize = int.MaxValue
            };

        }


        public ResultadoBase RecuperarUsuariosPermiso(int IdClienteNav, string strPermiso)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarUsuariosPermiso), IdClienteNav.ToString(), strPermiso);

            ResultadoLista<PermisoUsuarios> objResultadoApi = new ResultadoLista<PermisoUsuarios>();

            try
            {

                string urlBaseApi = this.objConfig.cfgUrlsApi.OnlineApi;
                string strPrams = $"IdClienteNav={IdClienteNav}&strPermiso={strPermiso}";

                string[] lstQueryParams = new string[] { (string)strPrams };

                objResultadoApi = this.objPeticionesHttpApi.GetApi<ResultadoLista<PermisoUsuarios>>(urlBaseApi, "/Permisos/RecuperarUsuariosPermiso", string.Empty, lstQueryParams);


            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarUsuariosPermiso), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarUsuariosPermiso), ex);

                objResultadoApi.sDescripcion = ex.Message;

            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarUsuariosPermiso), objResultadoApi.sDescripcion);
            return objResultadoApi;
        }

        public ResultadoBase RecuperarUsuarioProducto(int IdClienteNav, string IdUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarUsuarioProducto), IdClienteNav.ToString(), IdUsuarioPro);

            ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro> objResultadoApi = new ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>();

            try
            {

                string urlBaseApi = this.objConfig.cfgUrlsApi.ClavesApi;
                string strPrams = $"IdClienteNav={IdClienteNav}&IdUsuarioPro={IdUsuarioPro}";

                string[] lstQueryParams = new string[] { (string)strPrams };

                objResultadoApi = this.objPeticionesHttpApi.GetApi<ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>>(urlBaseApi, "/UsuariosProducto/RecuperarUsuarioProducto", string.Empty, lstQueryParams);

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarUsuarioProducto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarUsuarioProducto), ex);

                objResultadoApi = null;

            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarUsuarioProducto), objResultadoApi.sDescripcion);
            return objResultadoApi;
        }

        //public ResultadoBase RecuperarUsuariosProducto(int IdClienteNav, int iIdEstado, int iIdSuscripcionNav, string strBusqueda, int iIndDemo, int iIndSuscripcionAsociada, int iIdTipoUsuarioProducto, string strFechaDesde, string strFechaHasta, int iNumPage, int iNumSize)
        //{
        //    objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarUsuariosProducto), IdClienteNav.ToString(), strBusqueda);

        //    ResultadoLista<DllApiBase.Vistas.Claves.Usuario.UsuarioPro> objResultadoApi = new ResultadoLista<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>();

        //    try
        //    {

        //        string urlBaseApi = this.objConfig.cfgUrlsApi.ClavesApi;
        //        string strPrams = string.Empty;

        //        Dictionary<string, string> objBody = new Dictionary<string, string>();
        //        objBody.Add("idClienteNav", IdClienteNav.ToString());
        //        objBody.Add("iIdEstado", iIdEstado.ToString());
        //        objBody.Add("iIdSuscripcionNav", iIdSuscripcionNav.ToString());
        //        objBody.Add("strBusqueda", strBusqueda);
        //        objBody.Add("iIndDemo", iIndDemo.ToString());
        //        objBody.Add("iIndSuscripcionAsociada", iIndSuscripcionAsociada.ToString());
        //        objBody.Add("iIdTipoUsuarioProducto", iIdTipoUsuarioProducto.ToString());
        //        objBody.Add("strFechaDesde", strFechaDesde);
        //        objBody.Add("strFechaHasta", strFechaDesde);
        //        objBody.Add("iNumPage", iNumPage.ToString());
        //        objBody.Add("iNumSize", iNumSize.ToString());

        //        string[] lstQueryParams = new string[] { (string)strPrams };

        //        objResultadoApi = this.objPeticionesHttpApi.GetApi<ResultadoLista<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>>(urlBaseApi, "/UsuariosProducto/RecuperarUsuariosProducto", string.Empty, objBody, lstQueryParams);

        //    }
        //    catch (Exception ex)
        //    {
        //        objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarUsuarioProducto), ex.Message);
        //        objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarUsuarioProducto), ex);

        //        objResultadoApi = null;

        //    }

        //    objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarUsuarioProducto), objResultadoApi.sDescripcion);
        //    return objResultadoApi;
        //}

    }


}
