﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Kojak;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Servicio.Services
{
    public class GenialService : ServicioApiBase<GenialService>, IGenialService
    {

        private readonly ILogger<GenialService> objLogger;
        private PeticionesHttpApi<GenialService> objPeticionesHttpApi;
        internal ConfigApi objConfig { get; private set; }

        public GenialService(
          ILogger<GenialService> objLoggerBase,
          IOptions<ConfigApi> objConfigBase
          ) : base(objConfigBase, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
            objPeticionesHttpApi = new PeticionesHttpApi<GenialService>(objLogger);
        }

        public ResultadoBase RecuperarInfoGenialBoletin(int IdBoletin, int IdLineaBoletin)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdBoletin: {1} , IdLineaBoletin: {2}  ", nameof(RecuperarInfoGenialBoletin), IdBoletin.ToString(), IdLineaBoletin.ToString());
            ResultadoOperacion<ProductoGenialExt> objProductoGenial = new ResultadoOperacion<ProductoGenialExt>();
            try
            {
                objProductoGenial.oResultado = new ProductoGenialExt();
                objProductoGenial.oResultado = objPeticionesHttpApi.GetApi<ProductoGenialExt>($"{objConfig.Urls.Evolution.Domain}", "/web_service/infoGenial/BOLETIN/" + IdBoletin.ToString() + "/" + IdLineaBoletin, "");

                if (objProductoGenial.oResultado != null)
                {
                    objProductoGenial.eResultado = enumResultado.Ok_Recuperar;
                    if (objProductoGenial.oResultado != null && objProductoGenial.oResultado.encontrado == true)
                    {
                        objProductoGenial.sDescripcion = "Se ha encontrado la información del producto Genial";
                    }
                    else if (objProductoGenial.oResultado != null && objProductoGenial.oResultado.encontrado == false)
                    {
                        objProductoGenial.sDescripcion = "No se ha encontrado la información del producto Genial";
                    }
                }
                else
                {
                    objProductoGenial.eResultado = enumResultado.Error_Excepcion;
                    objProductoGenial.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoGenialBoletin), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoGenialBoletin), ex);
                //conn.Close();
                objProductoGenial.eResultado = enumResultado.Error_Excepcion;
                objProductoGenial.sDescripcion = "Error interno.";
            }

            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarInfoGenialBoletin), JsonConvert.SerializeObject(objProductoGenial.oResultado));
            return objProductoGenial;

        }

        public ResultadoBase RecuperarInfoGenialPresupuesto(int IdExpediente, string IdProductoNav, string Variante)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdExpediente: {1} , IdProductoNav: {2} , Variante: {3}   ", nameof(RecuperarInfoGenialPresupuesto), IdExpediente.ToString(), IdProductoNav, Variante);
            ResultadoOperacion<ProductoGenialExt> objProductoGenial = new ResultadoOperacion<ProductoGenialExt>();
            try
            {
                objProductoGenial.oResultado = new ProductoGenialExt();
                objProductoGenial.oResultado = objPeticionesHttpApi.GetApi<ProductoGenialExt>($"{objConfig.Urls.Evolution.Domain}", "/web_service/infoGenial/PRESUPUESTO/" + IdExpediente.ToString() + "/" + IdProductoNav + "/" + Variante, "");

                if (objProductoGenial.oResultado != null)
                {
                    objProductoGenial.eResultado = enumResultado.Ok_Recuperar;

                    if (objProductoGenial.oResultado != null && objProductoGenial.oResultado.encontrado == true)
                    {
                        objProductoGenial.sDescripcion = "Se ha encontrado la información del producto Genial";
                    }
                    else if (objProductoGenial.oResultado != null && objProductoGenial.oResultado.encontrado == false)
                    {
                        objProductoGenial.sDescripcion = "No se ha encontrado la información del producto Genial";
                    }
                }
                else
                {
                    objProductoGenial.eResultado = enumResultado.Error_Excepcion;
                    objProductoGenial.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoGenialPresupuesto), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoGenialPresupuesto), ex);
                objProductoGenial.eResultado = enumResultado.Error_Excepcion;
                objProductoGenial.sDescripcion = "Error interno.";
            }
            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarInfoGenialPresupuesto), JsonConvert.SerializeObject(objProductoGenial.oResultado));
            return objProductoGenial;

        }
    }
}
