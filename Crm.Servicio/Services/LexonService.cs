﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Contactos;
using Crm.Vistas.Crm;
using Crm.Vistas.Kojak;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Services
{
    public class LexonService : ServicioApiBase<LexonService>, ILexonService<ResultadoBase>
    {
        private readonly ILogger<LexonService> objLogger;
        private PeticionesHttpApi<LexonService> objPeticionesHttpApi;
        internal ConfigApi objConfig { get; private set; }

        public LexonService(
           ILogger<LexonService> objLoggerBase,
           IOptions<ConfigApi> objConfigBase
           ) : base(objConfigBase, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
            objPeticionesHttpApi = new PeticionesHttpApi<LexonService>(objLogger);
        }

        public ResultadoBase RecuperarInfoLexonBoletin(int IdBoletin, int IdLineaBoletin)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdBoletin: {1} , IdLineaBoletin: {2}  ", nameof(RecuperarInfoLexonBoletin), IdBoletin.ToString(), IdLineaBoletin.ToString());
            ResultadoOperacion<ProductoLexonExt> objProductoLexon = new ResultadoOperacion<ProductoLexonExt>();
            try
            {
                objProductoLexon.oResultado = new ProductoLexonExt();
                objProductoLexon.oResultado = objPeticionesHttpApi.GetApi<ProductoLexonExt>($"{objConfig.Urls.Evolution.Domain}", "/web_service/infoLexon/BOLETIN/" + IdBoletin.ToString() + "/" + IdLineaBoletin, "");

                if (objProductoLexon.oResultado != null )
                {
                    objProductoLexon.eResultado = enumResultado.Ok_Recuperar;
                    if (objProductoLexon.oResultado != null && objProductoLexon.oResultado.encontrado == true)
                    {
                        objProductoLexon.sDescripcion = "Se ha encontrado la información del producto Lexon";
                    }
                    else if (objProductoLexon.oResultado != null && objProductoLexon.oResultado.encontrado == false)
                    {
                        objProductoLexon.sDescripcion = "No se ha encontrado la información del producto Lexon";
                    }
                }
                else
                {
                    objProductoLexon.eResultado = enumResultado.Error_Excepcion;
                    objProductoLexon.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoLexonBoletin), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoLexonBoletin), ex);
                //conn.Close();
                objProductoLexon.eResultado = enumResultado.Error_Excepcion;
                objProductoLexon.sDescripcion = "Error interno.";
            }

            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarInfoLexonBoletin), JsonConvert.SerializeObject(objProductoLexon.oResultado));
            return objProductoLexon;

        }

        public ResultadoBase RecuperarInfoLexonPresupuesto(int IdExpediente, string IdProductoNav, string Variante)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdExpediente: {1} , IdProductoNav: {2} , Variante: {3}   ", nameof(RecuperarInfoLexonPresupuesto), IdExpediente.ToString(), IdProductoNav , Variante);
            ResultadoOperacion<ProductoLexonExt> objProductoLexon = new ResultadoOperacion<ProductoLexonExt>();
            try
            {
                objProductoLexon.oResultado = new ProductoLexonExt();
                objProductoLexon.oResultado = objPeticionesHttpApi.GetApi<ProductoLexonExt>($"{objConfig.Urls.Evolution.Domain}", "/web_service/infoLexon/PRESUPUESTO/" + IdExpediente.ToString() + "/" + IdProductoNav + "/" + Variante, "");
                
                if (objProductoLexon.oResultado != null)
                {
                    objProductoLexon.eResultado = enumResultado.Ok_Recuperar;
               
                    if (objProductoLexon.oResultado != null && objProductoLexon.oResultado.encontrado == true)
                    {
                        objProductoLexon.sDescripcion = "Se ha encontrado la información del producto Lexon";
                    }
                    else if (objProductoLexon.oResultado != null && objProductoLexon.oResultado.encontrado == false)
                    {
                        objProductoLexon.sDescripcion = "No se ha encontrado la información del producto Lexon";
                    }
                }
                else
                {
                    objProductoLexon.eResultado = enumResultado.Error_Excepcion;
                    objProductoLexon.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoLexonPresupuesto), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoLexonPresupuesto), ex);                
                objProductoLexon.eResultado = enumResultado.Error_Excepcion;
                objProductoLexon.sDescripcion = "Error interno.";
            }
            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarInfoLexonPresupuesto), JsonConvert.SerializeObject(objProductoLexon.oResultado));
            return objProductoLexon;

        }


        public ResultadoBase RecuperarDatosContactoExpedienteBaja(int IdClienteCRM, int NumSuscripcion)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdKojak: {1} , IdSuscripcion: {2}  ", nameof(RecuperarDatosContactoExpedienteBaja), IdClienteCRM.ToString(), NumSuscripcion);
            ResultadoOperacion<ResultadoContactoExpedienteBaja> objResultado = new ResultadoOperacion<ResultadoContactoExpedienteBaja>();
            try
            {
                
                objResultado.oResultado = new ResultadoContactoExpedienteBaja();
                objResultado.oResultado = objPeticionesHttpApi.GetApi<ResultadoContactoExpedienteBaja>($"{objConfig.Urls.Evolution.Domain}", "/web_service/obtenerInfoContactoExpedienteBaja/" + IdClienteCRM.ToString() + "/" + NumSuscripcion,"");

                if (objResultado.oResultado != null)
                {
                    objResultado.eResultado = enumResultado.Ok_Recuperar;

                    if (objResultado.oResultado != null && objResultado.oResultado.codigoResultado == 0)
                    {
                        objResultado.sDescripcion = "Se ha encontrado la información de los datos de contacto";
                    }
                    else if (objResultado.oResultado != null && objResultado.oResultado.codigoResultado != 0)
                    {
                        objResultado.sDescripcion = "No se ha encontrado la información de los datos de contacto";
                    }
                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_Excepcion;
                    objResultado.sDescripcion = "Error interno.";
                }
            }
            catch (Exception ex)
            {

                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarDatosContactoExpedienteBaja), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarDatosContactoExpedienteBaja), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = "Error interno."; ;
            }
            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarDatosContactoExpedienteBaja), JsonConvert.SerializeObject(objResultado.oResultado));
            return objResultado;
        }

    }
}
