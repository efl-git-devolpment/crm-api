﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Pedidos;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Services
{
    public class PedidosService : ServicioApiBase<PedidosService>, IPedidosService<ResultadoBase>
    {
        private readonly ILogger<PedidosService> _logger;
        internal ConfigApi objConfig { get; private set; }
        private PeticionesHttpApi<PedidosService> objPeticionesHttpApi;

        public PedidosService(
            IOptions<ConfigApi> configAccessor,
            ILogger<PedidosService> logger
            ) : base(configAccessor, logger)
        {
            _logger = logger;
            objConfig = configAccessor.Value;
            objPeticionesHttpApi = new PeticionesHttpApi<PedidosService>(_logger);
        }

        public ResultadoBase RecuperarTipoPedido(int intClasificacion)
        {
            ResultadoOperacion<TiposPedidos> objTiposDePedidos = new ResultadoOperacion<TiposPedidos>();
            objTiposDePedidos.oResultado = new TiposPedidos();
            string url = $"{objConfig.Urls.Evolution.Domain}" + "/web_service/dameTipoPedido/" + intClasificacion.ToString();
            objTiposDePedidos.oResultado = objPeticionesHttpApi.GetApi<TiposPedidos>(url, "", "");
            objTiposDePedidos.sDescripcion = objTiposDePedidos.oResultado.codigoResultado ? "La comprobación ha sido correcta." : "La comprobación ha sido incorrecta";
            return objTiposDePedidos;

        }
    }
}
