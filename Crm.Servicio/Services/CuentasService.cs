﻿using Crm.Comun.Configuration;
using Crm.Modelos.Entidades.Crm;
using Crm.Modelos.Entidades.CrmReplica;
using Crm.Modelos.Extendidos;
using Crm.Modelos.Repositorios.Interfaces;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Contactos;
using Crm.Vistas.Crm;
using Crm.Vistas.LibreriaVirtual;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crm.Servicio.Services
{
    public class CuentasService : BaseService<CuentasService>, ICuentasService
    {
        /****************************PARA RECUPERAR CONTACTOS HAY QUE LLAMAR A REPLICA Y PARA CREAR  CONTACTO O MODIFICAR HAY QUE LLAMAR AL NORMAL*******/
        private readonly CrmReplicaMySqlDbContext _contextReplica;
        private readonly CrmMySqlDbContext _context;
        private readonly ILogger<CuentasService> _logger;
        private readonly ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> _serviceCrm;
        private readonly ILibreriaVirtualService<UsuariosIndicator> _serviceLV;
        private readonly IContactosService<ContactoListadoExt, ContactoCreacion, MySqlConnection, ContactoModificacion> _serviceContactos;
        private readonly IContactosRepository<MySqlConnection, CrContactoListadoExt, CrContactoCreacion, CrContactoModificacion> _repoContactos;

        public CuentasService(
            CrmReplicaMySqlDbContext contextReplica,
            CrmMySqlDbContext context,
            ILogger<CuentasService> logger,
            ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> serviceCrm,
            ILibreriaVirtualService<UsuariosIndicator> serviceLV,
            IContactosService<ContactoListadoExt, ContactoCreacion, MySqlConnection, ContactoModificacion> serviceContactos,
            IContactosRepository<MySqlConnection, CrContactoListadoExt, CrContactoCreacion, CrContactoModificacion> repoContactos,
            IOptions<ConfigApi> configAccessor
            ) : base(configAccessor)
        {
            _contextReplica = contextReplica;
            _context = context;
            _logger = logger;
            _repoContactos = repoContactos;
            _serviceCrm = serviceCrm;
            _serviceLV = serviceLV;
            _serviceContactos = serviceContactos;
        }

        public async Task<int> CargarCuentasIndicatorAsync()
        {
            int result = -1;
            List<UsuariosIndicator> UsuariosIndicator = new List<UsuariosIndicator>();
            UsuariosIndicator = _serviceLV.RecuperarCuentasIndicatorPedidos();
            _logger.LogDebug("Metodo -->{0} Numero usuarios indicator-->{1}", nameof(CargarCuentasIndicatorAsync), UsuariosIndicator.Count());
            MySqlConnection connREPLICA = new MySqlConnection();
            connREPLICA.ConnectionString = _contextReplica.Database.GetDbConnection().ConnectionString;
            connREPLICA.Open();

            MySqlConnection conn = new MySqlConnection();
            conn.ConnectionString = _context.Database.GetDbConnection().ConnectionString;
            conn.Open();
            try
            {

                string value = "ClientesIndicator-LibreriaVirtualIndidator";
                string key = "aZ4DQEVxGadr9lgxvfW95HhTDvnItb0l";
                String OrigenSolicitud = Aes256CbcEncrypter.Encrypt(value, key);
                foreach (var item in UsuariosIndicator)
                {
                    _logger.LogDebug("metodo->{0}, IdusuarioIndicator:{1}", nameof(CargarCuentasIndicatorAsync), item.IdUsuarioIndicator);
                    ResultadoCuenta objResultadoIndicator = new ResultadoCuenta();
                    if (item.IdClienteCrm > 0)
                    {
                        objResultadoIndicator.numeroCuenta = item.IdClienteCrm.ToString();
                    }
                    else
                    {
                        objResultadoIndicator = await _serviceCrm.CargarCuentaIndicatorEnCrm(item, OrigenSolicitud);
                    }

                    if (objResultadoIndicator != null && !string.IsNullOrWhiteSpace(objResultadoIndicator.numeroCuenta))
                    {
                        int? IdContactoCrm = null;
                        if (!string.IsNullOrWhiteSpace(objResultadoIndicator.numeroContacto))
                        {
                            IdContactoCrm = Convert.ToInt32(objResultadoIndicator.numeroContacto);
                        }
                        _serviceLV.GuardarCuentaIndicator(item.IdUsuarioIndicator, Convert.ToInt32(objResultadoIndicator.numeroCuenta), IdContactoCrm, 1);

                    }
                }
                result = 1;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error -->{0} Error-->{1}", nameof(CargarCuentasIndicatorAsync), ex);
                result = -2;
                connREPLICA.Close();
                conn.Close();
            }
            finally
            {
                connREPLICA.Close();
                conn.Close();
            }


            return result;
        }
    }
}
