﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.CasoSac;
using Crm.Vistas.Crm;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using DllApiBase.Vistas.Claves.Usuario;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crm.Servicio.Services
{
    public class CasosSacService : ServicioApiBase<CasosSacService>, ICasosSacService<ResultadoBase, CasoSac>
    {
        private readonly ILogger<CasosSacService> _logger;
        internal ConfigApi objConfig { get; private set; }
        private PeticionesHttpApi<CasosSacService> objPeticionesHttpApi;

        public CasosSacService(
            IOptions<ConfigApi> configAccessor,
            ILogger<CasosSacService> logger
            ) : base(configAccessor, logger)
        {
            _logger = logger;
            objConfig = configAccessor.Value;
            objPeticionesHttpApi = new PeticionesHttpApi<CasosSacService>(_logger);
        }

        public ResultadoBase CrearCasoSac(CasoSac objPeticion)
        {
            ResultadoCadena objResultado = new ResultadoCadena();
            try
            {
                ResultadoHttp objResultadoCrm = new ResultadoHttp();
                CasoSacCrm objCasoSac = ConvertirCasoSac(objPeticion);
                objResultadoCrm = objPeticionesHttpApi.PostHttp($"{objConfig.Urls.Evolution.Domain}", "/web_service/creaCasoSac", string.Empty, objCasoSac, "") ;
                objResultado.eResultado = enumResultado.Ok_Recuperar;
                if (!string.IsNullOrWhiteSpace(objResultadoCrm.sDescripcionHttp))
                {
                    _logger.LogDebug("Resultado creaCasoSac -->{0}", objResultadoCrm.sDescripcionHttp);
                    ResultadoCasoSac objResultadoCasoSac = JsonConvert.DeserializeObject<ResultadoCasoSac>(objResultadoCrm.sDescripcionHttp);
                    if (!string.IsNullOrWhiteSpace(objResultadoCasoSac.idCasoSac))
                    {
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                        objResultado.sDescripcion = "Resultado Ok";
                        objResultado.sResultado = objResultadoCasoSac.idCasoSac;
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Excepcion;
                        objResultado.sDescripcion = "Error interno.";
                    }
                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_Excepcion;
                    objResultado.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = "Error interno.";
            }
            return objResultado;

        }

        public ResultadoBase ModificarCasoSac(CasoSac objPeticion)
        {
            ResultadoCadena objResultado = new ResultadoCadena();
            try
            {
                ResultadoHttp objResultadoCrm = new ResultadoHttp();
                CasoSacCrm objCasoSac = ConvertirCasoSac(objPeticion);
                objResultadoCrm = objPeticionesHttpApi.PostHttp($"{objConfig.Urls.Evolution.Domain}", "/web_service/actualizaCasoSac", string.Empty, objCasoSac, "");
                objResultado.eResultado = enumResultado.Ok_Recuperar;
                if (!string.IsNullOrWhiteSpace(objResultadoCrm.sDescripcionHttp))
                {
                    _logger.LogDebug("Resultado actualizaCasoSac -->{0}", objResultadoCrm.sDescripcionHttp);
                    ResultadoCasoSac objResultadoCasoSac = JsonConvert.DeserializeObject<ResultadoCasoSac>(objResultadoCrm.sDescripcionHttp);
                    if (!string.IsNullOrWhiteSpace(objResultadoCasoSac.idCasoSac))
                    {
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                        objResultado.sDescripcion = "Resultado Ok";
                        objResultado.sResultado = objResultadoCasoSac.idCasoSac;
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Excepcion;
                        objResultado.sDescripcion = "Error interno.";
                    }
                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_Excepcion;
                    objResultado.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = "Error interno.";
            }
            return objResultado;

        }

        public ResultadoBase RecuperarTipoCasoSac()
        {
            ResultadoLista<TipoCasoSac> objResultado = new ResultadoLista<TipoCasoSac>();
            try
            {
                ResultadoHttp objResultadoCrm = new ResultadoHttp();
                objResultadoCrm = objPeticionesHttpApi.GetHttp($"{objConfig.Urls.Evolution.Domain}", "/web_service/devuelveTipologiaCasoSac", "");
                objResultado.eResultado = enumResultado.Ok_Recuperar;
                if (!string.IsNullOrWhiteSpace(objResultadoCrm.sDescripcionHttp))
                {
                    _logger.LogDebug("Resultado actualizaCasoSac -->{0}", objResultadoCrm.sDescripcionHttp);
                    List<TipoCasoSacCrm> LstTipoCasoSac = JsonConvert.DeserializeObject<List<TipoCasoSacCrm>>(objResultadoCrm.sDescripcionHttp);
                    if (LstTipoCasoSac.Count > 0)
                    {
                        objResultado = ConvertirTipoCasoSac(LstTipoCasoSac);
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Excepcion;
                        objResultado.sDescripcion = "Error interno.";
                    }
                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_Excepcion;
                    objResultado.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = "Error interno.";
            }
            return objResultado;

        }


        public ResultadoLista<TipoCasoSac> ConvertirTipoCasoSac(List<TipoCasoSacCrm> lstTipoCasoSac)
        {
            ResultadoLista<TipoCasoSac> objResultado = new ResultadoLista<TipoCasoSac>();
            objResultado.lstResultado = new List<TipoCasoSac>();
            int? intNull = null;
            foreach (var objTipoCasoSacCrm in lstTipoCasoSac)
            {
                TipoCasoSac objtipoCasoSac = new TipoCasoSac();
                objtipoCasoSac.DescripcionSubmotivo = (!string.IsNullOrEmpty(objTipoCasoSacCrm.descripcion_submotivo) ? Convert.ToInt32(objTipoCasoSacCrm.descripcion_submotivo) : intNull);
                objtipoCasoSac.NombreDescripcionSubmotivo = objTipoCasoSacCrm.nombre_descripcion_submotivo;
                objtipoCasoSac.DescripcionTipo = objTipoCasoSacCrm.descripcion_tipo;
                objtipoCasoSac.DetalleGestion = objTipoCasoSacCrm.detalle_gestion;
                objtipoCasoSac.EstadoCaso = objTipoCasoSacCrm.estado_caso;
                objtipoCasoSac.Id = Convert.ToInt32(objTipoCasoSacCrm.id);
                objtipoCasoSac.Motivo = Convert.ToInt32(objTipoCasoSacCrm.motivo);
                objtipoCasoSac.NombreMotivo = objTipoCasoSacCrm.nombre_motivo;
                objtipoCasoSac.NombreTipo = objTipoCasoSacCrm.nombre_tipo;
                objtipoCasoSac.resultado = objTipoCasoSacCrm.resultado;
                objtipoCasoSac.Submotivo = Convert.ToInt32(objTipoCasoSacCrm.submotivo);
                objtipoCasoSac.NombreSubmotivo = objTipoCasoSacCrm.nombre_submotivo;
                objtipoCasoSac.Tipo = Convert.ToInt32(objTipoCasoSacCrm.tipo);
                objResultado.lstResultado.Add(objtipoCasoSac);
            }
            objResultado.iContador = objResultado.lstResultado.Count();
            objResultado.sDescripcion = "Resultado Ok";
            return objResultado;
        }
        public CasoSacCrm ConvertirCasoSac(CasoSac objCasoSac)
        {
            CasoSacCrm objResultado = new CasoSacCrm();


            objResultado.origen = "web";
            objResultado.id_cuenta = (objCasoSac.IdCuentaCrm > 0) ? objCasoSac.IdCuentaCrm.ToString() : "";
            objResultado.id_contacto = (objCasoSac.IdContactoCrm > 0) ? objCasoSac.IdContactoCrm.ToString() : "";
            objResultado.tipo_caso = objCasoSac.IdTipoCaso;
            objResultado.canal_primer_contacto = objCasoSac.CanalPrimerContacto;
            objResultado.sentido_primer_contacto = objCasoSac.SentidoPrimerContacto;
            objResultado.motivo_pendiente = objCasoSac.MotivoPendiente;
            objResultado.accion_realizada = objCasoSac.AccionRealizada;
            objResultado.comentarios = objCasoSac.Comentarios;
            objResultado.id_casosac = objCasoSac.IdCasosac;
            if (objCasoSac.IdUsuarioApp > 0)
            {
                ResultadoOperacion<UsuarioApp> objUsuario = objPeticionesHttpApi.GetApi<ResultadoOperacion<UsuarioApp>>(
                objConfig.cfgUrlsApi.ClavesApi,
                "/UsuariosAplicacion/RecuperarUsuarioAppById",
                null,
                "",
                String.Format("IdUsuarioApp={0}", objCasoSac.IdUsuarioApp));
                if (objUsuario != null && objUsuario.oResultado != null && objUsuario.oResultado.IdUsuarioApp > 0)
                {
                    objResultado.createdByName = objUsuario.oResultado.IdUsuarioLdap;
                    objResultado.agente_gestor_usuarios = objUsuario.oResultado.IdUsuarioSugar;
                    objResultado.agente = objUsuario.oResultado.IdUsuarioSugar;
                }
            }
            if (objCasoSac.SuscripcionesRelacionadas.Count > 0)
            {
                foreach(var ObjSuscripcionRelacionada in objCasoSac.SuscripcionesRelacionadas)
                {
                    if(!string.IsNullOrWhiteSpace(ObjSuscripcionRelacionada.IdProductoNav) && !string.IsNullOrWhiteSpace(ObjSuscripcionRelacionada.DescProductoNav) && ObjSuscripcionRelacionada.IdSuscripcionNav > 0)
                    {
                        if (!string.IsNullOrWhiteSpace(objResultado.productos_relacionados))
                        {
                            objResultado.productos_relacionados = objResultado.productos_relacionados + ",";
                        }
                        objResultado.productos_relacionados = objResultado.productos_relacionados + ObjSuscripcionRelacionada.IdSuscripcionNav+ "#Susc. "
                            + ObjSuscripcionRelacionada.IdSuscripcionNav + " - "+ ObjSuscripcionRelacionada.IdProductoNav + ". "+ ObjSuscripcionRelacionada.DescProductoNav;
                    }
                    
                }
            }
            return objResultado;
        }
    }
}
