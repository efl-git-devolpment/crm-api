﻿using Crm.Comun.Configuration;
using Crm.Vistas.Crm;
using Crm.Vistas.Kojak;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Crm.Servicio.Interfaces
{
    public class PortalClienteService : ServicioApiBase<PortalClienteService>, IPortalClienteService<ResultadoBase>
    {
        private readonly ILogger<PortalClienteService> objLogger;
        private PeticionesHttpApi<PortalClienteService> objPeticionesHttpApi;
        internal ConfigApi objConfig { get; private set; }

        public PortalClienteService(
           ILogger<PortalClienteService> objLoggerBase,
           IOptions<ConfigApi> configAccessor
           ) : base(configAccessor, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = configAccessor.Value;
            objPeticionesHttpApi = new PeticionesHttpApi<PortalClienteService>(objLogger);
        }

        public ResultadoBase RecuperarInfoPortalClienteBoletin(int IdBoletin, int IdLineaBoletin)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdBoletin: {1} , IdLineaBoletin: {2}  ", nameof(RecuperarInfoPortalClienteBoletin), IdBoletin.ToString(), IdLineaBoletin.ToString());
            ResultadoOperacion<ProductoPortalClienteExt> objProducotPortalCliente = new ResultadoOperacion<ProductoPortalClienteExt>();
            try
            {
                objProducotPortalCliente.oResultado = new ProductoPortalClienteExt();
                objProducotPortalCliente.oResultado = objPeticionesHttpApi.GetApi<ProductoPortalClienteExt>($"{objConfig.Urls.Evolution.Domain}", "/web_service/infoPortalCliente/BOLETIN/" + IdBoletin.ToString() + "/" + IdLineaBoletin, "");
    
                if (objProducotPortalCliente.oResultado != null)
                {
                    objProducotPortalCliente.eResultado = enumResultado.Ok_Recuperar;
                
                    if (objProducotPortalCliente.oResultado != null && objProducotPortalCliente.oResultado.encontrado == true)
                    {
                        objProducotPortalCliente.sDescripcion = "Se ha encontrado información del boletín";
                    }
                    else if (objProducotPortalCliente.oResultado != null && objProducotPortalCliente.oResultado.encontrado == false)
                    {
                        objProducotPortalCliente.sDescripcion = "No se ha encontrado información del boletín";
                    }
                }
                else
                {
                    objProducotPortalCliente.eResultado = enumResultado.Error_Excepcion;
                    objProducotPortalCliente.sDescripcion = "Error interno.";
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoPortalClienteBoletin), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoPortalClienteBoletin), ex);
                //conn.Close();
                objProducotPortalCliente.eResultado = enumResultado.Error_Excepcion;
                objProducotPortalCliente.sDescripcion = "Error interno.";
            }
            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarInfoPortalClienteBoletin), JsonConvert.SerializeObject(objProducotPortalCliente.oResultado));
            return objProducotPortalCliente;

        }

        public ResultadoBase RecuperarInfoPortalClientePresupuesto(int IdExpediente, string IdProductoNav, string Variante)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdExpediente: {1} , IdProductoNav: {2} , Variante: {3}   ", nameof(RecuperarInfoPortalClientePresupuesto), IdExpediente.ToString(), IdProductoNav, Variante);
            ResultadoOperacion<ProductoPortalClienteExt> objProducotPortalCliente = new ResultadoOperacion<ProductoPortalClienteExt>();
            try
            {
                objProducotPortalCliente.oResultado = new ProductoPortalClienteExt();
                objProducotPortalCliente.oResultado = objPeticionesHttpApi.GetApi<ProductoPortalClienteExt>($"{objConfig.Urls.Evolution.Domain}", "/web_service/infoPortalCliente/PRESUPUESTO/" + IdExpediente.ToString() + "/" + IdProductoNav + "/" + Variante, "");
                
                if (objProducotPortalCliente.oResultado != null)
                {
                    objProducotPortalCliente.eResultado = enumResultado.Ok_Recuperar;
               
                    if (objProducotPortalCliente.oResultado != null && objProducotPortalCliente.oResultado.encontrado == true)
                    {
                        objProducotPortalCliente.sDescripcion = "Se ha encontrado información del boletín";
                    }
                    else if (objProducotPortalCliente.oResultado != null && objProducotPortalCliente.oResultado.encontrado == false)
                    {
                        objProducotPortalCliente.sDescripcion = "No se ha encontrado información del boletín";
                    }
                }
                else
                {
                    objProducotPortalCliente.eResultado = enumResultado.Error_Excepcion;
                    objProducotPortalCliente.sDescripcion = "No se recuperan datos de Kojak";
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoPortalClientePresupuesto), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarInfoPortalClientePresupuesto), ex);
                //conn.Close();
                objProducotPortalCliente.eResultado = enumResultado.Error_Excepcion;
                objProducotPortalCliente.sDescripcion = "Error interno.";
            }
            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarInfoPortalClientePresupuesto), JsonConvert.SerializeObject(objProducotPortalCliente.oResultado));
            return objProducotPortalCliente;

        }

       
    }
}
