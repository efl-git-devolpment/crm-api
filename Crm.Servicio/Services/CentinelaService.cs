﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Kojak;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Services
{
    public class CentinelaService : ServicioApiBase<CentinelaService>, ICentinelaService<ResultadoBase>
    {
        private readonly ILogger<CentinelaService> objLogger;
        private PeticionesHttpApi<CentinelaService> objPeticionesHttpApi;
        internal ConfigApi objConfig { get; private set; }
        public CentinelaService(
            ILogger<CentinelaService> objLoggerBase,
            IOptions<ConfigApi> cfcConfigApiBase
            ) : base(cfcConfigApiBase, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = cfcConfigApiBase.Value;
            objPeticionesHttpApi = new PeticionesHttpApi<CentinelaService>(objLogger);
        }

        public ResultadoBase RecuperarSuperAdministradorCentinela(int IdBoletin, int IdLineaBoletin)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdBoletin: {1} , IdLineaBoletin: {2}  ", nameof(RecuperarSuperAdministradorCentinela), IdBoletin.ToString(), IdLineaBoletin.ToString());
            ResultadoOperacion<SuperAdministradorCentinela> objResultado = new ResultadoOperacion<SuperAdministradorCentinela>();
            try
            {


                var objResultadoCrm = objPeticionesHttpApi.GetApi<SuperAdministradorCentinela>($"{objConfig.Urls.Evolution.Domain}", "/web_service/dameSuperadministradorCentinela/BOLETIN/" + IdBoletin.ToString() + "/" + IdLineaBoletin, "");

                if (objResultadoCrm != null)
                {
                    objResultado.eResultado = enumResultado.Ok_Recuperar;

                    objResultado.oResultado = objResultadoCrm;

                    objResultado.sDescripcion = objResultado.oResultado.Encontrado ? "Se ha encontrado un super administrador" : "No se ha encontrado un super administrador";

                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_Validacion;
                    objResultado.sDescripcion = "No se han recuperado información de CRM.";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarSuperAdministradorCentinela), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarSuperAdministradorCentinela), ex);

                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }
            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarSuperAdministradorCentinela), objResultado.sDescripcion);
            return objResultado;
        }

        public ResultadoBase RecuperarSuperAdministradorCentinelaPresupuesto(int IdExpediente, string IdProductoNav, string Variante)
        {
            objLogger.LogDebug("START  --> {0} Parametros--> IdExpediente: {1} , IdProductoNav: {2} , Variante: {3}   ", nameof(RecuperarSuperAdministradorCentinelaPresupuesto), IdExpediente.ToString(), IdProductoNav, Variante);
            ResultadoOperacion<SuperAdministradorCentinela> objResultado = new ResultadoOperacion<SuperAdministradorCentinela>();
            try
            {
                var objResultadoCrm = objPeticionesHttpApi.GetApi<SuperAdministradorCentinela>($"{objConfig.Urls.Evolution.Domain}", "/web_service/dameSuperadministradorCentinela/PRESUPUESTOS/" + IdExpediente.ToString() + "/" + IdProductoNav + "/" + Variante, "");

                if (objResultadoCrm != null)
                {
                    objResultado.eResultado = enumResultado.Ok_Recuperar;

                    objResultado.oResultado = objResultadoCrm;

                    objResultado.sDescripcion = objResultado.oResultado.Encontrado ? "Se ha encontrado un super administrador" : "No se ha encontrado un super administrador";

                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_Validacion;
                    objResultado.sDescripcion = "No se han recuperado información de Kojak.";
                }


            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarSuperAdministradorCentinelaPresupuesto), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarSuperAdministradorCentinelaPresupuesto), ex);

                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }

            objLogger.LogDebug("END  --> {0} Resultado--> {1}", nameof(RecuperarSuperAdministradorCentinelaPresupuesto), objResultado.sDescripcion);
            return objResultado;
        }
    }
}
