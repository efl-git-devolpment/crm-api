﻿using AutoMapper;
using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Crm;
using Crm.Vistas.LibreriaVirtual;
using DllApiBase.Servicio;
using DllApiBase.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Crm.Servicio.Services
{
    public class CuentasV2Service : ServicioApiBase<CuentasV2Service>, ICuentasV2Service<ResultadoBase>
    {
        private readonly ILogger<CuentasV2Service> objLogger;
        private readonly ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> objServiceCrm;
        private readonly ConfigApi objConfig;
        private readonly IMapper objMapper;
        public CuentasV2Service(

           ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> objServiceCrmBase,
            ILogger<CuentasV2Service> objLoggerBase,
            IOptions<ConfigApi> objConfigBase,
            IMapper objMapperBase
            ) : base(objConfigBase, objLoggerBase)
        {
            objServiceCrm = objServiceCrmBase;
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
            objMapper = objMapperBase;
        }

        /// <summary>
        /// Método para recuperar información de una Cuenta por cod.Navision o cod.Crm
        /// </summary>
        /// <param name="IdClienteNav">Id Cliente Navision</param>
        /// <param name="IdClienteCrm">Id Cliente Crm</param>
        /// <returns></returns>
        public async Task<ResultadoBase> RecuperarInfoCuenta(int IdClienteNav, int IdClienteCrm)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarInfoCuenta), IdClienteNav.ToString(), IdClienteCrm.ToString());

            ResultadoOperacion<InfoCuenta> objResultado = new ResultadoOperacion<InfoCuenta>();

            try
            {


                var objResultadoCrm = (ResultadoObtenerInfoCuenta)await objServiceCrm.RecuperarInfoCuenta(IdClienteNav, IdClienteCrm);
                if (objResultadoCrm != null)
                {
                    if ((objResultadoCrm.codigoResultado == 0) && (objResultadoCrm.cuenta != null))
                    {
                        if (String.IsNullOrEmpty(objResultadoCrm.cuenta.id_cuenta))
                        {
                            objResultado.sDescripcion = "Error recuperado en servicio CRM : " + objResultadoCrm.mensajeResultado;
                            objResultado.eResultado = enumResultado.Error_Validacion;
                        }
                        else
                        {
                            objResultado.oResultado = objMapper.Map<InfoCuenta>(objResultadoCrm.cuenta);


                            objResultado.sDescripcion = "Encontrado información de cuenta para "
                            + (IdClienteNav > 0 ? " código Navision " + IdClienteNav.ToString() : " código Crm " + IdClienteCrm.ToString());
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                        }
                    }
                    else
                    {
                        objResultado.sDescripcion = "Error recuperado en servicio CRM : " + objResultadoCrm.mensajeResultado;
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }
                else
                {
                    objResultado.sDescripcion = "No se recuera información desde servicio CRM.";
                    objResultado.eResultado = enumResultado.Error_Validacion;
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInfoCuenta), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInfoCuenta), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarInfoCuenta), objResultado.sDescripcion);
            return objResultado;
        }


        private bool IsValidoDominio(string strDominio)
        {


            Boolean bResultado = false;
            try
            {
                Regex regex = new Regex(@"[0-9a-zA-Z]+\.[a-zA-Z]+[0-9a-zA-Z]+");
                bResultado = (regex.IsMatch(strDominio));
            }
            catch (Exception ex)
            {
                bResultado = false;
            }
            return bResultado;
        }
        /// <summary>
        /// Comprobar si un dominio es válido en CRM
        /// </summary>
        /// <param name="strDominio">Literal del dominio</param>
        /// <returns></returns>
        public async Task<ResultadoBase> ComprobarDominioValido(string strDominio)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(ComprobarDominioValido), strDominio);

            ResultadoBooleano objResultado = new ResultadoBooleano();

            try
            {

                if (!this.IsValidoDominio(strDominio))
                {

                    objResultado.sDescripcion = "Error en parámetro: StrDominio debe tener formato correcto (literal.extensión)";
                    objResultado.bResultado = false;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                }
                else
                {
                    bool? objResultadoCRM = await objServiceCrm.ComprobarDominioValido(strDominio);


                    if (objResultadoCRM != null)
                    {
                        objResultado.sDescripcion = "El dominio " + strDominio + (!objResultadoCRM.Value ? " es válido " : " no es válido");
                        objResultado.bResultado = !objResultadoCRM.Value;
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                        objLogger.LogDebug("CONTINUE --> {0} - Se ha recuperado información sobre el dominio {1} y es {2}", nameof(ComprobarDominioValido), strDominio, objResultadoCRM.Value.ToString());

                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "No hay respuesta desde CRM.";
                        objLogger.LogDebug("CONTINUE --> {0} - No se han recuperado información sobre el dominio {1} ", nameof(ComprobarDominioValido), strDominio);
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ComprobarDominioValido), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ComprobarDominioValido), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(ComprobarDominioValido), objResultado.sDescripcion);
            return objResultado;
        }

        /// <summary>
        /// Método para recuperar información de cuentas por diferentes filtros
        ///// </summary>
        ///// <param name="iTipoBusqueda">Id Cliente Navision</param>
        ///// <param name="IdClienteCrm">Id Cliente Crm</param>
        /// <returns></returns>
        public async Task<ResultadoBase> RecuperarInfoCuentas(int iTipoBusqueda, int IdClienteCrm, int IdClienteNav, string IdUsuarioCrm, string strNombreCompleto, string strRazonSocial, string strTelefono, string strEmail, string strCifNif, string strCampoAOrdenar, int iTipoOrden, int iPaginaInicial, int iNumeroFilas)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}", nameof(RecuperarInfoCuentas), iTipoBusqueda.ToString(), IdClienteCrm.ToString(), IdClienteNav.ToString(), IdUsuarioCrm, strNombreCompleto, strRazonSocial, strTelefono, strEmail, strCifNif, strCampoAOrdenar, iTipoOrden.ToString(), iPaginaInicial.ToString(), iNumeroFilas.ToString());

            ResultadoLista<DllApiBase.Vistas.Cliente.ClientesCrm> objResultado = new ResultadoLista<DllApiBase.Vistas.Cliente.ClientesCrm>();

            try
            {
                string strOrigen = "web";
                string strOrden = iTipoOrden == 1 ? "ASC" : "DESC";

                var objResultadoCrm = (ResultadoRecuperarInfoCuentas)await objServiceCrm.RecuperarInfoCuentas(iTipoBusqueda, strOrigen, IdClienteCrm, IdClienteNav, IdUsuarioCrm, strNombreCompleto, strRazonSocial, strTelefono, strEmail, strCifNif, strCampoAOrdenar, strOrden, iPaginaInicial, iNumeroFilas);
                if (objResultadoCrm != null)
                {
                    if ((objResultadoCrm.codigoResultado == 0) && (objResultadoCrm.numero_total_clientes >= 0))
                    {
                        if (objResultadoCrm.lstCuentas == null)
                        {
                            objResultado.iContador = 0;
                            objResultado.sDescripcion = "Error recuperado en servicio CRM : " + objResultadoCrm.mensajeResultado;
                            objResultado.eResultado = enumResultado.Error_Validacion;
                        }
                        else
                        {
                            objResultado.iContador = objResultadoCrm.numero_total_clientes.HasValue ? objResultadoCrm.numero_total_clientes.Value : 0;
                            objResultado.lstResultado = objMapper.Map<List<DllApiBase.Vistas.Cliente.ClientesCrm>>(objResultadoCrm.lstCuentas);

                            objResultado.sDescripcion = "Encontrado " + objResultadoCrm.numero_total_clientes.ToString();
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                        }
                    }
                    else
                    {
                        objResultado.sDescripcion = "Error recuperado en servicio CRM : " + objResultadoCrm.mensajeResultado;
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }
                else
                {
                    objResultado.sDescripcion = "No se recuera información desde servicio CRM.";
                    objResultado.eResultado = enumResultado.Error_Validacion;
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInfoCuentas), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInfoCuentas), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarInfoCuentas), objResultado.sDescripcion);
            return objResultado;
        }

        /// <summary>
        /// Método para recuperar ilos usuarios visual por cuenta
        /// </summary>
        /// <param name="iTipoBusqueda">Tipo de búsqueda (1 - cuenta , 2 - potencial (lead) </param>
        /// <param name="IdClienteCrm">Id Cliente Crm</param>
        /// <returns></returns>
        public async Task<ResultadoBase> RecuperarComerciales(int iTipoBusqueda, int IdClienteCrm)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarComerciales), iTipoBusqueda.ToString(), IdClienteCrm.ToString());

            ResultadoLista<UsuarioVisual> objResultado = new ResultadoLista<UsuarioVisual>();

            try
            {

                var objResultadoCrm = (ResultadoDameInfoComercial)await objServiceCrm.RecuperarInfoComercial(iTipoBusqueda, IdClienteCrm);
                if (objResultadoCrm != null)
                {
                    if (objResultadoCrm.codigoResultado == 0)
                    {
                        if (objResultadoCrm.listadoUsuarios == null)
                        {
                            objResultado.sDescripcion = "Error recuperado en servicio CRM : " + objResultadoCrm.mensajeResultado;
                            objResultado.eResultado = enumResultado.Error_Validacion;
                        }
                        else
                        {
                            objResultado.lstResultado = objMapper.Map<List<UsuarioVisual>>(objResultadoCrm.listadoUsuarios);
                            objResultado.iContador = objResultado.lstResultado.Count;


                            objResultado.sDescripcion = "Encontrado " + objResultado.lstResultado.Count.ToString();
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                        }
                    }
                    else
                    {
                        objResultado.sDescripcion = "Error recuperado en servicio CRM : " + objResultadoCrm.mensajeResultado;
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }
                else
                {
                    objResultado.sDescripcion = "No se recuera información desde servicio CRM.";
                    objResultado.eResultado = enumResultado.Error_Validacion;
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarComerciales), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarComerciales), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarComerciales), objResultado.sDescripcion);
            return objResultado;
        }


    }
}
