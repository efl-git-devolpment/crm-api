﻿using Crm.Comun.Configuration;
using Crm.Servicio.Interfaces;
using Crm.Vistas.Boletines;
using Crm.Vistas.Crm;
using Crm.Vistas.LibreriaVirtual;
using DllApiBase.Servicio;
using DllApiBase.Vistas;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace Crm.Servicio.Services
{
    public class BoletinesService : ServicioApiBase<BoletinesService>, IBoletinesV2Service<ResultadoBase>
    {
        private readonly ILogger<BoletinesService> objLogger;
        private readonly ConfigApi objConfig;

        private readonly ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> objServiceCrm;


        public BoletinesService(
            ILogger<BoletinesService> objLoggerBase,
            IOptions<ConfigApi> objConfigBase,
            ICrmService<UsuariosIndicator, ResultadoCuenta, ResultadoDuplicadoContactos, ResultadoDameBoletinInfo, ResultadoOperacionContacto, ResultadoObtenerInfoCuenta, ResultadoInfoContacto, ResultadoInfoValidoContacto, ResultadoRecuperarInfoCuentas, ResultadoBoletinGenial, ResultadoDameInfoComercial, ResultadoContactosCuenta, ResultadoModificarEmailContacto> objServiceCrmBase
            ) : base(objConfigBase, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
            objServiceCrm = objServiceCrmBase;
        }


        public ResultadoBase ComprobarPermisoCreacionBoletin(int IdClienteCrm, string strOrigen, string IdUsuarioCrm)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3}", nameof(ComprobarPermisoCreacionBoletin), IdClienteCrm.ToString(), strOrigen, IdUsuarioCrm);

            ResultadoOperacion<PermitirBoletin> objResultado = new ResultadoOperacion<PermitirBoletin>();

            try
            {
                // Esta funcionalidad hay que migrarla a CRM Service

                PermitirBoletin objPermitirBoletin = new PermitirBoletin();

                HttpClient objClient = new HttpClient();
                string strUrl = $"{objConfig.Urls.Evolution.Domain}" + "/web_service/permisoCreacionBoletin/" + IdClienteCrm.ToString() + "/" + strOrigen + "/" + IdUsuarioCrm;

                HttpResponseMessage objResponse = objClient.GetAsync(strUrl).Result;

                string strResultado = objResponse.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrWhiteSpace(strResultado))
                {
                    objLogger.LogDebug("Continue --> Resultado recuperado en {0} Resultado devuelto CRM -->{1}", nameof(ComprobarPermisoCreacionBoletin), strResultado);

                    PermitirBoletin objResultadoCrm = new PermitirBoletin();

                    objResultadoCrm = JsonConvert.DeserializeObject<PermitirBoletin>(strResultado);
                    if (objResultado != null)
                    {
                        objLogger.LogError("Continue --> Recuperado info en  {0} con resultado  -->{1}", nameof(ComprobarPermisoCreacionBoletin), objResultadoCrm.codigoResultado);
                        objResultado.oResultado = objResultadoCrm;
                        objResultado.sDescripcion = "Recuperado Permitir Boletin con mensaje CRM" + objResultadoCrm.codigoResultado;
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                    }
                    else
                    {
                        objLogger.LogError("Continue --> No se ha recuperado info en {0} a la url {1}", nameof(ComprobarPermisoCreacionBoletin), strUrl);
                        objResultado.oResultado = null;
                        objResultado.sDescripcion = "Error al recuperar";
                        objResultado.eResultado = enumResultado.Error_Validacion;
                    }
                }
                else
                {
                    objResultado.oResultado = null;
                    objResultado.sDescripcion = "Error al recuperar desde la url : " + strUrl;
                    objResultado.eResultado = enumResultado.Error_Validacion;
                    objLogger.LogError("Continue --> No se ha recibido ninguna respuesta en {0} a la url {1}", nameof(ComprobarPermisoCreacionBoletin), strUrl);
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ComprobarPermisoCreacionBoletin), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ComprobarPermisoCreacionBoletin), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = "Error en la llamada a CRM : " + ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(ComprobarPermisoCreacionBoletin), objResultado.sDescripcion);
            return objResultado;
        }


        /// <summary>
        /// Recuperar info del boletin Crm
        /// </summary>
        /// <param name="iIdBoletin">Id boletin Crm</param>
        /// <returns></returns>
        public ResultadoBase RecuperarBoletinInfo(int iIdBoletin)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarBoletinInfo), iIdBoletin.ToString());

            ResultadoOperacion<BoletinInfo> objResultado = new ResultadoOperacion<BoletinInfo>();

            try
            {

                var objResultadoCRM = (ResultadoDameBoletinInfo)objServiceCrm.RecuperarBoletinInfo(iIdBoletin).Result;

                if ((objResultadoCRM != null) && (objResultadoCRM.mensajeResultado != null))
                {
                    objResultado.sDescripcion = "Respuesta desde CRM : " + objResultadoCRM.mensajeResultado;
                    if (objResultadoCRM.codigoResultado.ToLower() == "ok")
                    {
                        objResultado.oResultado = objResultadoCRM.mensajeResultado;
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                        objResultado.sDescripcion = "Se han recuperado info del boletin " + iIdBoletin.ToString();

                        objLogger.LogDebug("CONTINUE --> {0} - Se han recuperado info del boletin {1}", nameof(RecuperarBoletinInfo), iIdBoletin.ToString());

                    }
                    else
                    {
                        objResultado.oResultado = null;
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "No se han recuperado info del boletin, con mensaje desde CRM : " + objResultadoCRM.codigoResultado;

                        objLogger.LogDebug("CONTINUE --> {0} - No se han recuperado info del boletin, con mensaje desde CRM : {1},{2}", nameof(RecuperarBoletinInfo), iIdBoletin.ToString(), objResultadoCRM.codigoResultado);
                    }
                }
                else
                {
                    objResultado.oResultado = null;
                    objResultado.eResultado = enumResultado.Error_Validacion;
                    objResultado.sDescripcion = "No se ha recuperado información de CRM.";
                    objLogger.LogDebug("CONTINUE --> {0} - No se ha recuperado info de CRM", nameof(RecuperarBoletinInfo));

                    objResultado.eResultado = enumResultado.Error_Excepcion;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarBoletinInfo), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarBoletinInfo), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarBoletinInfo), objResultado.sDescripcion);
            return objResultado;
        }

        /// <summary>
        /// Recuperar info del boletin Genial
        /// </summary>
        /// <param name="iIdBoletin">Id contacto Crm</param>
        /// <param name="iLineaBoletin">Linea de Boletín</param>
        /// <returns></returns>
        public ResultadoBase RecuperarBoletinGenialInfo(int iIdBoletin, int iLineaBoletin)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarBoletinGenialInfo), iIdBoletin.ToString(), iLineaBoletin.ToString());

            ResultadoOperacion<BoletinGenial> objResultado = new ResultadoOperacion<BoletinGenial>();

            try
            {

                var objResultadoCRM = (ResultadoBoletinGenial)objServiceCrm.RecuperarBoletinGenial(iIdBoletin, iLineaBoletin).Result;

                if ((objResultadoCRM != null))
                {
                    objResultado.sDescripcion = "Respuesta desde CRM : " + objResultadoCRM.encontrado;
                    if ((objResultadoCRM.encontrado) && (objResultadoCRM.productoGenial != null))
                    {
                        objResultado.oResultado = objResultadoCRM.productoGenial;
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                        objResultado.sDescripcion = "Se han recuperado info del boletin de Genial " + iIdBoletin.ToString() + " con línea " + iLineaBoletin.ToString();
                    }
                    else
                    {
                        objResultado.oResultado = null;
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "No se han recuperado info del boletin, con mensaje desde CRM : " + objResultadoCRM.encontrado;
                    }
                }
                else
                {
                    objResultado.oResultado = null;
                    objResultado.eResultado = enumResultado.Error_Validacion;
                    objResultado.sDescripcion = "No se ha recuperado información de CRM.";
                    objResultado.eResultado = enumResultado.Error_Excepcion;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarBoletinGenialInfo), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarBoletinGenialInfo), ex);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarBoletinGenialInfo), objResultado.sDescripcion);
            return objResultado;
        }

    }

}
