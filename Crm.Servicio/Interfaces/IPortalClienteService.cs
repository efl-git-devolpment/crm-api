﻿namespace Crm.Servicio.Interfaces
{
    public interface IPortalClienteService<RB>
    {
        RB RecuperarInfoPortalClienteBoletin(int IdBoletin, int IdLineaBoletin);

        RB RecuperarInfoPortalClientePresupuesto(int IdExpediente, string IdProductoNav, string Variante);

       
    }
}