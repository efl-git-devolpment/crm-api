﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Servicio.Interfaces
{
    public interface ICuentasV2Service<RB>
    {


        Task<RB> RecuperarInfoCuenta(int IdClienteNav, int IdClienteCrm);
        Task<RB> ComprobarDominioValido(string strDominio);

        Task<RB> RecuperarInfoCuentas(int iTipoBusqueda, int IdClienteCrm, int IdClienteNav, string IdUsuarioCrm, string strNombreCompleto, string strRazonSocial, string strTelefono, string strEmail, string strCifNif, string strCampoAOrdenar, int iTipoOrden, int iPaginaInicial, int iNumeroFilas);

        Task<RB> RecuperarComerciales(int iTipoBusqueda, int IdClienteCrm);

    }
}
