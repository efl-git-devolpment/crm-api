﻿using Crm.Vistas.Crm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Servicio.Interfaces
{
    public interface ICrmService<UI, RC, RDC, RDBI, ROC, ROIC, RIC, RIVC, RCS, RBG, RICOM, RCC, RMECE>
    {
        Task<int> CargarContactoCRM(int iNoCliente);
        Task<RC> CargarCuentaIndicatorEnCrm(UI objCuenta, string strOrigenSolicitud);

        Task<RDC> ComprobarDuplicadosContacto(string strOrigen, int IdClienteCrm, string strModule, string strNombre, string strApellido1, string strApellido2, string strEmail);

        Task<RDBI> RecuperarBoletinInfo(int iIdBoletin);

        Task<ROC> InsertarContacto(string strOrigen, int IdClienteCrm, string strModule, string strNombre, string strApellido1, string strApellido2, string strEmail, string strCifNif, string strTelefono, bool bIndEspejo, bool bIndTercero);

        Task<ROC> ModificarContacto(int idContactoCrm, string strOrigen, int IdClienteCrm, string strModule, string strNombre, string strApellido1, string strApellido2, string strEmail, string strEmailAnt, string strCifNif, string strTelefono, string strTipoEmail);

        Task<ROIC> RecuperarInfoCuenta(int IdClienteNav, int IdClienteCrm);

        Task<Boolean?> ComprobarDominioValido(string strDominio);

        Task<RIC> RecuperarInfoContacto(int IdContactoCrm);

        Task<RIVC> RecuperarInfoValidoContacto(int IdContactoCrm);

        Task<RCS> RecuperarInfoCuentas(int iTipoBusqueda,string strOrigen, int IdClienteCrm, int IdClienteNav, string IdUsuarioCrm, string strNombreCompleto, string strRazonSocial, string strTelefono, string strEmail, string strCifNif, string strCampoAOrdenar, string strOrden, int iPaginaInicial, int iNumeroFilas);

        Task<RBG> RecuperarBoletinGenial(int iIdBoletin, int iLineaBoletin);

        Task<RICOM> RecuperarInfoComercial(int iTipoBusqueda, int IdClienteCrm);

        Task<RCC> RecuperarContactos(string strOrigen, int iTipoBusqueda, int IdClienteCrm, int IdClienteNav, string strNombre, string strApellidos, string strEmail, string strNif, string strTelefono, string strBusqueda, int iPaginaInicial, int iNumeroFilas);

        Task<RMECE> ModificarContactoEntidad(int idContactoCrm, string strOrigen, string strModule, string strEmailNuevo, string strEmailAnt, string strIdContacto);


    }
}
