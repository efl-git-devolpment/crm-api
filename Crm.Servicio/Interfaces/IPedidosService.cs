﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Interfaces
{
    public interface IPedidosService<RB>
    {
        RB RecuperarTipoPedido(int intClasificacion);
    }
}
