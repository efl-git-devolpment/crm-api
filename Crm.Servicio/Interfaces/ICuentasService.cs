﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Servicio.Interfaces
{
    public interface ICuentasService
    {
        Task<int> CargarCuentasIndicatorAsync();
    }
}
