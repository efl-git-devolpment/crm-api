﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Interfaces
{
    public interface ILibreriaVirtualService<T>
    {
        List<T> RecuperarCuentasIndicatorPedidos();
        int GuardarCuentaIndicator(int idUsuarioIndicator, int IdClienteCrm, int? IdContactoCrm, int idUic);
        int GuardarClienteCoincidenteIndicator(int idUsuarioIndicator,string strModulo, int IdCrm, string strNombre, int idUic);
    }
}
