﻿using DllApiBase.Vistas;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Interfaces
{
    public interface IBoletinesV2Service<RB>
    {
        RB ComprobarPermisoCreacionBoletin(int IdClienteCrm, string strOrigen, string IdUsuarioCrm);

        RB RecuperarBoletinInfo(int iIdBoletin);

        RB RecuperarBoletinGenialInfo(int iIdBoletin, int iLineaBoletin);
    }
}
