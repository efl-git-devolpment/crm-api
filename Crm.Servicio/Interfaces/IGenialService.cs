﻿using DllApiBase.Vistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Servicio.Interfaces
{
    public interface IGenialService
    {
        ResultadoBase RecuperarInfoGenialBoletin(int IdBoletin, int IdLineaBoletin);

        ResultadoBase RecuperarInfoGenialPresupuesto(int IdExpediente, string IdProductoNav, string Variante);
    }
}
