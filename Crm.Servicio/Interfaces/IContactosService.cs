﻿using DllApiBase.Vistas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Servicio.Interfaces
{
    public interface IContactosService<T, U, V, Y>
    {
        Task<T> RecuperarCuentaContactosByNavisionAsync(
            int IdClienteNav,
            string Search,
            int PageNum,
            int PageSize);

        Task<T> RecuperarCuentaContactosAsync(
            int IdClienteCrm,
            string Search,
            int PageNum,
            int PageSize);

        Task<int> AddAsync(U entity);
        Task<int> AddContactoAsync(U entity, V conn);

        Task<int> AddContactoClavesAsync(
            int NumTdcSugar,
            int OrigenModule,
            string Nombre,
            string Apellido1,
            string Apellido2,
            string Telefono,
            string Email,
            string CifNif);

        Task<int> updateContactoClaves(
            int IdContactoCrm,
            int NumTdcSugar,
            int OrigenModule,
            string Nombre,
            string Apellido1,
            string Apellido2,
            string Telefono,
            string Email,
            string CifNif);

        Task<int> UpdateContactoAsync(Y entity, V conn);

        Task<int> updateContactoCompleto(
            Y contacto);

    }
}
