﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Interfaces
{
    public interface IAsistentesService<RB>
    {
        RB ComprobarPermisoCreacionAsistente(int IdClienteCrmFacturacion, int IdClienteCrmEnvio, int IdContactoCrm, string strEmailContacto);
    }
}
