﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Interfaces
{
    public interface ICentinelaService<RB>
    {
        RB RecuperarSuperAdministradorCentinela(int IdBoletin, int IdLineaBoletin);

        RB RecuperarSuperAdministradorCentinelaPresupuesto(int IdExpediente, string IdProductoNav, string Variante);
    }
}
