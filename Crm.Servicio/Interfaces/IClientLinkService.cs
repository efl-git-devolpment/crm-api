﻿namespace Crm.Servicio.Interfaces
{
    public  interface IClientLinkService<RB>
    {
        RB RecuperarInfoClientLinkBoletin(int IdBoletin, int IdLineaBoletin);

        RB RecuperarInfoClientLinkPresupuesto(int IdExpediente, string IdProductoNav, string Variante);
    }
}