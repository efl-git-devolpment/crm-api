﻿namespace Crm.Servicio.Interfaces
{
    public interface ILexonService<RB>
    {
        RB RecuperarInfoLexonBoletin(int IdBoletin, int IdLineaBoletin);
        RB RecuperarInfoLexonPresupuesto(int IdExpediente, string IdProductoNav, string Variante);
        RB RecuperarDatosContactoExpedienteBaja(int IdClienteCRM, int NumSuscripcion);
    }
}