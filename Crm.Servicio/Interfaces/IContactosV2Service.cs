﻿using DllApiBase.Vistas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Servicio.Interfaces
{
    public interface IContactosV2Service<RB>
    {
       

        Task<RB> RecuperarContactoAdministrador(int IdClienteNav);

        Task<RB> RecuperarContactosDuplicados(int IdClienteCrm, int iTipoBusqueda, string strNombre, string strApellido1, string strApellido2, string strEmail);

        Task<RB> InsertarContacto(int IdClienteCrm, int intOrigenModule, string strNombre, string strApellido1, string strApellido2, string strTelefono, string strEmail, string strCifNif, bool bIndTercero);

        Task<RB> RecuperarContactosByClienteCrm(int IdClienteCrm, string strBusqueda, int iPaginaInicial, int iNumeroDeFilas);

        Task<RB> ModificarContacto(int IdContactoCrm, int IdClienteCrm, int intOrigenModule, string strNombre, string strApellido1, string strApellido2, string strTelefono, string strEmail, string strEmailAnt, string strCifNif, int intTipoEmail);

        Task<RB> RecuperarInfoContacto(int IdContactoCrm);

        Task<RB> RecuperarInfoValidoContacto(int IdContactoCrm);

        Task<RB> RecuperarContactos(int iTipoBusqueda, int IdClienteCrm, int IdClienteNav, string strNombre, string strApellidos, string strEmail, string strCifNif, string strTelefono, string strBusqueda, int iPaginaInicial, int iNumeroFilas);

        Task<RB> ModificarContactoEntidad(int IdContactoCrm, int intOrigeModule, string strEmailNuevo, string strEmailAnt, string strIdContacto);


    }
}
