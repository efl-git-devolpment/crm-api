﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Interfaces
{
    public interface ICasosSacService<RB, CS>
    {
        RB CrearCasoSac(CS objPeticion);
        RB ModificarCasoSac(CS objPeticion);
        RB RecuperarTipoCasoSac();
    }
}
