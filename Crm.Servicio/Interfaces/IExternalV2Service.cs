﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.Servicio.Interfaces
{
    public interface IExternalV2Service<RB>
    {
        RB RecuperarUsuariosPermiso(int IdClienteNav, string strPermiso);

        RB RecuperarUsuarioProducto(int IdClienteNav, string IdUsuarioPro);

       // RB RecuperarUsuariosProducto(int idClienteNav, int iIdEstado, int iIdSuscripcionNav, string strBusqueda, int iIndDemo, int iIndSuscripcionAsociada, int iIdTipoUsuarioProducto, string strFechaDesde, string strFechaHasta, int iNumPage, int iNumSize);
    }
}
