﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Servicio.Interfaces
{
    public interface ILeadsService<RB, T>
    {
        Task<RB> GuardarLeadKojak(T lead, int iIdLead);
        Task<RB> RecuperarResultadoLeadsKojak(int iIdLead);
    }
}
