﻿using AutoMapper;
using Crm.Modelos.Extendidos;
using Crm.Vistas.CasoSac;
using Crm.Vistas.Contactos;
using Crm.Vistas.Crm;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Crm.Servicio
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CrContactoListadoExt, ContactoListadoExt>()
                .ForMember(dest => dest.Listado, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<CrContacto, Contacto>()
                .ReverseMap();

        }


    }
}
